package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Paths;

import org.dcm4che.data.VR;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.jhmi.mint.data.BulkdataReference;
import edu.jhmi.mint.data.Study;

public class DefaultStudyStoreTest {

    private static final String TEST_STUDY_IUID = "2.25.1234.5678";

    private static final byte[] TEST_VALUE1 = "blah blah".getBytes(Charset
            .forName("UTF-8"));

    private static final byte[] TEST_VALUE2 = { 0x01, 0x02, 0x03 };

    private DefaultStudyStore studyStore = null;

    @Before
    public void setUp() throws IOException {

        studyStore = new DefaultStudyStore(
                Paths.get("target/test-out/study-store-test"),true);
    }

    @Test
    public void testReadWriteBulkdataValues() throws IOException {

        BulkdataReference bdr1 = studyStore.putBulkdataValue(TEST_STUDY_IUID,
                ByteBuffer.wrap(TEST_VALUE1), VR.OW);
        BulkdataReference bdr2 = studyStore.putBulkdataValue(TEST_STUDY_IUID,
                ByteBuffer.wrap(TEST_VALUE2), VR.OB);
        studyStore.finishStudy(new Study(TEST_STUDY_IUID));
        Assert.assertTrue(bdr1.getValueOffset() > 0);
        Assert.assertEquals(bdr1.getValueOffset() + TEST_VALUE1.length,
                bdr2.getValueOffset());
        Assert.assertEquals(TEST_VALUE2.length, bdr2.getValueLength());

        ByteBuffer buffer1 = studyStore.getBulkdataValue(TEST_STUDY_IUID, bdr1);
        ByteBuffer buffer2 = studyStore.getBulkdataValue(TEST_STUDY_IUID, bdr2);
        Assert.assertEquals(ByteBuffer.wrap(TEST_VALUE1), buffer1);
        Assert.assertEquals(ByteBuffer.wrap(TEST_VALUE2), buffer2);
    }
}
