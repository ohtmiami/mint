package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class DicomInputSourceTest {

    public static void main(String[] args) throws IOException {

        if (args.length < 1) {
            System.err.println("Usage: DicomInputSourceTest <dir>");
            System.exit(1);
        }

        Path dirPath = Paths.get(args[0]);
        System.out.println("======== Any extension ========");
        try (DicomInputSource inputSource = DicomInputSource
                .fromFolder(dirPath)) {
            while (inputSource.hasNext()) {
                try (InputStream fileIn = inputSource.next()) {
                    // Do nothing
                }
                System.out.println(inputSource.currentInstanceURI());
            }
        }
        System.out.println();
        System.out.println("======== DCM extension ========");
        try (DicomInputSource inputSource = DicomInputSource.fromFolder(
                dirPath, DicomInputSource.DCM_EXTENSION_PATTERN)) {
            while (inputSource.hasNext()) {
                try (InputStream fileIn = inputSource.next()) {
                    // Do nothing
                }
                System.out.println(inputSource.currentInstanceURI());
            }
        }
    }
}
