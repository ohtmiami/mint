package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MintHeaderTest {

    private static final byte[] IV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

    private static final String STUDY_IUID = "1.2.3.4";
    
    private static final String compressedAlgorithm="deflate";
    
    private static final String encryptedAlgorithm="AES-GCM-256";
    
    private static final UUID FILE_UUID = new UUID(0xABCD, 0xEF01);

    private static MintHeader createHeader() throws MalformedMintHeaderException {
        
        MintHeader header = new MintHeader();
        MintHeader.setMajorVersion((byte) 0x02);
        MintHeader.setMinorVersion((byte) 0x00);
        header.setPayloadType(MintHeader.PayloadType.METADATA);
        header.setEncrypted(true);
        header.setCompressed(true);
        header.setUncompressedLength(0x6789);

        header.addOption(new MintUIDOption(STUDY_IUID));
        header.addOption(new MintUUIDOption(FILE_UUID));
        header.addOption(new MintIVOption(IV));
        return header;
    }
    private static MintHeader  createHeader21() throws MalformedMintHeaderException{
        MintHeader header = new MintHeader();
        MintHeader.setMajorVersion((byte) 0x02);
        MintHeader.setMinorVersion((byte) 0x01);
        header.setPayloadType(MintHeader.PayloadType.METADATA);
        header.setEncrypted(true);
        header.setCompressed(true);
        header.addOption(new MintUUIDOption(FILE_UUID));
        header.addOption(new MintUIDOption(STUDY_IUID));
        header.addOption(new MintCompressedOption(compressedAlgorithm));
        header.addOption(new MintEncryptedOption(encryptedAlgorithm));
        header.addOption(new MintIVOption(IV));
        return header;        
    }
    @Test
    public void testEncodeDecode() throws MalformedMintHeaderException {
        //This part is used to test old version of header
        MintHeader header = createHeader();
        ByteBuffer buffer = header.encode();
        MintHeader decodedHeader = new MintHeader();
        decodedHeader.decode(buffer);
        Assert.assertEquals(header.getPayloadType(),
                decodedHeader.getPayloadType());
        Assert.assertEquals(header.isEncrypted(), decodedHeader.isEncrypted());
        Assert.assertEquals(header.isCompressed(), decodedHeader.isCompressed());
        Assert.assertEquals(header.getUncompressedLength(),
                decodedHeader.getUncompressedLength());
        for (MintHeaderOption<?> option : header.getOptions()) {
            MintHeaderOption<?> decodedOption = decodedHeader.getOption(option
                    .getBitmask());
            Assert.assertEquals(option, decodedOption);
        }
        //This part is used to test new version of header
        MintHeader headerNew=createHeader21();
        ByteBuffer bufferNew = headerNew.encode();
        MintHeader decodedHeader21 = new MintHeader();
        decodedHeader21.decode(bufferNew);
        Assert.assertEquals(headerNew.getPayloadType(),
                decodedHeader21.getPayloadType());
        Assert.assertEquals(headerNew.isEncrypted(), decodedHeader21.isEncrypted());
        Assert.assertEquals(headerNew.isCompressed(), decodedHeader21.isCompressed());
        Assert.assertEquals(headerNew.getUncompressedLength(),
                decodedHeader21.getUncompressedLength());
        for (MintHeaderOption<?> option : headerNew.getOptions()) {
            MintHeaderOption<?> decodedOption = decodedHeader21.getOption(option
                    .getBitmask());
            Assert.assertEquals(option, decodedOption);
        }
    }
}
