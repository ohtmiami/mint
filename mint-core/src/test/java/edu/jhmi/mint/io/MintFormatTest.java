package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.data.TestUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MintFormatTest {

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.err.println("Usage: MintFormatTest"
                    + " <study_input_dir> <study_output_dir>");
            System.exit(1);
        }
        MintEncodingOptions encodingOptions = new MintEncodingOptions(true,
                true);
        // testWriteReadStudy(Paths.get(args[0]), Paths.get(args[1]),
        // encodingOptions);
        testReadWriteStudy(Paths.get(args[0]), Paths.get(args[1]),
                encodingOptions);
    }

    public static void testWriteReadStudy(Path studyPath, Path studyOutputPath,
            MintEncodingOptions encodingOptions) throws IOException {

        Study study = TestUtils.readDicomStudy(studyPath);
        TestUtils.getStudyStore().reorganizeBulkdata(study,
                encodingOptions.encryptedAlgorithm, true);
        Path metadataFilePath = TestUtils.writeStudyMetadata(study,
                studyOutputPath, encodingOptions);
        Study mintStudy = TestUtils.readStudyMetadata(metadataFilePath);
        TestUtils.printStudy(mintStudy, System.out);
    }

    public static void testReadWriteStudy(Path metadataFilePath,
            Path studyOutputPath, MintEncodingOptions encodingOptions)
            throws IOException {

        Study study = TestUtils.readStudyMetadata(metadataFilePath);
        TestUtils.getStudyStore().reorganizeBulkdata(study,
                encodingOptions.encryptedAlgorithm, false);
        Path newMetadataFilePath = TestUtils.writeStudyMetadata(study,
                studyOutputPath, encodingOptions);
        Study mintStudy = TestUtils.readStudyMetadata(newMetadataFilePath);
        TestUtils.printStudy(mintStudy, System.out);
    }
}
