package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class NormalizationTest {

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.err.println("Usage: NormalizationTest"
                    + " <study_input_dir> <study_output_dir>");
            System.exit(1);
        }
        testReadWriteStudy(Paths.get(args[0]), Paths.get(args[1]));
    }

    public static void testReadWriteStudy(Path studyPath, Path studyOutputPath)
            throws IOException {

        // Study study = TestUtils.readDicomStudy(studyPath, true);
        Study study = TestUtils.readDicomStudy(studyPath);
        TestUtils.printStudy(study, System.out);
        TestUtils.writeDicomStudy(study, studyOutputPath);
    }
}
