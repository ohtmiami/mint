package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.io.DefaultStudyStore;
import edu.jhmi.mint.io.DicomInputSource;
import edu.jhmi.mint.io.DicomOutputSink;
import edu.jhmi.mint.io.DicomStudyReader;
import edu.jhmi.mint.io.DicomStudyWriter;
import edu.jhmi.mint.io.MintEncodingOptions;
import edu.jhmi.mint.io.MintStudyReader;
import edu.jhmi.mint.io.MintStudyWriter;
import edu.jhmi.mint.io.MultiSeriesDicomReader;
import edu.jhmi.mint.io.MultiSeriesDicomWriter;
import edu.jhmi.mint.io.StudyPrinter;
import edu.jhmi.mint.io.StudyStore;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class TestUtils {

    private static StudyStore studyStore = null;

    public static StudyStore getStudyStore() throws IOException {

        if (studyStore == null) {
            studyStore = new DefaultStudyStore(
                    Paths.get("target/test-out/study-store"),true);
        }
        return studyStore;
    }

    public static Study readDicomStudy(Path studyDirPath, boolean ignoreFMI)
            throws IOException {

        StudyStore studyStore = getStudyStore();
        DicomStudyReader studyReader = new DicomStudyReader(studyStore);
        try (DicomInputSource inputSource = DicomInputSource
                .fromFolder(studyDirPath)) {
            return studyReader.readStudy(inputSource, ignoreFMI);
        }
    }

    public static Study readDicomStudy(Path studyDirPath) throws IOException {

        return readDicomStudy(studyDirPath, false);
    }

    public static void printStudy(Study study, PrintStream out) {

        StudyPrinter studyPrinter = new StudyPrinter();
        studyPrinter.printStudy(study, out);
    }

    public static void writeDicomStudy(Study study, Path outputDirPath)
            throws IOException {

        StudyStore studyStore = getStudyStore();
        DicomStudyWriter studyWriter = new DicomStudyWriter(studyStore);
        DicomOutputSink outputSink = DicomOutputSink.fromFolder(outputDirPath,
                false);
        studyWriter.writeStudy(study, outputSink);
    }

    public static Path writeMSDicomStudy(Study study, Path outputDirPath)
            throws IOException {

        Files.createDirectories(outputDirPath);
        String studyInstanceUID = study.getUID();
        Path studyFilePath = outputDirPath.resolve(String.format("%s.msdicom",
                studyInstanceUID));
        try (BufferedOutputStream bufferedOut = new BufferedOutputStream(
                Files.newOutputStream(studyFilePath))) {
            MultiSeriesDicomWriter.writeStudy(bufferedOut, study);
        }
        return studyFilePath;
    }

    public static Study readMSDicomStudy(Path studyFilePath) throws IOException {

        try (InputStream in = Files.newInputStream(studyFilePath)) {
            MultiSeriesDicomReader msdicomReader = new MultiSeriesDicomReader();
            return msdicomReader.readStudy(in);
        }
    }

    public static Path writeStudyMetadata(Study study, Path outputDirPath,
            MintEncodingOptions encodingOptions) throws IOException {

        Files.createDirectories(outputDirPath);
        String studyInstanceUID = study.getUID();
        Path metadataFilePath = outputDirPath.resolve(String.format("%s.mmd",
                studyInstanceUID));
        try (BufferedOutputStream bufferedOut = new BufferedOutputStream(
                Files.newOutputStream(metadataFilePath))) {
            MintStudyWriter.writeMetadata(bufferedOut, study, encodingOptions);
        }
        return metadataFilePath;
    }

    public static Path writeStudyMetadata(Study study, Path outputDirPath)
            throws IOException {

        return writeStudyMetadata(study, outputDirPath,
                MintEncodingOptions.METADATA_DEFAULT);
    }

    public static Study readStudyMetadata(Path metadataFilePath)
            throws IOException {

        try (InputStream in = Files.newInputStream(metadataFilePath)) {
            return MintStudyReader.readMetadata(in);
        }
    }
}
