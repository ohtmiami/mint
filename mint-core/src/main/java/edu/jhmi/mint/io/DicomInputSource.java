package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public abstract class DicomInputSource implements Closeable {

    public static final String DCM_EXTENSION_PATTERN = "*.[Dd][Cc][Mm]";

    public static final String ANY_EXTENSION_PATTERN = "*";

    public static final String DICOMDIR_PATTERN = "DICOMDIR";

    @Override
    public void close() throws IOException {
    }

    public abstract boolean hasNext() throws IOException;

    public abstract InputStream next() throws IOException;

    public abstract String currentInstanceURI();

    public static DicomInputSource fromFolder(Path studyDirPath,
            String globPattern) throws IOException {

        return new DefaultDicomInputSource(studyDirPath, globPattern);
    }

    public static DicomInputSource fromFolder(Path studyDirPath)
            throws IOException {

        return fromFolder(studyDirPath, ANY_EXTENSION_PATTERN);
    }

    public static DicomInputSource fromFiles(List<Path> paths)
            throws IOException {

        return new DicomFileList(paths);
    }

    private static class DefaultDicomInputSource extends DicomInputSource {

        private final PathMatcher matcher;

        private final PathMatcher dicomDirMatcher;

        private final Deque<DirectoryStream<Path>> dirStreams;

        private final Deque<Iterator<Path>> dirIterators;

        private Path nextDicomFilePath;

        private DefaultDicomInputSource(Path studyDirPath, String globPattern)
                throws IOException {

            matcher = FileSystems.getDefault().getPathMatcher(
                    "glob:" + globPattern);
            dicomDirMatcher = FileSystems.getDefault().getPathMatcher(
                    "glob:" + DICOMDIR_PATTERN);
            dirStreams = new ArrayDeque<DirectoryStream<Path>>(2);
            dirIterators = new ArrayDeque<Iterator<Path>>(2);
            DirectoryStream<Path> dirStream = Files
                    .newDirectoryStream(studyDirPath);
            dirStreams.push(dirStream);
            dirIterators.push(dirStream.iterator());
            nextDicomFilePath = null;
        }

        @Override
        public void close() throws IOException {

            nextDicomFilePath = null;
            dirIterators.clear();
            while (!dirStreams.isEmpty()) {
                dirStreams.pop().close();
            }
        }

        @Override
        public boolean hasNext() throws IOException {

            return hasNextRecursive();
        }

        private boolean hasNextRecursive() throws IOException {

            if (dirIterators.isEmpty()) {
                return false;
            }

            Iterator<Path> dirIterator = dirIterators.peek();
            while (dirIterator.hasNext()) {
                Path path = dirIterator.next();
                if (Files.isDirectory(path)) {
                    DirectoryStream<Path> dirStream = Files
                            .newDirectoryStream(path);
                    dirStreams.push(dirStream);
                    dirIterators.push(dirStream.iterator());
                    return hasNextRecursive();
                } else if (matcher.matches(path.getFileName())
                        && !dicomDirMatcher.matches(path.getFileName())) {
                    nextDicomFilePath = path;
                    return true;
                }
            }

            dirIterators.pop();
            dirStreams.pop().close();
            return hasNextRecursive();
        }

        @Override
        public InputStream next() throws IOException {

            if (nextDicomFilePath == null) {
                throw new NoSuchElementException();
            }
            return Files.newInputStream(nextDicomFilePath);
        }

        @Override
        public String currentInstanceURI() {

            return nextDicomFilePath == null ? null : nextDicomFilePath
                    .toString();
        }
    }

    private static class DicomFileList extends DicomInputSource {

        private final Iterator<Path> pathIterator;

        private Path currentDicomFilePath;

        private DicomFileList(List<Path> paths) {

            pathIterator = paths.iterator();
            currentDicomFilePath = null;
        }

        @Override
        public void close() throws IOException {

            currentDicomFilePath = null;
        }

        @Override
        public boolean hasNext() throws IOException {

            return pathIterator.hasNext();
        }

        @Override
        public InputStream next() throws IOException {

            currentDicomFilePath = pathIterator.next();
            return Files.newInputStream(currentDicomFilePath);
        }

        @Override
        public String currentInstanceURI() {

            return currentDicomFilePath == null ? null : currentDicomFilePath
                    .toString();
        }
    }
}
