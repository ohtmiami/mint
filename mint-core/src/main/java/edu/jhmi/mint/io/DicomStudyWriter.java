package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.UUID;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.Tag;
import org.dcm4che.data.UID;
import org.dcm4che.data.VR;
import org.dcm4che.data.Value;
import org.dcm4che.io.DicomEncodingOptions;
import org.dcm4che.io.DicomOutputStream;

import edu.jhmi.mint.data.BulkdataReference;
import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.NormalizedDataSet;
import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class DicomStudyWriter {

    private static final byte[] DICOM_FMI_VERSION = { 0, 1 };

    private final StudyStore studyStore;

    public DicomStudyWriter(StudyStore studyStore) {

        if (studyStore == null) {
            throw new NullPointerException("studyStore");
        }
        this.studyStore = studyStore;
    }

    public void writeStudy(Study study, DicomOutputSink outputSink)
            throws IOException {

        String studyInstanceUID = study.getUID();
        for (Series series : study) {
            LoadedBulkdata loadedBulkdata = new LoadedBulkdata(studyInstanceUID);
            for (Instance instance : series) {
                String transferSyntaxUID = instance.fmi()
                        .getStringHierarchical(Tag.TransferSyntaxUID);
                if (transferSyntaxUID == null
                        || transferSyntaxUID.equals(UID.ImplicitVRLittleEndian)
                        || transferSyntaxUID.equals(UID.ExplicitVRBigEndian)) {
                    transferSyntaxUID = UID.ExplicitVRLittleEndian;
                }
                try (OutputStream out = outputSink.getOutputStream(
                        studyInstanceUID, series.getUID(), instance.getUID());
                        DicomOutputStream dicomOut = new DicomOutputStream(out,
                                transferSyntaxUID)) {
                    NormalizedDataSet fmi = NormalizedDataSet.newItem(study
                            .fmi().size()
                            + series.fmi().size()
                            + instance.fmi().size());
                    fmi.insertAll(study.fmi());
                    fmi.insertAll(series.fmi());
                    fmi.insertAll(instance.fmi());

                    NormalizedDataSet dataSet = NormalizedDataSet.newItem(study
                            .dataSet().size()
                            + series.dataSet().size()
                            + instance.dataSet().size());
                    dataSet.insertAll(study.dataSet());
                    dataSet.insertAll(series.dataSet());
                    dataSet.insertAll(instance.dataSet());
                    restoreBulkdata(dataSet, loadedBulkdata);

                    if (!fmi.contains(Tag.FileMetaInformationVersion)) {
                        fmi.setBytes(Tag.FileMetaInformationVersion, VR.OB,
                                DICOM_FMI_VERSION);
                    }
                    String sopClassUID = dataSet.getString(Tag.SOPClassUID);
                    fmi.setString(Tag.MediaStorageSOPClassUID, VR.UI,
                            sopClassUID);
                    fmi.setString(Tag.MediaStorageSOPInstanceUID, VR.UI,
                            instance.getUID());
                    fmi.setString(Tag.TransferSyntaxUID, VR.UI,
                            transferSyntaxUID);

                    dicomOut.writeFileMetaInformation(fmi);
                    dataSet.writeTo(dicomOut);
                }
            }
        }
    }

    private void restoreBulkdata(Attributes dataSet,
            LoadedBulkdata loadedBulkdata) throws IOException {

        for (int i = 0; i < dataSet.size(); i++) {
            Object value = dataSet.valueAt(i);
            if (value instanceof BulkdataReference) {
                BulkdataReference bdr = (BulkdataReference) value;
                ByteBuffer valueBuffer;
                if (loadedBulkdata.fileUUID != null
                        && !loadedBulkdata.fileUUID.equals(bdr.getFileUUID())) {
                    valueBuffer = studyStore.getBulkdataValue(
                            loadedBulkdata.studyInstanceUID, bdr);
                } else {
                    if (loadedBulkdata.fileUUID == null) {
                        loadedBulkdata.fileUUID = bdr.getFileUUID();
                        loadedBulkdata.dataBuffer = studyStore.getBulkdata(
                                loadedBulkdata.studyInstanceUID,
                                loadedBulkdata.fileUUID);
                    }
                    valueBuffer = loadedBulkdata.dataBuffer.duplicate();
                    valueBuffer.position(bdr.getValueOffset());
                    valueBuffer.limit(bdr.getValueLimit() + 1);
                }
                // TODO Avoid extra copying of bulkdata
                final byte[] valueBytes;
                if (valueBuffer.hasArray()
                        && valueBuffer.remaining() == valueBuffer.capacity()) {
                    valueBytes = valueBuffer.array();
                } else {
                    valueBytes = new byte[valueBuffer.remaining()];
                    valueBuffer.get(valueBytes);
                }
                int tag = dataSet.tagAt(i);
                if (bdr.isFragments()) {
                    dataSet.setValue(tag, bdr.getVR(), new Value() {

                        @Override
                        public boolean isEmpty() {

                            return false;
                        }

                        @Override
                        public byte[] toBytes(VR vr, boolean bigEndian)
                                throws IOException {

                            return valueBytes;
                        }

                        @Override
                        public void writeTo(DicomOutputStream out, VR vr)
                                throws IOException {

                            out.write(valueBytes);
                        }

                        @Override
                        public int calcLength(DicomEncodingOptions encOpts,
                                boolean explicitVR, VR vr) {

                            return valueBytes.length;
                        }

                        @Override
                        public int getEncodedLength(
                                DicomEncodingOptions encOpts,
                                boolean explicitVR, VR vr) {

                            return -1;
                        }
                    });
                } else {
                    dataSet.setBytes(tag, bdr.getVR(), valueBytes);
                }
            } else if (value instanceof Sequence) {
                Sequence sequence = (Sequence) value;
                for (Attributes nestedDataSet : sequence) {
                    restoreBulkdata(nestedDataSet, loadedBulkdata);
                }
            }
        }
    }

    private static class LoadedBulkdata {

        private final String studyInstanceUID;

        private UUID fileUUID;

        private ByteBuffer dataBuffer;

        private LoadedBulkdata(String studyInstanceUID) {

            this.studyInstanceUID = studyInstanceUID;
            fileUUID = null;
            dataBuffer = null;
        }
    }
}
