package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.crypto.AEADBadTagException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Fragments;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.data.Value;
import org.dcm4che.io.DicomEncodingOptions;
import org.dcm4che.util.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.jhmi.mint.data.BulkdataReference;
import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class DefaultStudyStore implements StudyStore {

    private interface BulkdataSource {

        VR getVR();

        boolean isFragments();

        int getValueLength();

        void writeTo(ByteBuffer mappedBuffer);

        void writeTo(FileChannel fileChannel) throws IOException;
    }

    private static final Logger LOG = LoggerFactory
            .getLogger(DefaultStudyStore.class);

    private static final String BULKDATA_FILE_EXT = "mbd";

    private static final String UNFINISHED_BULKDATA_FILE_EXT = "mbd.unfinished";

    private static final String OBSOLETE_BULKDATA_FILE_EXT = "mbd.obsolete";

    private final Path rootDir;

    private boolean writeBulkData;

    public DefaultStudyStore(Path rootDir, boolean writeBulkData)
            throws IOException {

        this.rootDir = rootDir == null ? Paths.get(
                System.getProperty("java.io.tmpdir"), "study-store") : rootDir;
        if (!Files.exists(rootDir)) {
            // If rootDir exists and is a symbolic link, createDirectories() may
            // throw a FileAlreadyExistsException
            Files.createDirectories(this.rootDir);
        }
        this.writeBulkData = writeBulkData;
    }

    private static boolean useMappedFiles() {

        return System.getProperty("mint.io.useMappedFiles") != null;
    }

    @Override
    public ByteBuffer getBulkdataValue(String studyInstanceUID,
            BulkdataReference bdr) throws IOException {

        Path studyDir = rootDir.resolve(studyInstanceUID);
        Path bulkdataFile = studyDir.resolve(String.format("%s.%s",
                bdr.getFileUUID(), BULKDATA_FILE_EXT));
        try (FileChannel fileIn = FileChannel.open(bulkdataFile,
                StandardOpenOption.READ)) {
            MintHeader header = new MintHeader();
            ByteBuffer encodedMV = ByteBuffer.allocate(MintHeader.getMVSize());
            fileIn.read(encodedMV);
            encodedMV.flip();
            MintHeader.decodeVersion(encodedMV);
            ByteBuffer encodedHeader = ByteBuffer.allocate(MintHeader
                    .getMaxSize());
            encodedHeader.limit(MintHeader.getPeekSize());
            fileIn.read(encodedHeader);
            encodedHeader.flip();
            int headerLength = MintHeader.peek(encodedHeader);
            encodedHeader.limit(MintHeader.getPeekSize() + headerLength);
            fileIn.read(encodedHeader);
            encodedHeader.position(MintHeader.getPeekSize());
            header.decode(encodedHeader, headerLength);

            ByteBuffer value = ByteBuffer.allocate(bdr.getValueLength());
            if (header.isEncrypted()) {
                MintHeaderOption<?> option = header.getOption(MintHeaderOption
                        .getIVBitMask());
                if (option == null) {
                    throw new MalformedMintHeaderException("Missing IV option");
                }
                byte[] ivBytes = (byte[]) option.getValue();
                StudyCipher studyCipher = null;
                if (MintHeader.isVersion21()) {
                    studyCipher = new StudyCipher((String) header.getOption(
                            MintHeaderOption.getIVBitMask()).getValue());
                } else if (MintHeader.isVersion20()) {
                    studyCipher = new StudyCipher("AES-GCM-256");
                }
                Cipher decrypter = studyCipher.getDecryptingCipher(ivBytes, 0,
                        studyInstanceUID, MintHeader.getMajorVersion(),
                        MintHeader.getMinorVersion());

                ByteBuffer aad = encodedHeader.duplicate();
                aad.position(0);
                aad.limit(MintHeader.getPeekSize() + headerLength);
                decrypter.updateAAD(aad);

                ByteBuffer tempInBuffer = ByteBuffer.allocate(512 * 1024);
                ByteBuffer tempOutBuffer = null;
                int outputOffset = (int) fileIn.position();
                int valueOffset = bdr.getValueOffset();
                int valueEnd = valueOffset + bdr.getValueLength();
                while (true) {
                    tempInBuffer.clear();
                    int numBytesRead = fileIn.read(tempInBuffer);
                    if (numBytesRead == -1) {
                        ByteBuffer decrypted = tempOutBuffer == null
                                || tempOutBuffer.capacity() < tempInBuffer
                                        .capacity() ? tempInBuffer.duplicate()
                                : tempOutBuffer;
                        int outputSize = decrypter.getOutputSize(0);
                        if (decrypted.capacity() < outputSize) {
                            decrypted = ByteBuffer.allocate(outputSize);
                        } else {
                            decrypted.clear();
                        }
                        tempInBuffer.limit(0);
                        try {
                            decrypter.doFinal(tempInBuffer, decrypted);
                        } catch (ShortBufferException e) {
                            throw new RuntimeException("Unexpected error", e);
                        } catch (IllegalBlockSizeException e) {
                            // Does not apply to decryption
                            throw new RuntimeException("Unexpected error", e);
                        } catch (AEADBadTagException e) {
                            throw new IOException("Failed to decrypt", e);
                        } catch (BadPaddingException e) {
                            // Does not apply to GCM/NoPadding
                            throw new RuntimeException("Unexpected error", e);
                        }
                        decrypted.flip();
                        int outputEnd = outputOffset + decrypted.remaining();
                        if (outputOffset <= valueOffset
                                && outputEnd > valueOffset) {
                            decrypted.position(valueOffset - outputOffset);
                            if (outputEnd > valueEnd) {
                                decrypted.limit(valueEnd - outputOffset);
                            }
                            value.put(decrypted);
                        }
                        break;
                    }

                    tempInBuffer.flip();
                    ByteBuffer decrypted = tempInBuffer.duplicate();
                    decrypted.clear();
                    try {
                        decrypter.update(tempInBuffer, decrypted);
                    } catch (ShortBufferException e) {
                        int outputSize = decrypter.getOutputSize(tempInBuffer
                                .remaining());
                        if (tempOutBuffer == null) {
                            tempOutBuffer = ByteBuffer.allocate(outputSize);
                        } else if (tempOutBuffer.capacity() < outputSize) {
                            int newCapacity = tempOutBuffer.capacity() * 2;
                            if (newCapacity < outputSize) {
                                newCapacity = outputSize;
                            }
                            tempOutBuffer = ByteBuffer.allocate(newCapacity);
                        } else {
                            tempOutBuffer.clear();
                        }
                        decrypted = tempOutBuffer;
                        try {
                            decrypter.update(tempInBuffer, decrypted);
                        } catch (ShortBufferException e1) {
                            throw new RuntimeException("Unexpected error", e1);
                        }
                    }
                    decrypted.flip();
                    int outputEnd = outputOffset + decrypted.remaining();
                    if (outputOffset <= valueOffset && outputEnd > valueOffset) {
                        decrypted.position(valueOffset - outputOffset);
                        if (outputEnd > valueEnd) {
                            decrypted.limit(valueEnd - outputOffset);
                            valueOffset = valueEnd;
                        } else {
                            valueOffset = outputEnd;
                        }
                        value.put(decrypted);
                    }
                    outputOffset = outputEnd;
                }
            } else {
                fileIn.read(value, bdr.getValueOffset());
            }
            value.flip();
            return value;
        }
    }

    @Override
    public ByteBuffer getBulkdata(String studyInstanceUID, UUID fileUUID)
            throws IOException {
        Path studyDir = rootDir.resolve(studyInstanceUID);
        Path bulkdataFile = studyDir.resolve(String.format("%s.%s", fileUUID,
                BULKDATA_FILE_EXT));
        int fileSize = (int) Files.size(bulkdataFile);
        ByteBuffer fileBuffer = ByteBuffer.allocate(fileSize);
        try (FileChannel fileIn = FileChannel.open(bulkdataFile,
                StandardOpenOption.READ)) {
            int numBytesRead = -1;
            do {
                numBytesRead = fileIn.read(fileBuffer);
            } while (numBytesRead > 0);
            fileBuffer.flip();
        }

        MintHeader header = new MintHeader();
        ByteBuffer encodedHeader = fileBuffer.duplicate();
        encodedHeader.limit(MintHeader.getMaxSize());
        header.decode(encodedHeader);
        encodedHeader.flip();

        ByteBuffer dataBuffer = fileBuffer.duplicate();
        if (header.isEncrypted()) {
            MintHeaderOption<?> option = header.getOption(MintHeaderOption
                    .getIVBitMask());
            if (option == null) {
                throw new MalformedMintHeaderException("Missing IV option");
            }
            byte[] ivBytes = (byte[]) option.getValue();
            StudyCipher studyCipher = null;
            if (MintHeader.isVersion21()) {
                studyCipher = new StudyCipher((String) header.getOption(
                        MintHeaderOption.getEncryptedBitMask()).getValue());
            }else if(MintHeader.isVersion20()){
                studyCipher=new StudyCipher("AES-GCM-256");
            }
            Cipher decrypter = studyCipher.getDecryptingCipher(ivBytes, 0,
                    studyInstanceUID, MintHeader.getMajorVersion(),
                    MintHeader.getMinorVersion());

            ByteBuffer aad = encodedHeader.duplicate();
            decrypter.updateAAD(aad);
            int headerLength = encodedHeader.remaining();
            fileBuffer.position(headerLength);
            dataBuffer.position(headerLength);
            try {
                decrypter.doFinal(fileBuffer, dataBuffer);
            } catch (ShortBufferException e) {
                throw new RuntimeException("Unexpected error", e);
            } catch (IllegalBlockSizeException e) {
                // Does not apply to decryption
                throw new RuntimeException("Unexpected error", e);
            } catch (AEADBadTagException e) {
                throw new IOException("Failed to decrypt", e);
            } catch (BadPaddingException e) {
                // Does not apply to GCM/NoPadding
                throw new RuntimeException("Unexpected error", e);
            }
            dataBuffer.flip();
        }
        return dataBuffer;
    }

    @Override
    public BulkdataReference putBulkdataValue(String studyInstanceUID,
            final ByteBuffer value, final VR vr) throws IOException {
        if (writeBulkData) {
            return put(studyInstanceUID, new BulkdataSource() {

                @Override
                public VR getVR() {

                    return vr;
                }

                @Override
                public boolean isFragments() {

                    return false;
                }

                @Override
                public int getValueLength() {

                    return value.remaining();
                }

                @Override
                public void writeTo(ByteBuffer mappedBuffer) {

                    mappedBuffer.put(value);
                }

                @Override
                public void writeTo(FileChannel fileChannel) throws IOException {

                    fileChannel.write(value);
                }
            });
        }
        return null;
    }

    @Override
    public BulkdataReference putFragments(String studyInstanceUID,
            final Fragments fragments) throws IOException {

        return put(studyInstanceUID, new BulkdataSource() {

            @Override
            public VR getVR() {

                return fragments.vr();
            }

            @Override
            public boolean isFragments() {

                return true;
            }

            @Override
            public int getValueLength() {

                return fragments.calcLength(DicomEncodingOptions.DEFAULT, true,
                        fragments.vr());
            }

            @Override
            public void writeTo(ByteBuffer mappedBuffer) {

                byte[] tagBytes = new byte[4];
                ByteUtils.tagToBytesLE(Tag.Item, tagBytes, 0);
                mappedBuffer.order(ByteOrder.LITTLE_ENDIAN);
                for (Object fragment : fragments) {
                    mappedBuffer.put(tagBytes);
                    if (fragment == Value.NULL) {
                        mappedBuffer.putInt(0);
                    } else {
                        // TODO Allow fragment to be ByteBuffer?
                        byte[] fragmentBytes = (byte[]) fragment;
                        mappedBuffer.putInt(fragmentBytes.length);
                        mappedBuffer.put(fragmentBytes);
                    }
                }
            }

            @Override
            public void writeTo(FileChannel fileChannel) throws IOException {

                ByteBuffer tagBuffer = ByteBuffer.allocate(4);
                ByteUtils.tagToBytesLE(Tag.Item, tagBuffer.array(), 0);
                ByteBuffer lengthBuffer = ByteBuffer.allocate(4);
                for (Object fragment : fragments) {
                    tagBuffer.clear();
                    fileChannel.write(tagBuffer);
                    lengthBuffer.clear();
                    if (fragment == Value.NULL) {
                        ByteUtils.intToBytesLE(0, lengthBuffer.array(), 0);
                        fileChannel.write(lengthBuffer);
                    } else {
                        // TODO Allow fragment to be ByteBuffer?
                        byte[] fragmentBytes = (byte[]) fragment;
                        ByteUtils.intToBytesLE(fragmentBytes.length,
                                lengthBuffer.array(), 0);
                        fileChannel.write(lengthBuffer);
                        fileChannel.write(ByteBuffer.wrap(fragmentBytes));
                    }
                }
            }
        });
    }

    private BulkdataReference put(String studyInstanceUID,
            BulkdataSource bulkdataSource) throws IOException {

        Path unfinishedFile = findUnfinishedBulkdataFile(studyInstanceUID);
        long currentFileSize;
        if (!Files.exists(unfinishedFile)) {
            Files.createDirectories(unfinishedFile.getParent());
            currentFileSize = 0;
        } else {
            currentFileSize = Files.size(unfinishedFile);
        }
        UUID fileUUID;
        int valueOffset;
        int valueLength = bulkdataSource.getValueLength();
        if (currentFileSize + valueLength > Integer.MAX_VALUE) {
            finishBulkdataFile(unfinishedFile);
            fileUUID = UUID.randomUUID();
            unfinishedFile = unfinishedFile.resolveSibling(String.format(
                    "%s.%s", fileUUID, UNFINISHED_BULKDATA_FILE_EXT));
            valueOffset = 0;
        } else {
            String unfinishedFileName = unfinishedFile.getFileName().toString();
            fileUUID = UUID.fromString(unfinishedFileName.substring(
                    0,
                    unfinishedFileName.length()
                            - UNFINISHED_BULKDATA_FILE_EXT.length() - 1));
            valueOffset = (int) currentFileSize;
        }

        boolean useMappedFiles = useMappedFiles();
        StandardOpenOption[] openOptions = {
                useMappedFiles ? StandardOpenOption.READ
                        : StandardOpenOption.APPEND, StandardOpenOption.WRITE,
                StandardOpenOption.CREATE };
        try (FileChannel fileOut = FileChannel
                .open(unfinishedFile, openOptions)) {
            if (valueOffset == 0) {
                MintHeader header = createBulkdataHeader(studyInstanceUID,
                        fileUUID, null);
                ByteBuffer encodedHeader = header.encode();
                valueOffset = encodedHeader.remaining();
                fileOut.write(encodedHeader);
            }
            if (useMappedFiles) {
                ByteBuffer mappedBuffer = fileOut.map(MapMode.READ_WRITE,
                        valueOffset, valueLength);
                bulkdataSource.writeTo(mappedBuffer);
            } else {
                bulkdataSource.writeTo(fileOut);
            }
        }
        return new BulkdataReference(bulkdataSource.getVR(),
                bulkdataSource.isFragments(), fileUUID, valueOffset,
                valueOffset + valueLength - 1);
    }

    private Path findUnfinishedBulkdataFile(String studyInstanceUID)
            throws IOException {

        Path studyDir = rootDir.resolve(studyInstanceUID);
        if (Files.exists(studyDir)) {
            try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
                    studyDir,
                    String.format("*.%s", UNFINISHED_BULKDATA_FILE_EXT))) {
                for (Path unfinishedFile : dirStream) {
                    // There should not be more than one match
                    return unfinishedFile;
                }
            }
        }
        return studyDir.resolve(String.format("%s.%s", UUID.randomUUID(),
                UNFINISHED_BULKDATA_FILE_EXT));
    }

    private Path finishBulkdataFile(Path unfinishedFile) throws IOException {

        String unfinishedFileName = unfinishedFile.getFileName().toString();
        if (!unfinishedFileName.endsWith(UNFINISHED_BULKDATA_FILE_EXT)) {
            throw new InvalidPathException(unfinishedFileName,
                    "Not an unfinished bulkdata file");
        }
        String finishedFileName = unfinishedFileName.substring(
                0,
                unfinishedFileName.length()
                        - UNFINISHED_BULKDATA_FILE_EXT.length())
                + BULKDATA_FILE_EXT;
        Path finishedFile = unfinishedFile.resolveSibling(finishedFileName);
        return Files.move(unfinishedFile, finishedFile);
    }

    @Override
    public void finishStudy(Study study) throws IOException {

        Path studyDir = rootDir.resolve(study.getUID());
        // There may not be any bulkdata
        if (Files.exists(studyDir)) {
            try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
                    studyDir,
                    String.format("*.%s", UNFINISHED_BULKDATA_FILE_EXT))) {
                for (Path unfinishedFile : dirStream) {
                    finishBulkdataFile(unfinishedFile);
                }
            }
        }
    }

    @Override
    public void reorganizeBulkdata(Study study, String encryptedAlgorithm,
            boolean replace) throws IOException {

        String studyInstanceUID = study.getUID();
        // There rarely are more than two unorganized bulkdata files
        Set<UUID> referencedFileUUIDs = new HashSet<UUID>(2);
        List<BulkdataReference> bdrs = new ArrayList<BulkdataReference>();
        long totalSize = findAllBulkdataReferences(study.dataSet(), bdrs,
                referencedFileUUIDs);
        if (totalSize > Integer.MAX_VALUE) {
            List<List<BulkdataReference>> bdrGroups = groupBulkdataReferences(
                    bdrs, Integer.MAX_VALUE);
            for (List<BulkdataReference> bdrGroup : bdrGroups) {
                createSeparateBulkdataFileFor(studyInstanceUID, bdrGroup,
                        encryptedAlgorithm);
            }
        } else {
            createSeparateBulkdataFileFor(studyInstanceUID, bdrs,
                    encryptedAlgorithm);
        }

        for (Series series : study) {
            bdrs.clear();
            totalSize = findAllBulkdataReferences(series.dataSet(), bdrs,
                    referencedFileUUIDs);
            for (Instance instance : series) {
                totalSize += findAllBulkdataReferences(instance.dataSet(),
                        bdrs, referencedFileUUIDs);
            }
            if (totalSize > Integer.MAX_VALUE) {
                List<List<BulkdataReference>> bdrGroups = groupBulkdataReferences(
                        bdrs, Integer.MAX_VALUE);
                for (List<BulkdataReference> bdrGroup : bdrGroups) {
                    createSeparateBulkdataFileFor(studyInstanceUID, bdrGroup,
                            encryptedAlgorithm);
                }
            } else {
                createSeparateBulkdataFileFor(studyInstanceUID, bdrs,
                        encryptedAlgorithm);
            }
        }

        if (replace) {
            Path studyDir = rootDir.resolve(studyInstanceUID);
            for (UUID referencedFileUUID : referencedFileUUIDs) {
                Path referencedFile = studyDir.resolve(String.format("%s.%s",
                        referencedFileUUID, BULKDATA_FILE_EXT));
                try {
                    Files.delete(referencedFile);
                } catch (IOException e) {
                    LOG.warn("Failed to delete bulkdata file {}: {}",
                            referencedFile, e.getMessage());
                    Path obsoleteFile = referencedFile.resolveSibling(String
                            .format("%s.%s", referencedFileUUID,
                                    OBSOLETE_BULKDATA_FILE_EXT));
                    Files.move(referencedFile, obsoleteFile);
                    LOG.warn("Renamed {} to {}", referencedFile, obsoleteFile);
                }
            }
        }
    }

    private void createSeparateBulkdataFileFor(String studyInstanceUID,
            List<BulkdataReference> bdrs, String encryptedAlgorithm)
            throws IOException {

        if (bdrs.isEmpty()) {
            return;
        }

        UUID newUUID = UUID.randomUUID();
        MintHeader header = createBulkdataHeader(studyInstanceUID, newUUID,
                encryptedAlgorithm);

        Path studyDir = rootDir.resolve(studyInstanceUID);
        Path newFile = studyDir.resolve(String.format("%s.%s", newUUID,
                BULKDATA_FILE_EXT));
        Cipher encrypter = null;
        if (encryptedAlgorithm != null) {
            StudyCipher studyCipher = new StudyCipher(encryptedAlgorithm);
            byte[] ivBytes = studyCipher.generateIV();
            header.addOption(new MintIVOption(ivBytes));
            encrypter = studyCipher.getEncryptingCipher(ivBytes, 0,
                    studyInstanceUID, MintHeader.getMajorVersion(),
                    MintHeader.getMinorVersion());

            ByteBuffer aad = header.encode();
            encrypter.updateAAD(aad);
        }
        try (FileChannel fileOut = FileChannel.open(newFile,
                StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)) {
            ByteBuffer encodedHeader = header.encode();
            int valueOffset = encodedHeader.remaining();
            fileOut.write(encodedHeader);

            ByteBuffer tempBuffer = null;
            for (BulkdataReference bdr : bdrs) {
                ByteBuffer value = getBulkdataValue(studyInstanceUID, bdr);
                if (encrypter != null) {
                    ByteBuffer encryptedValue = value.duplicate();
                    try {
                        encrypter.update(value, encryptedValue);
                    } catch (ShortBufferException e) {
                        // Could not encrypt in place
                        int outputSize = encrypter.getOutputSize(value
                                .remaining());
                        if (tempBuffer == null) {
                            tempBuffer = ByteBuffer.allocate(outputSize);
                        } else if (tempBuffer.capacity() < outputSize) {
                            int newCapacity = tempBuffer.capacity() * 2;
                            if (newCapacity < outputSize) {
                                newCapacity = outputSize;
                            }
                            tempBuffer = ByteBuffer.allocate(newCapacity);
                        } else {
                            tempBuffer.clear();
                        }
                        try {
                            encrypter.update(value, tempBuffer);
                        } catch (ShortBufferException e1) {
                            throw new RuntimeException("Unexpected error", e1);
                        }
                        encryptedValue = tempBuffer;
                    }
                    encryptedValue.flip();
                    value = encryptedValue;
                }
                int valueLength = bdr.getValueLength();
                bdr.setFileUUID(newUUID);
                bdr.setValueOffset(valueOffset);
                bdr.setValueLimit(valueOffset + valueLength - 1);
                fileOut.write(value);
                valueOffset += valueLength;
            }
            if (encrypter != null) {
                try {
                    byte[] tailBytes = encrypter.doFinal();
                    fileOut.write(ByteBuffer.wrap(tailBytes));
                } catch (IllegalBlockSizeException e) {
                    // Does not apply to GCM, which accepts any input length
                    throw new RuntimeException("Unexpected error", e);
                } catch (BadPaddingException e) {
                    // Does not apply to encryption
                    throw new RuntimeException("Unexpected error", e);
                }
            }
        }
    }

    private static MintHeader createBulkdataHeader(String studyInstanceUID,
            UUID fileUUID, String encryptedAlgorithm) {
        MintHeader header = new MintHeader();
        header.setPayloadType(MintHeader.PayloadType.BULKDATA);
        if (MintHeader.isVersion21()) {
            header.addOption(new MintPayloadTypeOption(fileUUID));
        }
        header.addOption(new MintUUIDOption(fileUUID));
        header.addOption(new MintUIDOption(studyInstanceUID));
        if (encryptedAlgorithm != null) {
            header.setEncrypted(true);
            if (MintHeader.isVersion21()) {
                header.addOption(new MintEncryptedOption(encryptedAlgorithm));
            }
        } else {
            header.setEncrypted(false);
        }
        // TODO Should compression of bulkdata be supported?
        header.setCompressed(false);

        return header;
    }

    /*
     * private static MintHeader createBulkdataHeader(String studyInstanceUID,
     * UUID fileUUID, boolean encrypt) {
     * 
     * MintHeader header = new MintHeader();
     * header.setPayloadType(MintHeader.PayloadType.BULKDATA);
     * header.setEncrypted(encrypt); // TODO Should compression of bulkdata be
     * supported? header.setCompressed(false); header.addOption(new
     * MintUIDOption(studyInstanceUID)); header.addOption(new
     * MintUUIDOption(fileUUID)); return header; }
     */

    private static long findAllBulkdataReferences(Attributes dataSet,
            List<BulkdataReference> bdrs, Set<UUID> referencedFileUUIDs) {

        long totalSize = 0;
        for (int i = 0; i < dataSet.size(); i++) {
            Object value = dataSet.valueAt(i);
            if (value instanceof BulkdataReference) {
                BulkdataReference bdr = (BulkdataReference) value;
                referencedFileUUIDs.add(bdr.getFileUUID());
                totalSize += bdr.getValueLength();
                bdrs.add(bdr);
            } else if (value instanceof Sequence) {
                Sequence sequence = (Sequence) value;
                for (Attributes nestedDataSet : sequence) {
                    totalSize += findAllBulkdataReferences(nestedDataSet, bdrs,
                            referencedFileUUIDs);
                }
            }
        }
        return totalSize;
    }

    private static List<List<BulkdataReference>> groupBulkdataReferences(
            List<BulkdataReference> bdrs, int sizeThreshold) {

        // We rarely need more than two groups
        List<List<BulkdataReference>> bdrGroups = new ArrayList<List<BulkdataReference>>(
                2);
        int numUngrouped = bdrs.size();
        List<BulkdataReference> bdrGroup = new ArrayList<BulkdataReference>(
                numUngrouped);
        bdrGroups.add(bdrGroup);
        long groupSize = 0;
        for (BulkdataReference bdr : bdrs) {
            groupSize += bdr.getValueLength();
            if (groupSize > sizeThreshold) {
                groupSize = 0;
                bdrGroup = new ArrayList<BulkdataReference>(numUngrouped);
                bdrGroups.add(bdrGroup);
            }
            bdrGroup.add(bdr);
            --numUngrouped;
        }
        return bdrGroups;
    }
}
