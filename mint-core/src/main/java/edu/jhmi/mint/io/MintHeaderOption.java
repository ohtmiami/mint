package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.nio.ByteBuffer;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 */
public abstract class MintHeaderOption<T> {

    private static final int BITMASK_UID_20 = 0x00000002;

    static final int BITMASK_IV_20 = 0x00000001;

    private static final int BITMASK_UUID_20 = 0x00000004;

    private static final int BITMASK_METADATA_21 = 0x00000001;

    private static final int BITMASK_UUID_21 = 0x00000002;

    private static final int BITMASK_UID_21 = 0x00000004;

    private static final int BITMASK_COMPRESSED_21 = 0x00000008;

    private static final int BITMASK_ENCRYPTED_21 = 0x00000010;

    private static final int BITMASK_IV_21 = 0x00000020;

    private T value;

    public T getValue() {

        return value;
    }

    public void setValue(T value) {

        this.value = value;
    }

    public abstract int getBitmask();

    public abstract int calculateLength();

    public void encode(ByteBuffer buffer) {

        // Length (2 bytes)
        int length = calculateLength() & 0xFF;
        buffer.putShort((short) length);
        encodeValue(buffer);
    }

    protected abstract void encodeValue(ByteBuffer buffer);

    public static int getUIDBitMask() {

        return MintHeader.isVersion20() ? BITMASK_UID_20 : BITMASK_UID_21;
    }

    public static int getUUIDBitMask() {

        return MintHeader.isVersion20() ? BITMASK_UUID_20 : BITMASK_UUID_21;
    }

    public static int getIVBitMask() {

        return MintHeader.isVersion20() ? BITMASK_IV_20 : BITMASK_IV_21;
    }

    public static int getMetadataBitMask() {

        return BITMASK_METADATA_21;
    }

    public static int getEncryptedBitMask() {

        return BITMASK_ENCRYPTED_21;
    }

    public static int getCompressedBitMask() {

        return BITMASK_COMPRESSED_21;
    }

    public void decode(ByteBuffer buffer) throws MalformedMintHeaderException {

        // Length (2 bytes)
        int length = buffer.getShort() & 0xFF;
        decodeValue(buffer, length);
    }

    protected abstract void decodeValue(ByteBuffer buffer, int length)
            throws MalformedMintHeaderException;

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + (value == null ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MintHeaderOption)) {
            return false;
        }
        MintHeaderOption<?> other = (MintHeaderOption<?>) obj;
        if (value == null) {
            return other.value == null;
        }
        return value.equals(other.value);
    }
}
