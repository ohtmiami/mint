package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;

import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintStudyWriter {

    public static void writeMetadata(OutputStream out, Study study,
            MintEncodingOptions encodingOptions) throws IOException {

        MintHeader header = new MintHeader();
        header.setPayloadType(MintHeader.PayloadType.METADATA);
        header.setEncrypted(encodingOptions.encrypt);
        header.setCompressed(encodingOptions.compress);
        header.addOption(new MintUIDOption(study.getUID()));

        OutputStream wrappedOut = out;
        Cipher encrypter = null;
        Deflater compressor = null;
        if (encodingOptions.compress && MintHeader.isVersion21()) {
            header.addOption(new MintCompressedOption(
                    encodingOptions.compressedAlgorithm));
        }
        if (encodingOptions.encrypt && MintHeader.isVersion21()) {
            header.addOption(new MintEncryptedOption(
                    encodingOptions.encryptedAlgorithm));
        }
        if (encodingOptions.encrypt) {
            StudyCipher studyCipher = new StudyCipher(
                    encodingOptions.encryptedAlgorithm);
            byte[] ivBytes = studyCipher.generateIV();

            header.addOption(new MintIVOption(ivBytes));
            encrypter = studyCipher.getEncryptingCipher(ivBytes, 0,
                    study.getUID(), MintHeader.getMajorVersion(),
                    MintHeader.getMinorVersion());

            ByteBuffer aad = header.encode();
            encrypter.updateAAD(aad);

            wrappedOut = new CipherOutputStream(wrappedOut, encrypter);
        }
        if (encodingOptions.compress) {
            compressor = new Deflater(encodingOptions.compressionLevel);
            wrappedOut = new DeflaterOutputStream(wrappedOut, compressor);
        }

        ByteBuffer encodedHeader = header.encode();
        out.write(encodedHeader.array(), encodedHeader.arrayOffset(),
                encodedHeader.remaining());
        MultiSeriesDicomWriter.writeStudy(wrappedOut, study,
                encodingOptions.writeMSDicomFMI,
                encodingOptions.useExplicitLengths);
        if (compressor != null) {
            ((DeflaterOutputStream) wrappedOut).finish();
            compressor.end();
        }
        if (encrypter != null) {
            byte[] tailBytes = null;
            try {
                tailBytes = encrypter.doFinal();
            } catch (IllegalBlockSizeException e) {
                // Does not apply to GCM, which accepts any input length
                throw new RuntimeException("Unexpected error", e);
            } catch (BadPaddingException e) {
                // Does not apply to encryption
                throw new RuntimeException("Unexpected error", e);
            }
            if (tailBytes != null) {
                out.write(tailBytes);
            }
            out.flush();
        }
    }
}
