package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Tag;
import org.dcm4che.io.BulkDataDescriptor;
import org.dcm4che.io.DicomInputStream;
import org.dcm4che.io.DicomStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class DicomStudyReader {

	private static final Logger LOG = LoggerFactory
			.getLogger(DicomStudyReader.class);

	private final StudyStore studyStore;

	private BulkDataDescriptor bulkdataDescriptor;

	private long normalizationTimeMillis;

	public DicomStudyReader(StudyStore studyStore) {

		if (studyStore == null) {
			throw new NullPointerException("studyStore");
		}
		this.studyStore = studyStore;
		bulkdataDescriptor = new BasicBulkdataDescriptor();
	}

	public final BulkDataDescriptor getBulkdataDescriptor() {

		return bulkdataDescriptor;
	}

	public final void setBulkdataDescriptor(
			BulkDataDescriptor bulkdataDescriptor) {

		if (bulkdataDescriptor == null) {
			throw new NullPointerException("bulkdataDescriptor");
		}
		this.bulkdataDescriptor = bulkdataDescriptor;
	}

	public Study readStudy(DicomInputSource inputSource, boolean ignoreFMI)
			throws IOException {

		long normalizationStartMillis = 0;
		if (LOG.isTraceEnabled()) {
			normalizationTimeMillis = 0;
		}
		Study study = null;
		while (inputSource.hasNext()) {
			try (InputStream in = inputSource.next();
					DicomInputStream dicomIn = new DicomInputStream(in)) {
				InstanceInputHandler handler = new InstanceInputHandler(
						studyStore);
				dicomIn.setDicomInputHandler(handler);
				dicomIn.setBulkDataDescriptor(bulkdataDescriptor);

				Attributes dataSet = dicomIn.readDataset(-1, -1);
				Attributes fmi = dicomIn.readFileMetaInformation();
				if (study == null) {
					String studyInstanceUID = dataSet
							.getString(Tag.StudyInstanceUID);
					study = new Study(studyInstanceUID);
				}
				if (LOG.isTraceEnabled()) {
					normalizationStartMillis = System.currentTimeMillis();
				}
				boolean added;
				if (ignoreFMI) {
					String transferSyntaxUID = fmi.getString(
							Tag.TransferSyntaxUID, dicomIn.getTransferSyntax());
					added = study.addSOPInstance(transferSyntaxUID, dataSet) != null;
				} else {
					if (fmi == null) {
						added = study.addSOPInstance(dataSet) != null;
					} else {
						added = study.addSOPInstance(fmi, dataSet) != null;
					}
				}
				if (!added) {
					LOG.warn(String.format("Skipped instance %s",
							dataSet.getString(Tag.SOPInstanceUID)));
				}
				if (LOG.isTraceEnabled()) {
					normalizationTimeMillis += System.currentTimeMillis()
							- normalizationStartMillis;
				}
			} catch (DicomStreamException e) {
				LOG.warn(String.format("Skipped non-DICOM input %s",
						inputSource.currentInstanceURI()));
			}
		}

		if (study != null) {
			studyStore.finishStudy(study);
		}
		return study;
	}

	public Study readStudy(DicomInputSource inputSource) throws IOException {

		return readStudy(inputSource, false);
	}

	public static void main(String[] args) throws IOException {

		if (args.length < 2) {
			System.err.println("Usage: DicomStudyReader"
					+ " <study_input_dir> <output_dir>");
			System.exit(1);
		}

		Path studyDirPath = Paths.get(args[0]);
		Path outputDirPath = Paths.get(args[1]);
		StudyStore studyStore = new DefaultStudyStore(outputDirPath, true);
		DicomStudyReader studyReader = new DicomStudyReader(studyStore);
		Study study = null;
		long readTimeMillis = 0;
		try (DicomInputSource inputSource = DicomInputSource
				.fromFolder(studyDirPath)) {
			long readStartMillis = System.currentTimeMillis();
			study = studyReader.readStudy(inputSource);
			readTimeMillis = System.currentTimeMillis() - readStartMillis;
		}
		if (study != null) {
			System.out.printf("%d\t%d", studyReader.normalizationTimeMillis,
					readTimeMillis);
		}
	}
}
