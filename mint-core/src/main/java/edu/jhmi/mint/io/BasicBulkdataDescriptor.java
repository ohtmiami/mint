package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.List;

import org.dcm4che.data.ItemPointer;
import org.dcm4che.data.VR;
import org.dcm4che.io.BulkDataDescriptor;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class BasicBulkdataDescriptor extends BulkDataDescriptor {

    private int valueLengthThreshold = 256;

    public final int getValueLengthThreshold() {

        return valueLengthThreshold;
    }

    public final void setValueLengthThreshold(int valueLengthThreshold) {

        this.valueLengthThreshold = valueLengthThreshold;
    }

    @Override
    public boolean isBulkData(List<ItemPointer> itemPointer,
            String privateCreator, int tag, VR vr, int length) {

        boolean isFragments = length == -1 && vr != VR.SQ;
        return length > valueLengthThreshold || isFragments;
    }
}
