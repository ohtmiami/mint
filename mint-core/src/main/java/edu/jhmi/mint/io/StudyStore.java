package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.UUID;

import org.dcm4che.data.Fragments;
import org.dcm4che.data.VR;

import edu.jhmi.mint.data.BulkdataReference;
import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public interface StudyStore {

    ByteBuffer getBulkdataValue(String studyInstanceUID, BulkdataReference bdr)
            throws IOException;

    ByteBuffer getBulkdata(String studyInstanceUID, UUID fileUUID)
            throws IOException;

    BulkdataReference putBulkdataValue(String studyInstanceUID,
            ByteBuffer value, VR vr) throws IOException;

    BulkdataReference putFragments(String studyInstanceUID, Fragments fragments)
            throws IOException;

    void finishStudy(Study study) throws IOException;

    void reorganizeBulkdata(Study study, String encryptedAlgorithm, boolean replace)
            throws IOException;
}
