package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Fragments;
import org.dcm4che.data.ItemPointer;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.io.DicomInputHandler;
import org.dcm4che.io.DicomInputStream;
import org.dcm4che.util.ByteUtils;
import org.dcm4che.util.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.jhmi.mint.data.BulkdataReference;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class InstanceInputHandler implements DicomInputHandler {

    private static class TagPath {

        private static final ItemPointer[] EMPTY_ITEM_POINTERS = {};

        private final ItemPointer[] itemPointers;

        private final int tag;

        private TagPath(List<ItemPointer> itemPointers, int tag) {

            this.itemPointers = itemPointers.toArray(EMPTY_ITEM_POINTERS);
            this.tag = tag;
        }
    }

    private static final Logger LOG = LoggerFactory
            .getLogger(InstanceInputHandler.class);

    private static final String UNEXPECTED_NON_ZERO_ITEM_LENGTH = "Unexpected item value of {} #{} @ {}";

    private static final String UNEXPECTED_ATTRIBUTE = "Unexpected attribute {} #{} @ {}";

    private final StudyStore studyStore;

    private final List<ItemPointer> itemPointers;

    private final Set<Integer> fmiTags;

    private final List<TagPath> pendingBulkdataTags;

    private String studyInstanceUID;

    public InstanceInputHandler(StudyStore studyStore) {

        this.studyStore = studyStore;
        itemPointers = new ArrayList<ItemPointer>(4);
        fmiTags = new HashSet<Integer>(10);
        pendingBulkdataTags = new ArrayList<TagPath>(2);
        studyInstanceUID = null;
    }

    public int level() {

        return itemPointers.size();
    }

    @Override
    public void readValue(DicomInputStream dis, Attributes dataSet)
            throws IOException {

        int tag = dis.tag();
        VR vr = dis.vr();
        int length = dis.length();
        if (length == 0) {
            dataSet.setNull(tag, vr);
        } else if (vr == VR.SQ) {
            readSequence(dis, dataSet);
        } else if (length == -1) {
            // dis.tag, etc. may be changed by readFragments()
            boolean isBulkdata = isBulkdata(dis, dataSet);
            Fragments fragments = readFragments(dis, dataSet);
            if (isBulkdata) {
                if (studyInstanceUID == null) {
                    dataSet.setValue(tag, vr, fragments);
                    pendingBulkdataTags.add(new TagPath(itemPointers, tag));
                } else {
                    dataSet.setValue(tag, VR.BR, studyStore.putFragments(
                            studyInstanceUID, fragments));
                }
            } else {
                dataSet.setValue(tag, vr, fragments);
            }
        } else if (TagUtils.isFileMetaInformation(tag) && level() == 0
                && fmiTags.contains(tag)) {
            dis.skipFully(length);
            LOG.warn(
                    "Ignored duplicate FMI attribute {} {}"
                            + " with value length of {}",
                    new Object[] { TagUtils.toString(tag),
                            ElementDictionary.keywordOf(tag, null), length });
        } else {
            if (TagUtils.isFileMetaInformation(tag) && level() == 0) {
                fmiTags.add(tag);
            }
            byte[] valueBytes = dis.readValue();
            if (!TagUtils.isGroupLength(tag)) {
                if (dis.bigEndian() != dataSet.bigEndian()) {
                    vr.toggleEndian(valueBytes, false);
                }
                if (isBulkdata(dis, dataSet)) {
                    if (studyInstanceUID == null) {
                        dataSet.setBytes(tag, vr, valueBytes);
                        pendingBulkdataTags.add(new TagPath(itemPointers, tag));
                    } else {
                        dataSet.setValue(tag, VR.BR, studyStore
                                .putBulkdataValue(studyInstanceUID,
                                        ByteBuffer.wrap(valueBytes), vr));
                    }
                } else {
                    dataSet.setBytes(tag, vr, valueBytes);
                    if (studyInstanceUID == null && tag == Tag.StudyInstanceUID
                            && level() == 0) {
                        Object s = vr.toStrings(valueBytes,
                                dataSet.bigEndian(),
                                dataSet.getSpecificCharacterSet(vr));
                        if (s instanceof String) {
                            studyInstanceUID = (String) s;
                            for (TagPath tagPath : pendingBulkdataTags) {
                                Attributes containingDataSet = dataSet
                                        .getNestedDataset(tagPath.itemPointers);
                                int bulkdataTag = tagPath.tag;
                                Object value = containingDataSet
                                        .getValue(bulkdataTag);
                                BulkdataReference bdr = value instanceof byte[] ? studyStore
                                        .putBulkdataValue(
                                                studyInstanceUID,
                                                ByteBuffer.wrap((byte[]) value),
                                                containingDataSet
                                                        .getVR(bulkdataTag))
                                        : studyStore.putFragments(
                                                studyInstanceUID,
                                                (Fragments) value);
                                containingDataSet.setValue(bulkdataTag, VR.BR,
                                        bdr);
                            }
                        }
                        // TODO else?
                    }
                }
            } else if (tag == Tag.FileMetaInformationGroupLength) {
                dis.setFileMetaInformationGroupLength(valueBytes);
            }
        }
    }

    @Override
    public void readValue(DicomInputStream dis, Sequence sequence)
            throws IOException {

        int length = dis.length();
        if (length == 0) {
            sequence.add(new Attributes(sequence.getParent().bigEndian(), 0));
            return;
        }
        Attributes item = new Attributes(sequence.getParent().bigEndian());
        sequence.add(item);
        dis.readAttributes(item, length, Tag.ItemDelimitationItem);
        item.trimToSize();
    }

    @Override
    public void readValue(DicomInputStream dis, Fragments fragments)
            throws IOException {

        if (dis.length() == 0) {
            fragments.add(ByteUtils.EMPTY_BYTES);
        } else {
            byte[] fragmentBytes = dis.readValue();
            if (dis.bigEndian() != fragments.bigEndian()) {
                fragments.vr().toggleEndian(fragmentBytes, false);
            }
            fragments.add(fragmentBytes);
        }
    }

    @Override
    public void startDataset(DicomInputStream dis) throws IOException {

        itemPointers.clear();
        fmiTags.clear();
        pendingBulkdataTags.clear();
        studyInstanceUID = null;
    }

    @Override
    public void endDataset(DicomInputStream dis) throws IOException {
    }

    public boolean isBulkdata(DicomInputStream dis, Attributes dataSet) {

        int tag = dis.tag();
        return dis.getBulkDataDescriptor().isBulkData(itemPointers, null, tag,
                dis.vr(), dis.length());
    }

    protected void skipAttribute(DicomInputStream dis, String message)
            throws IOException {

        int length = dis.length();
        LOG.warn(message, new Object[] { TagUtils.toString(dis.tag()), length,
                dis.getTagPosition() });
        dis.skip(length);
    }

    protected void readSequence(DicomInputStream dis, Attributes dataSet)
            throws IOException {

        int sqTag = dis.tag();
        int length = dis.length();
        if (length == 0) {
            dataSet.setNull(sqTag, VR.SQ);
            return;
        }
        Sequence sequence = dataSet.newSequence(sqTag, 10);
        boolean undefLen = length == -1;
        long endPos = dis.getPosition() + (length & 0xFFFFFFFFL);
        for (int i = 0; undefLen || dis.getPosition() < endPos; i++) {
            dis.readHeader();
            int tag = dis.tag();
            if (tag == Tag.Item) {
                addItemPointer(sqTag, null, i);
                readValue(dis, sequence);
                removeItemPointer();
            } else if (tag == Tag.SequenceDelimitationItem) {
                if (dis.length() != 0) {
                    skipAttribute(dis, UNEXPECTED_NON_ZERO_ITEM_LENGTH);
                }
                break;
            } else {
                skipAttribute(dis, UNEXPECTED_ATTRIBUTE);
            }
        }
        if (sequence.isEmpty()) {
            dataSet.setNull(sqTag, VR.SQ);
        } else {
            sequence.trimToSize();
        }
    }

    private void addItemPointer(int sqTag, String privateCreator, int itemIndex) {

        itemPointers.add(new ItemPointer(sqTag, privateCreator, itemIndex));
    }

    private void removeItemPointer() {

        itemPointers.remove(itemPointers.size() - 1);
    }

    protected Fragments readFragments(DicomInputStream dis, Attributes dataSet)
            throws IOException {

        int fragsTag = dis.tag();
        VR vr = dis.vr();
        Fragments fragments = new Fragments(vr, dataSet.bigEndian(), 10);
        for (int i = 0; true; i++) {
            dis.readHeader();
            int tag = dis.tag();
            if (tag == Tag.Item) {
                addItemPointer(fragsTag, null, i);
                readValue(dis, fragments);
                removeItemPointer();
            } else if (tag == Tag.SequenceDelimitationItem) {
                if (dis.length() != 0) {
                    skipAttribute(dis, UNEXPECTED_NON_ZERO_ITEM_LENGTH);
                }
                break;
            } else {
                skipAttribute(dis, UNEXPECTED_ATTRIBUTE);
            }
        }
        return fragments;
    }
}
