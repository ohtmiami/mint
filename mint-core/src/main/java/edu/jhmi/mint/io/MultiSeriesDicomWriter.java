package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.Tag;
import org.dcm4che.data.UID;
import org.dcm4che.data.VR;
import org.dcm4che.io.DicomEncodingOptions;
import org.dcm4che.io.DicomOutputStream;

import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.NormalizedDataSet;
import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MultiSeriesDicomWriter {

    public static final byte[] MSDICOM_FORMAT_VERSION = { 0x2, 0x0 };

    public static void writeStudy(OutputStream out, Study study,
            boolean writeMSDicomFMI, boolean useExplicitLengths)
            throws IOException {

        Attributes msdicomDataSet = new Attributes(1);
        Sequence patientStudiesSequence = msdicomDataSet.newSequence(
                Tag.PatientStudiesSequence, 2);
        // TODO Patient-level attributes
        patientStudiesSequence.add(new Attributes());
        patientStudiesSequence.add(createStudyItem(study));

        DicomOutputStream dicomOut = new DicomOutputStream(out,
                UID.ExplicitVRLittleEndian);
        if (useExplicitLengths) {
            DicomEncodingOptions encodingOptions = new DicomEncodingOptions(
                    false, false, false, false, false);
            dicomOut.setEncodingOptions(encodingOptions);
            msdicomDataSet.calcLength(encodingOptions, true);
        }
        // Cannot call DicomOutputStream.writeDataset() because it may
        // manipulate private attributes before writing them out
        if (writeMSDicomFMI) {
            Attributes msdicomFMI = createMSDicomFileMetaInformation(study);
            dicomOut.writeFileMetaInformation(msdicomFMI);
        }
        msdicomDataSet.writeTo(dicomOut);
    }

    public static void writeStudy(OutputStream out, Study study)
            throws IOException {

        writeStudy(out, study, true, true);
    }

    private static Attributes createMSDicomFileMetaInformation(Study study) {

        Attributes msdicomFMI = new Attributes(6);
        msdicomFMI.setBytes(Tag.FileMetaInformationVersion, VR.OB,
                MSDICOM_FORMAT_VERSION);
        msdicomFMI.setString(Tag.MediaStorageSOPClassUID, VR.UI,
                UID.MultiSeriesStudyStorage);
        msdicomFMI.setString(Tag.MediaStorageSOPInstanceUID, VR.UI,
                study.getUID());
        msdicomFMI.setString(Tag.TransferSyntaxUID, VR.UI,
                UID.ExplicitVRLittleEndian);
        // TODO
        // fmi.setString(Tag.ImplementationClassUID, VR.UI, "");
        // TODO
        // fmi.setString(Tag.ImplementationVersionName, VR.SH, "");
        return msdicomFMI;
    }

    private static Attributes createStudyItem(Study study) {

        NormalizedDataSet studyItem = NormalizedDataSet.newItem(study.fmi()
                .size() + study.dataSet().size() + 1);
        studyItem.insertAll(study.fmi());
        studyItem.insertAll(study.dataSet());
        Sequence seriesSequence = studyItem.newSequence(
                Tag.PerSeriesFunctionalGroupsSequence, study.seriesCount());
        for (Series series : study) {
            seriesSequence.add(createSeriesItem(series));
        }
        return studyItem;
    }

    private static Attributes createSeriesItem(Series series) {

        NormalizedDataSet seriesItem = NormalizedDataSet.newItem(series.fmi()
                .size() + series.dataSet().size() + 1);
        seriesItem.insertAll(series.fmi());
        seriesItem.insertAll(series.dataSet());
        Sequence instanceSequence = seriesItem
                .newSequence(Tag.PerInstanceFunctionalGroupsSequence,
                        series.instanceCount());
        for (Instance instance : series) {
            instanceSequence.add(createInstanceItem(instance));
        }
        return seriesItem;
    }

    private static Attributes createInstanceItem(Instance instance) {

        NormalizedDataSet instanceItem = NormalizedDataSet.newItem(instance
                .fmi().size() + instance.dataSet().size());
        instanceItem.insertAll(instance.fmi());
        instanceItem.insertAll(instance.dataSet());
        return instanceItem;
    }
}
