package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.zip.Deflater;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintEncodingOptions {

    public static final MintEncodingOptions METADATA_DEFAULT = new MintEncodingOptions(
            true, true);

    public static final MintEncodingOptions BULKDATA_DEFAULT = new MintEncodingOptions(
            true, false);

    public final boolean encrypt;

    public final boolean compress;

    public final String compressedAlgorithm;

    public final String encryptedAlgorithm;

    public final int compressionLevel;

    public final boolean writeMSDicomFMI;

    public final boolean useExplicitLengths;

    public MintEncodingOptions(boolean encrypt, String encryptedAlgorithm,
            boolean compress, String compressedAlgorithm, int compressionLevel,
            boolean writeMSDicomFMI, boolean useExplicitLengths) {

        if ((compressionLevel < 0 || compressionLevel > 9)
                && compressionLevel != Deflater.DEFAULT_COMPRESSION) {
            throw new IllegalArgumentException(String.format(
                    "Invalid compression level %d", compressionLevel));
        }
        this.encrypt = encrypt;
        this.compress = compress;
        this.compressionLevel = compressionLevel;
        this.writeMSDicomFMI = writeMSDicomFMI;
        this.useExplicitLengths = useExplicitLengths;
        this.encryptedAlgorithm = encryptedAlgorithm;
        this.compressedAlgorithm = compressedAlgorithm;
    }

    public MintEncodingOptions(boolean encrypt, boolean compress) {

        // Currently, we just provide AES=GCM-256 for encryption and one
        // compression Algorithm
        this(encrypt, "AES-GCM-256", compress, "Deflate",
                Deflater.DEFAULT_COMPRESSION, false, true);
    }
}
