package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintUUIDOption extends MintHeaderOption<UUID> {

    private static final int UUID_SIZE = 16;

    public MintUUIDOption() {
    }

    public MintUUIDOption(UUID uuid) {

        setValue(uuid);
    }

    @Override
    public void setValue(UUID value) {

        if (value == null) {
            throw new NullPointerException("value");
        }
        super.setValue(value);
    }

    @Override
    public int getBitmask() {

        return MintHeaderOption.getUUIDBitMask();
    }

    @Override
    public int calculateLength() {

        return UUID_SIZE;
    }

    @Override
    protected void encodeValue(ByteBuffer buffer) {

        UUID uuid = this.getValue();
        buffer.putLong(uuid.getMostSignificantBits());
        buffer.putLong(uuid.getLeastSignificantBits());
    }

    @Override
    protected void decodeValue(ByteBuffer buffer, int length)
            throws MalformedMintHeaderException {

        if (length != UUID_SIZE) {
            throw new MalformedMintHeaderException(String.format(
                    "Invalid UUID length %d", length));
        }
        try {
            long msb = buffer.getLong();
            long lsb = buffer.getLong();
            UUID uuid = new UUID(msb, lsb);
            setValue(uuid);
        } catch (Exception e) {
            throw new MalformedMintHeaderException(e);
        }
    }
}
