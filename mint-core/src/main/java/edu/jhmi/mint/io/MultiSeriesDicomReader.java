package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.Tag;
import org.dcm4che.data.UID;
import org.dcm4che.io.DicomInputStream;
import org.dcm4che.io.DicomStreamException;
import org.dcm4che.util.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MultiSeriesDicomReader {

    private static final Logger LOG = LoggerFactory
            .getLogger(MultiSeriesDicomReader.class);

    private long reconstructionTimeMillis;

    public Study readStudy(InputStream in) throws IOException {

        long reconstructionStartMillis = 0;

        @SuppressWarnings("resource")
        DicomInputStream dicomIn = new DicomInputStream(in);
        dicomIn.setDicomInputHandler(new MultiSeriesDicomInputHandler());

        Attributes msdicomFMI = dicomIn.readFileMetaInformation();
        if (msdicomFMI != null) {
            checkMSDicomFileMetaInformation(msdicomFMI);
        }

        Attributes msdicomDataSet = dicomIn.readDataset(-1, -1);
        if (LOG.isTraceEnabled()) {
            reconstructionStartMillis = System.currentTimeMillis();
        }
        Attributes studyItem = msdicomDataSet.getNestedDataset(
                Tag.PatientStudiesSequence, 1);
        if (studyItem == null) {
            return null;
        }

        String studyInstanceUID = studyItem.getString(Tag.StudyInstanceUID);
        Object seriesSequenceValue = studyItem
                .remove(Tag.PerSeriesFunctionalGroupsSequence);
        if (!(seriesSequenceValue instanceof Sequence)) {
            throw new DicomStreamException(String.format(
                    "Not a MSDICOM stream: Missing %s %s", TagUtils
                            .toString(Tag.PerSeriesFunctionalGroupsSequence),
                    ElementDictionary.keywordOf(
                            Tag.PerSeriesFunctionalGroupsSequence, null)));
        }

        Sequence seriesSequence = (Sequence) seriesSequenceValue;
        int seriesCount = seriesSequence.size();
        Study study = new Study(studyInstanceUID, studyItem, seriesCount);
        for (Attributes seriesItem : seriesSequence) {
            String seriesInstanceUID = (seriesCount > 1 ? seriesItem
                    : studyItem).getString(Tag.SeriesInstanceUID);
            Object instanceSequenceValue = seriesItem
                    .remove(Tag.PerInstanceFunctionalGroupsSequence);
            if (!(instanceSequenceValue instanceof Sequence)) {
                throw new DicomStreamException(
                        String.format(
                                "Not a MSDICOM stream: Missing %s %s",
                                TagUtils.toString(Tag.PerInstanceFunctionalGroupsSequence),
                                ElementDictionary
                                        .keywordOf(
                                                Tag.PerInstanceFunctionalGroupsSequence,
                                                null)));
            }

            Sequence instanceSequence = (Sequence) instanceSequenceValue;
            int instanceCount = instanceSequence.size();
            Series series = study.newSeries(seriesInstanceUID, seriesItem,
                    instanceCount);
            for (Attributes instanceItem : instanceSequence) {
                String sopInstanceUID = (instanceCount > 1 ? instanceItem
                        : (seriesCount > 1 ? seriesItem : studyItem))
                        .getString(Tag.SOPInstanceUID);
                series.newInstance(sopInstanceUID, instanceItem);
            }
        }
        if (LOG.isTraceEnabled()) {
            reconstructionTimeMillis = System.currentTimeMillis()
                    - reconstructionStartMillis;
        }
        return study;
    }

    private static void checkMSDicomFileMetaInformation(Attributes msdicomFMI)
            throws DicomStreamException {

        byte[] fmiVersion = null;
        try {
            fmiVersion = msdicomFMI.getBytes(Tag.FileMetaInformationVersion);
        } catch (IOException e) {
            throw new RuntimeException("Unexpected error", e);
        }
        if (fmiVersion != null
                && !Arrays.equals(fmiVersion,
                        MultiSeriesDicomWriter.MSDICOM_FORMAT_VERSION)) {
            throw new DicomStreamException(String.format(
                    "Not a MSDICOM stream: Invalid %s %s", TagUtils
                            .toString(Tag.FileMetaInformationVersion),
                    ElementDictionary.keywordOf(Tag.FileMetaInformationVersion,
                            null)));
        }
        String mediaStorageSOPClassUID = msdicomFMI
                .getString(Tag.MediaStorageSOPClassUID);
        if (mediaStorageSOPClassUID != null
                && !UID.MultiSeriesStudyStorage.equals(mediaStorageSOPClassUID)) {
            throw new DicomStreamException(String.format(
                    "Not a MSDICOM stream: Invalid %s %s", TagUtils
                            .toString(Tag.MediaStorageSOPClassUID),
                    ElementDictionary.keywordOf(Tag.MediaStorageSOPClassUID,
                            null)));
        }
    }

    public static void main(String[] args) throws IOException {

        if (args.length < 1) {
            System.err.println("Usage: MultiSeriesDicomReader"
                    + " <study_metadata_file>");
            System.exit(1);
        }

        Path studyFilePath = Paths.get(args[0]);
        MultiSeriesDicomReader msdicomReader = new MultiSeriesDicomReader();
        long readStartMillis = System.currentTimeMillis();
        Study study = null;
        try (InputStream in = Files.newInputStream(studyFilePath)) {
            study = msdicomReader.readStudy(in);
        }
        long readTimeMillis = System.currentTimeMillis() - readStartMillis;
        if (study != null) {
            System.out.printf("%d\t%d", msdicomReader.reconstructionTimeMillis,
                    readTimeMillis);
        }
    }
}
