package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class StudyCipher {

    public static final int IV_SIZE = 12;

    public static final int KEY_SIZE = 16;

    public static final int MAC_SIZE = 16;

    private static final String CONFIG_FILE_RESOURCE_NAME = "crypto.properties";

    private final SecureRandom random;

    private final Mac hmac;

    private final Cipher cipher;

    private byte[] secret;

    public StudyCipher(String encryptedAlgorithm) {

        random = new SecureRandom();
        try {
            hmac = Mac.getInstance("HmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unexpected error", e);
        }
        try {
            // For using different encryption algorithm in the future
            if (encryptedAlgorithm.equals("AES-GCM-256")) {
                cipher = Cipher.getInstance("AES/GCM/NoPadding");
            } else {
                cipher = Cipher.getInstance("AES/GCM/NoPadding");
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unexpected error", e);
        } catch (NoSuchPaddingException e) {
            throw new RuntimeException("Unexpected error", e);
        }
        try (InputStream configIn = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream(CONFIG_FILE_RESOURCE_NAME)) {
            Properties config = new Properties();
            config.load(configIn);
            String secretString = config.getProperty("secret");
            setSecret(decodeHexString(secretString));
        } catch (Exception e) {
            secret = null;
        }
    }

    public static byte[] decodeHexString(String encoded) {

        if (encoded.length() % 2 != 0) {
            throw new IllegalArgumentException(String.format(
                    "Invalid hex string %s"
                            + " - length must be an even number", encoded));
        }
        int n = encoded.length() / 2;
        byte[] decoded = new byte[n];
        for (int i = 0; i < n; i++) {
            char highChar = encoded.charAt(i * 2);
            byte high = decodeHexChar(highChar);
            char lowChar = encoded.charAt(i * 2 + 1);
            byte low = decodeHexChar(lowChar);
            decoded[i] = (byte) (high << 4 | low);
        }
        return decoded;
    }

    private static byte decodeHexChar(char c) {

        if (c >= '0' && c <= '9') {
            return (byte) (c - '0');
        }
        if (c >= 'A' && c <= 'F') {
            return (byte) (c - 'A' + 0xA);
        }
        if (c >= 'a' && c <= 'f') {
            return (byte) (c - 'a' + 0xA);
        }
        throw new IllegalArgumentException("Invalid hex char " + c);
    }

    public byte[] getSecret() {

        return secret == null ? null : Arrays.copyOf(secret, secret.length);
    }

    public void setSecret(byte[] secret) {

        if (secret == null) {
            throw new NullPointerException("secret");
        }
        this.secret = secret;
    }

    public byte[] generateIV() {

        byte[] ivBytes = new byte[IV_SIZE];
        random.nextBytes(ivBytes);
        return ivBytes;
    }

    public byte[] deriveKey(String studyInstanceUID, int majorVersion,
            int minorVersion) {

        if (studyInstanceUID == null) {
            throw new NullPointerException("studyInstanceUID");
        }
        if (secret == null) {
            throw new IllegalStateException("Secret has not been set");
        }
        String keyMessage = String.format("MINT %d.%d %s", majorVersion,
                minorVersion, studyInstanceUID);
        byte[] keyMessageBytes = keyMessage.getBytes(StandardCharsets.UTF_8);
        try {
            hmac.init(new SecretKeySpec(secret, ""));
        } catch (InvalidKeyException e) {
            throw new RuntimeException("Unexpected error", e);
        }
        return hmac.doFinal(keyMessageBytes);
    }

    private void initCipher(int opMode, byte[] ivBytes, int ivOffset,
            String studyInstanceUID, int majorVersion, int minorVersion) {

        if (ivBytes.length < ivOffset + IV_SIZE) {
            throw new BufferTooShortException(ivBytes.length - ivOffset,
                    IV_SIZE);
        }
        GCMParameterSpec gcmParams = new GCMParameterSpec(MAC_SIZE * 8,
                ivBytes, ivOffset, IV_SIZE);
        byte[] keyBytes = deriveKey(studyInstanceUID, majorVersion,
                minorVersion);
        SecretKeySpec aesKey = new SecretKeySpec(keyBytes, 0, KEY_SIZE, "AES");
        try {
            cipher.init(opMode, aesKey, gcmParams, random);
        } catch (InvalidKeyException e) {
            throw new RuntimeException("Unexpected error", e);
        } catch (InvalidAlgorithmParameterException e) {
            throw new RuntimeException("Unexpected error", e);
        }
    }

    public Cipher getEncryptingCipher(byte[] ivBytes, int ivOffset,
            String studyInstanceUID, int majorVersion, int minorVersion) {

        initCipher(Cipher.ENCRYPT_MODE, ivBytes, ivOffset, studyInstanceUID,
                majorVersion, minorVersion);
        return cipher;
    }

    public Cipher getDecryptingCipher(byte[] ivBytes, int ivOffset,
            String studyInstanceUID, int majorVersion, int minorVersion) {

        initCipher(Cipher.DECRYPT_MODE, ivBytes, ivOffset, studyInstanceUID,
                majorVersion, minorVersion);
        return cipher;
    }
}
