package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class BufferTooShortException extends RuntimeException {

    private static final long serialVersionUID = -347805120499264338L;

    private final int actualSize;

    private final int minimumSize;

    public BufferTooShortException(int actualSize, int minimumSize) {

        super(String.format("Buffer has only %d bytes remaining"
                + " - need at least %d bytes", actualSize, minimumSize));
        this.actualSize = actualSize;
        this.minimumSize = minimumSize;
    }

    public int getActualSize() {

        return actualSize;
    }

    public int getMinimumSize() {

        return minimumSize;
    }
}
