package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MalformedMintHeaderException extends IOException {

    private static final long serialVersionUID = -5658893053356030229L;

    public MalformedMintHeaderException(String message) {

        super(message);
    }

    public MalformedMintHeaderException(Throwable cause) {

        super(cause);
    }

    public MalformedMintHeaderException(String message, Throwable cause) {

        super(message, cause);
    }
}
