package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 */
public class MintHeader {

    public static enum PayloadType {

        METADATA, BULKDATA
    }

    // Magic and Version
    public static final int MV_SIZE = 6;
    
    // Magic, Version and Length
    public static final int PEEK_SIZE_20 = 2;
    // Magic, Version, Payload Length and Options Length
    public static final int PEEK_SIZE_21 = 6;

    // All required fields
    public static final int MIN_SIZE_20 = 22;
    public static final int MIN_SIZE_21 = 16;

    // All required fields plus all options
    public static final int MAX_SIZE_20 = 120;
    // Currently, the maximum size for compressed and encrypted field are 32
    public static final int MAX_SIZE_21 = 200;

    private static final int MIN_LENGTH_20 = MIN_SIZE_20 - PEEK_SIZE_20
            - MV_SIZE;

    private static final int MIN_LENGTH_21 = MIN_SIZE_21 - PEEK_SIZE_21
            - MV_SIZE;

    private static final int MAX_LENGTH_20 = MAX_SIZE_20 - PEEK_SIZE_20
            - MV_SIZE;

    private static final int MAX_LENGTH_21 = MAX_SIZE_21 - PEEK_SIZE_21
            - MV_SIZE;

    private static final byte[] MAGIC = { 'M', 'I', 'N', 'T' };

    private static final byte[] VERSION_20 = { 0x02, 0x00 };

    private static final byte[] VERSION_21 = { 0x02, 0x01 };

    private static final int BITMASK_METADATA_20 = 0x0001;

    private static final int BITMASK_COMPRESSED_20 = 0x0002;

    private static final int BITMASK_ENCRYPTED_20 = 0x0004;

    private static int uncompressedLength;

    private static byte[] VERSION = { 0x02, 0x01 };

    private PayloadType payloadType;

    private boolean encrypted;

    private boolean compressed;

    private final SortedMap<Integer, MintHeaderOption<?>> options = new TreeMap<Integer, MintHeaderOption<?>>();

    public static int getMajorVersion() {

        return VERSION[0] & 0xFF;
    }

    public static int getMinorVersion() {

        return VERSION[1] & 0xFF;
    }

    public static void setMajorVersion(byte majorVersion)
            throws MalformedMintHeaderException {

        VERSION[0] = majorVersion;
        if (!isVersion20() && !isVersion21()) {
            throw new MalformedMintHeaderException(String.format(
                    "Unsupported major version of MINT header %d", VERSION[0]));
        }
    }

    public static void setMinorVersion(byte minorVersion)
            throws MalformedMintHeaderException {

        VERSION[1] = minorVersion;
        if (!isVersion20() && !isVersion21()) {
            throw new MalformedMintHeaderException(String.format(
                    "Unsupported minor version of MINT header %d", VERSION[1]));
        }
    }

    public PayloadType getPayloadType() {

        return payloadType;
    }

    public void setPayloadType(PayloadType payloadType) {

        if (payloadType == null) {
            throw new NullPointerException("payloadType");
        }
        this.payloadType = payloadType;
    }

    public boolean isEncrypted() {

        return encrypted;
    }

    public void setEncrypted(boolean encrypted) {

        this.encrypted = encrypted;
    }

    public boolean isCompressed() {

        return compressed;
    }

    public void setCompressed(boolean compressed) {

        this.compressed = compressed;
    }

    public int getUncompressedLength() {

        return uncompressedLength;
    }

    public void setUncompressedLength(int uncompressedLength) {

        MintHeader.uncompressedLength = uncompressedLength;
    }

    public Collection<MintHeaderOption<?>> getOptions() {

        return Collections.unmodifiableCollection(options.values());
    }

    public MintHeaderOption<?> getOption(int bitmask) {

        return options.get(bitmask);
    }

    public MintHeaderOption<?> addOption(MintHeaderOption<?> option) {

        return options.put(option.getBitmask(), option);
    }

    public MintHeaderOption<?> removeOption(int bitmask) {

        return options.remove(bitmask);
    }

    public static boolean isVersion20() {

        return (VERSION[0] == VERSION_20[0]) && (VERSION[1] == VERSION_20[1]);
    }

    public static boolean isVersion21() {

        return (VERSION[0] == VERSION_21[0]) && (VERSION[1] == VERSION_21[1]);
    }

    public static int getMaxLength() {

        return isVersion20() ? MAX_LENGTH_20 : MAX_LENGTH_21;
    }

    public static int getMinLength() {

        return isVersion20() ? MIN_LENGTH_20 : MIN_LENGTH_21;
    }

    public static int getMinSize() {

        return isVersion20() ? MIN_SIZE_20 : MIN_SIZE_21;
    }

    public static int getMaxSize() {

        return isVersion20() ? MAX_SIZE_20 : MAX_SIZE_21;
    }

    public static int getPeekSize() {

        return isVersion20() ? PEEK_SIZE_20 : PEEK_SIZE_21;
    }

    public static int getMVSize() {

        return MV_SIZE;
    }

    public int calculateLength(boolean includeAllFields) {

        int length = includeAllFields ? getMinSize() : getMinLength();
        for (MintHeaderOption<?> option : options.values()) {
            length += 2 + option.calculateLength();
        }
        return length;
    }

    public void encode(ByteBuffer buffer) throws BufferTooShortException {

        int totalLength = calculateLength(true);
        if (buffer.remaining() < totalLength) {
            throw new BufferTooShortException(buffer.remaining(), totalLength);
        }
        doEncode(buffer);
    }

    public ByteBuffer encode() {

        ByteBuffer buffer = ByteBuffer.allocate(calculateLength(true));
        doEncode(buffer);
        buffer.flip();
        return buffer;
    }

    private void doEncode(ByteBuffer buffer) {

        if (payloadType == null) {
            throw new NullPointerException("payloadType");
        }
        // Magic (4 bytes)
        buffer.put(MAGIC);
        // Version (2 bytes)
        buffer.put(VERSION);
        int length = calculateLength(false) & 0xFF;

        if (isVersion20()) {
            // Length (2 bytes)
            buffer.putShort((short) length);
            // Flags (2 bytes)
            buffer.putShort((short) encodeFlags());
            // Uncompressed Payload Length (4 bytes)
            buffer.putInt(uncompressedLength);
            // Reserved (4 bytes)
            buffer.putInt(0);
        } else if (isVersion21()) {
            // Uncompressed Payload Length (4 bytes)
            buffer.putInt(uncompressedLength);
            // Length (2 bytes)
            buffer.putShort((short) length);
        }

        // Options Bitmap (4 bytes)
        buffer.putInt(encodeOptionsBitmap());
        // Options
        for (MintHeaderOption<?> option : options.values()) {
            option.encode(buffer);
        }
    }

    private int encodeFlags() {

        int flags = 0;
        if (payloadType == PayloadType.METADATA) {
            flags |= BITMASK_METADATA_20;
        }
        if (compressed) {
            flags |= BITMASK_COMPRESSED_20;
        }
        if (encrypted) {
            flags |= BITMASK_ENCRYPTED_20;
        }
        return flags;
    }

    private int encodeOptionsBitmap() {

        int bitmap = 0;
        for (MintHeaderOption<?> option : options.values()) {
            bitmap |= option.getBitmask();
        }
        return bitmap;
    }

    public static void decodeVersion(ByteBuffer buffer)
            throws BufferTooShortException, MalformedMintHeaderException {
      
        if (buffer.remaining() < MV_SIZE) {
            throw new BufferTooShortException(buffer.remaining(), MV_SIZE);
        }
        byte[] temp = new byte[4];
        // Magic (4 bytes)
        buffer.get(temp, 0, 4);
        if (!Arrays.equals(MAGIC, temp)) {
            throw new MalformedMintHeaderException("Not a MINT header");
        }
        // Version (2 bytes)
        buffer.get(VERSION, 0, 2);
        if (!isVersion20() && !isVersion21()) {
            throw new MalformedMintHeaderException(String.format(
                    "Unsupported MINT header version %d.%d", VERSION[0],
                    VERSION[1]));
        }
    }

    public static int peek(ByteBuffer buffer) throws BufferTooShortException,
            MalformedMintHeaderException {

        if (buffer.remaining() < getPeekSize()) {
            throw new BufferTooShortException(buffer.remaining(), getPeekSize());
        }

        if (isVersion21()) {
            // Payload Length (4 bytes)
            uncompressedLength = buffer.getInt();
        }
        // Header Length (2 bytes)
        int length = buffer.getShort() & 0xFF;
        if (length < getMinLength() || length > getMaxLength()) {
            throw new MalformedMintHeaderException(
                    String.format("Invalid header length %d"
                            + " - must be between %d and %d", length,
                            getMinLength(), getMaxLength()));
        }

        return length;
    }

    public void decode(ByteBuffer buffer, int headerLength)
            throws BufferTooShortException, MalformedMintHeaderException {

        int length = headerLength;
        if (length == -1) {
            decodeVersion(buffer);
            length = peek(buffer);
        } else if ((length < getMinLength() || length > getMaxLength())) {
            throw new MalformedMintHeaderException(
                    String.format("Invalid header length %d"
                            + " - must be between %d and %d", length,
                            getMinLength(), getMaxLength()));
        }

        if (buffer.remaining() < length) {
            throw new BufferTooShortException(buffer.remaining(), length);
        }

        if (isVersion20()) {
            // Flags (2 bytes)
            int flags = buffer.getShort() & 0xFF;
            decodeFlags(flags);
            // Uncompressed Payload Length (4 bytes)
            uncompressedLength = buffer.getInt();
            // Reserved (4 bytes)
            buffer.getInt();
        }
        // Options
        decodeOptions(buffer);
    }

    public void decode(ByteBuffer buffer) throws BufferTooShortException,
            MalformedMintHeaderException {

        decode(buffer, -1);
    }

    private void decodeFlags(int flags) {

        payloadType = (flags & BITMASK_METADATA_20) != 0 ? PayloadType.METADATA
                : PayloadType.BULKDATA;
        compressed = (flags & BITMASK_COMPRESSED_20) != 0;
        encrypted = (flags & BITMASK_ENCRYPTED_20) != 0;
    }

    private void decodeOptions(ByteBuffer buffer)
            throws MalformedMintHeaderException {

        // Options Bitmap (4 bytes)
        int optionsBitmap = buffer.getInt();
        options.clear();
        // The order in which the options are tested is important
        if (isVersion20()) {
            if ((optionsBitmap & MintHeaderOption.getIVBitMask()) != 0) {
                MintIVOption ivOption = new MintIVOption();
                ivOption.decode(buffer);
                addOption(ivOption);
            }
            if ((optionsBitmap & MintHeaderOption.getUIDBitMask()) != 0) {
                MintUIDOption uidOption = new MintUIDOption();
                uidOption.decode(buffer);
                addOption(uidOption);
            }
            if ((optionsBitmap & MintHeaderOption.getUUIDBitMask()) != 0) {
                MintUUIDOption uuidOption = new MintUUIDOption();
                uuidOption.decode(buffer);
                addOption(uuidOption);
            }
        } else if (isVersion21()) {
            if ((optionsBitmap & MintHeaderOption.getMetadataBitMask()) != 0) {
                payloadType = PayloadType.BULKDATA;
                MintPayloadTypeOption payloadOption = new MintPayloadTypeOption();
                payloadOption.decode(buffer);
                addOption(payloadOption);

            } else {
                payloadType = PayloadType.METADATA;
            }
            if ((optionsBitmap & MintHeaderOption.getUUIDBitMask()) != 0) {
                MintUUIDOption uuidOption = new MintUUIDOption();
                uuidOption.decode(buffer);
                addOption(uuidOption);
            }
            if ((optionsBitmap & MintHeaderOption.getUIDBitMask()) != 0) {
                MintUIDOption uidOption = new MintUIDOption();
                uidOption.decode(buffer);
                addOption(uidOption);
            }
            if ((optionsBitmap & MintHeaderOption.getCompressedBitMask()) != 0) {
                compressed = true;
                MintCompressedOption compressedOption = new MintCompressedOption();
                compressedOption.decode(buffer);
                addOption(compressedOption);
            }
            if ((optionsBitmap & MintHeaderOption.getEncryptedBitMask()) != 0) {
                encrypted = true;
                MintEncryptedOption encryptedOption = new MintEncryptedOption();
                encryptedOption.decode(buffer);
                addOption(encryptedOption);
            }
            if ((optionsBitmap & MintHeaderOption.getIVBitMask()) != 0) {
                MintIVOption ivOption = new MintIVOption();
                ivOption.decode(buffer);
                addOption(ivOption);
            }
        }
    }

}
