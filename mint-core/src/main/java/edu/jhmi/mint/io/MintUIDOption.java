package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import org.dcm4che.util.UIDUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintUIDOption extends MintHeaderOption<String> {

    public MintUIDOption() {
    }

    public MintUIDOption(String uid) {

        setValue(uid);
    }

    @Override
    public void setValue(String value) {

        if (!UIDUtils.isValid(value)) {
            throw new IllegalArgumentException(String.format("Invalid UID %s",
                    value));
        }
        super.setValue(value);
    }

    @Override
    public int getBitmask() {

        return MintHeaderOption.getUIDBitMask();
    }

    @Override
    public int calculateLength() {

        return this.getValue().length();
    }

    @Override
    protected void encodeValue(ByteBuffer buffer) {

        String uid = this.getValue();
        byte[] uidBytes = uid.getBytes(StandardCharsets.US_ASCII);
        buffer.put(uidBytes);
    }

    @Override
    protected void decodeValue(ByteBuffer buffer, int length)
            throws MalformedMintHeaderException {

        byte[] uidBytes = new byte[length];
        try {
            buffer.get(uidBytes);
            String uid = new String(uidBytes, StandardCharsets.US_ASCII);
            setValue(uid);
        } catch (Exception e) {
            throw new MalformedMintHeaderException(e);
        }
    }
}
