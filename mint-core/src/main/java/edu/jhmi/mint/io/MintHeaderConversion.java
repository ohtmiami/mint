package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;

import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintHeaderConversion {

    private static MintEncodingOptions setEncodingOptions(MintHeader header) {
        boolean isEncrypted = false, isCompressed = false;

        if (header.isCompressed()) {
            isCompressed = true;
        }
        if (header.isEncrypted()) {
            isEncrypted = true;
        }

        return new MintEncodingOptions(isEncrypted, isCompressed);
    }

    private static Study readMetadata(InputStream in, MintHeader header,
            ByteBuffer encodedMV, ByteBuffer encodedHeader, int headerLength)
            throws IOException {

        InputStream wrappedIn = in;
        Cipher decrypter = null;
        Inflater decompressor = null;
        if (header.isEncrypted()) {
            MintHeaderOption<?> option = header.getOption(MintHeaderOption
                    .getUIDBitMask());
            if (option == null) {
                throw new MalformedMintHeaderException("Missing UID option");
            }
            String studyInstanceUID = (String) option.getValue();

            option = header.getOption(MintHeaderOption.getIVBitMask());
            if (option == null) {
                throw new MalformedMintHeaderException("Missing IV option");
            }
            byte[] ivBytes = (byte[]) option.getValue();
            StudyCipher studyCipher = null;
            if (MintHeader.isVersion21()) {
                studyCipher = new StudyCipher((String) header.getOption(
                        MintHeaderOption.getEncryptedBitMask()).getValue());
            } else if (MintHeader.isVersion20()) {
                studyCipher = new StudyCipher("AES-GCM-256");
            }
            decrypter = studyCipher.getDecryptingCipher(ivBytes, 0,
                    studyInstanceUID, MintHeader.getMajorVersion(),
                    MintHeader.getMinorVersion());

            ByteBuffer aad = ByteBuffer.allocate(MintHeader.getMVSize()
                    + MintHeader.getPeekSize() + headerLength);
            aad.put(encodedMV.array());
            aad.put(encodedHeader.array(), 0, MintHeader.getPeekSize()
                    + headerLength);
            aad.position(0);
            aad.limit(MintHeader.getMVSize() + MintHeader.getPeekSize()
                    + headerLength);

            decrypter.updateAAD(aad);

            wrappedIn = new CipherInputStream(wrappedIn, decrypter);
        }
        if (header.isCompressed()) {
            decompressor = new Inflater();
            wrappedIn = new InflaterInputStream(wrappedIn, decompressor);
        }

        MultiSeriesDicomReader msdicomReader = new MultiSeriesDicomReader();
        Study study = msdicomReader.readStudy(wrappedIn);
        if (decompressor != null) {
            decompressor.end();
        }
        return study;
    }

    public static void mintMetaDataHeaderConversion(InputStream in,
            OutputStream out, String majorVersion, String minorVersion)
            throws IOException {

        MintHeader oldHeader = new MintHeader();
        ByteBuffer encodedOldMV = ByteBuffer.allocate(MintHeader.getMVSize());
        in.read(encodedOldMV.array(), encodedOldMV.arrayOffset(),
                MintHeader.getMVSize());
        encodedOldMV.limit(MintHeader.getMVSize());
        MintHeader.decodeVersion(encodedOldMV);

        ByteBuffer encodedOldHeader = ByteBuffer.allocate(MintHeader
                .getMaxSize());
        in.read(encodedOldHeader.array(), encodedOldHeader.arrayOffset(),
                MintHeader.getPeekSize());
        encodedOldHeader.limit(MintHeader.getPeekSize());
        int headerLength = MintHeader.peek(encodedOldHeader);
        in.read(encodedOldHeader.array(), encodedOldHeader.arrayOffset()
                + MintHeader.getPeekSize(), headerLength);
        encodedOldHeader.limit(MintHeader.getPeekSize() + headerLength);
        oldHeader.decode(encodedOldHeader, headerLength);

        MintEncodingOptions newEncodingOptions = setEncodingOptions(oldHeader);

        Study study = readMetadata(in, oldHeader, encodedOldMV,
                encodedOldHeader, headerLength);

        MintHeader.setMajorVersion((byte) Integer.parseInt(majorVersion));
        MintHeader.setMinorVersion((byte) Integer.parseInt(minorVersion));

        MintStudyWriter.writeMetadata(out, study, newEncodingOptions);
    }
}
