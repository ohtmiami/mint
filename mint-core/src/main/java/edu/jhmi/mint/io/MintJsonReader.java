package edu.jhmi.mint.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.data.Series;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.util.Base64;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */
/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */

public class MintJsonReader {

    private static String TRANSFERSYNTAX = "transferSyntax";

    private static String ITEMS = "items";

    private static String INSTANCES = "instances";

    private static String SERIES = "series";

    private static String STUDIES = "studies";

    private static String EMPTYSTRING = "";

    private static String COLON = ":";

    private static String DASH = "-";

    private static VR string2VR(String str) {
        if ("AE".equals(str))
            return VR.AE;
        else if ("AS".equals(str))
            return VR.AS;
        else if ("AT".equals(str))
            return VR.AT;
        else if ("BR".equals(str))
            return VR.BR;
        else if ("CS".equals(str))
            return VR.CS;
        else if ("DA".equals(str))
            return VR.DA;
        else if ("DS".equals(str))
            return VR.DS;
        else if ("DT".equals(str))
            return VR.DT;
        else if ("FD".equals(str))
            return VR.FD;
        else if ("FL".equals(str))
            return VR.FL;
        else if ("IS".equals(str))
            return VR.IS;
        else if ("LO".equals(str))
            return VR.LO;
        else if ("LT".equals(str))
            return VR.LT;
        else if ("OB".equals(str))
            return VR.OB;
        else if ("OF".equals(str))
            return VR.OF;
        else if ("OW".equals(str))
            return VR.OW;
        else if ("PN".equals(str))
            return VR.PN;
        else if ("SH".equals(str))
            return VR.SH;
        else if ("SL".equals(str))
            return VR.SL;
        else if ("SS".equals(str))
            return VR.SS;
        else if ("ST".equals(str))
            return VR.ST;
        else if ("TM".equals(str))
            return VR.TM;
        else if ("UI".equals(str))
            return VR.UI;
        else if ("UL".equals(str))
            return VR.UL;
        else if ("UN".equals(str))
            return VR.UN;
        else if ("US".equals(str))
            return VR.US;
        else if ("UT".equals(str))
            return VR.UT;
        else
            return null;
    }

    private static void readSequence(JsonParser jp, Sequence sequence)
            throws JsonParseException, IOException {

        while (jp.nextToken() != JsonToken.END_ARRAY) {// {
            Attributes dataSet = new Attributes();
            while (jp.nextToken() != JsonToken.END_OBJECT) {
                readAttribute(jp, dataSet);
            }

            sequence.add(dataSet);
        }

    }

    private static void readAttribute(JsonParser jp, Attributes dataSet)
            throws JsonParseException, IOException {

        jp.nextToken();// {
        jp.nextToken();// tag
        jp.nextToken();
        int tag = Integer.parseInt(jp.getText(), 16);
        jp.nextToken();
        String secondFieldName = jp.getText();
        if (secondFieldName.equals(ITEMS)) {
            VR vr = VR.SQ;
            if (jp.nextToken() == JsonToken.VALUE_NULL) {
                dataSet.setNull(tag, vr);
            } else {
                Sequence sequence = dataSet.ensureSequence(tag, 0);
                readSequence(jp, sequence);
            }

        } else {
            jp.nextToken();// fieldName
            VR vr = string2VR(jp.getText());
            jp.nextToken();// fieldName
            JsonToken token = jp.nextToken();// value
            if (token == JsonToken.VALUE_NULL) {
                dataSet.setNull(tag, vr);
            } else if (token == JsonToken.START_ARRAY) {
                if (vr == VR.TM) {
                    ArrayList<String> strs = new ArrayList<String>();
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        strs.add(jp.getText().replace(COLON, EMPTYSTRING));
                    }
                    dataSet.setString(tag, vr,
                            strs.toArray(new String[strs.size()]));
                } else if (vr == VR.DA) {
                    ArrayList<String> strs = new ArrayList<String>();
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        strs.add(jp.getText().replace(DASH, EMPTYSTRING));
                    }
                    dataSet.setString(tag, vr,
                            strs.toArray(new String[strs.size()]));
                } else if (vr.isInlineBinary()) {
                    System.out.println("Contains Binary Array");
                } else if (vr.isStringType() || vr == VR.AT) {
                    ArrayList<String> strs = new ArrayList<String>();
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        strs.add(jp.getText());
                    }
                    dataSet.setString(tag, vr,
                            strs.toArray(new String[strs.size()]));
                } else if (vr.isIntType()) {
                    ArrayList<Integer> is = new ArrayList<Integer>();
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        is.add(jp.getIntValue());
                    }

                    int[] nums = new int[is.size()];
                    for (int i = 0; i < is.size(); i++) {
                        nums[i] = is.get(i);
                    }

                    dataSet.setInt(tag, vr, nums);
                } else if (vr == VR.FD) {
                    ArrayList<Double> ds = new ArrayList<Double>();
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        ds.add(jp.getDoubleValue());
                    }

                    double[] nums = new double[ds.size()];
                    for (int i = 0; i < ds.size(); i++) {
                        nums[i] = ds.get(i);
                    }

                    dataSet.setDouble(tag, vr, nums);
                } else if (vr == VR.FL) {
                    ArrayList<Float> fs = new ArrayList<Float>();
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        fs.add(jp.getFloatValue());
                    }

                    double[] nums = new double[fs.size()];
                    for (int i = 0; i < fs.size(); i++) {
                        nums[i] = fs.get(i);
                    }

                    dataSet.setDouble(tag, vr, nums);
                }
            } else {

                if (vr == VR.TM) {
                    String time = jp.getText();
                    time = time.replace(COLON, EMPTYSTRING);
                    dataSet.setString(tag, vr, time);
                } else if (vr == VR.DA) {
                    String date = jp.getText();
                    date = date.replace(DASH, EMPTYSTRING);
                    dataSet.setString(tag, vr, date);
                } else if (vr.isInlineBinary()) {
                    String str = jp.getText();
                    char[] charValue = str.toCharArray();
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    Base64.decode(charValue, 0, charValue.length, out);
                    dataSet.setBytes(tag, vr, out.toByteArray());
                } else if (vr.isStringType() || vr == VR.AT) {
                    String str = jp.getText();
                    dataSet.setString(tag, vr, str);
                } else if (vr.isIntType()) {
                    int num = jp.getIntValue();
                    dataSet.setInt(tag, vr, num);
                } else if (vr == VR.FD) {
                    double num = jp.getDoubleValue();
                    dataSet.setDouble(tag, vr, num);
                } else if (vr == VR.FL) {
                    float num = jp.getFloatValue();
                    dataSet.setFloat(tag, vr, num);
                }
            }
        }
        jp.nextToken();// }
    }

    private static Instance readInstance(JsonParser jp, String instanceUID,
            Series series) throws JsonParseException, IOException {
        if (jp.nextToken() != JsonToken.START_OBJECT) {
            return null;
        }
        Instance instance = null;
        Attributes dataSet = new Attributes();

        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getText();

            if (fieldName.equals("transferSyntax")) {
                jp.nextToken();
                String transferSyntax = jp.getText();
                dataSet.setString(Tag.TransferSyntaxUID, VR.UI, transferSyntax);
            } else {
                readAttribute(jp, dataSet);
            }
        }
        instance = series.newInstance(instanceUID, dataSet);

        return instance;
    }

    private static Series readSeries(JsonParser jp, String seriesUID,
            Study study) throws JsonParseException, IOException {
        if (jp.nextToken() != JsonToken.START_OBJECT) {
            return null;
        }
        Series series = null;
        Attributes dataSet = new Attributes();

        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getText();
            if (fieldName.equals(INSTANCES)) {
                series = study.newSeries(seriesUID, dataSet, 0);
                jp.nextToken();
                while (jp.nextToken() != JsonToken.END_OBJECT) {
                    String instanceUID = jp.getText();
                    readInstance(jp, instanceUID, series);
                }
            } else if (fieldName.equals(TRANSFERSYNTAX)) {
                jp.nextToken();
                String transferSyntax = jp.getText();
                dataSet.setString(Tag.TransferSyntaxUID, VR.UI, transferSyntax);
            } else {
                readAttribute(jp, dataSet);
            }
        }

        return series;
    }

    private static Study readStudy(JsonParser jp, String studyUID)
            throws JsonParseException, IOException {

        if (jp.nextToken() != JsonToken.START_OBJECT) {
            return null;
        }
        Study study = null;
        Attributes dataSet = new Attributes();

        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getText();
            if (fieldName.equals(SERIES)) {
                study = new Study(studyUID, dataSet, 0);
                jp.nextToken();
                while (jp.nextToken() != JsonToken.END_OBJECT) {
                    String seriesUID = jp.getText();
                    readSeries(jp, seriesUID, study);
                }
            } else if (fieldName.equals(TRANSFERSYNTAX)) {
                jp.nextToken();
                String transferSyntax = jp.getText();

                dataSet.setString(Tag.TransferSyntaxUID, VR.UI, transferSyntax);
            } else {
                readAttribute(jp, dataSet);
            }
        }
        return study;
    }

    private static Study readPatient(JsonParser jp) throws JsonParseException,
            IOException {
        // this part will be modified after defining Patient Dataset
        if (jp.nextToken() != JsonToken.START_OBJECT) {
            return null;
        }

        Study study = null;
        Attributes patientDataSet = new Attributes();
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getText();
            if (fieldName.equals(STUDIES)) {
                while (jp.nextToken() != JsonToken.END_OBJECT) {
                    jp.nextToken();
                    String studyUID = jp.getText();
                    study = readStudy(jp, studyUID);
                }
            } else {
                readAttribute(jp, patientDataSet);
            }
        }
        study.dataSet().addAll(patientDataSet);
        return study;
    }

    public static Study readJSON(InputStream in) throws IOException {

        JsonFactory f = new JsonFactory();
        JsonParser jp = f.createParser(in);
        Study study = readPatient(jp);
        jp.close();

        return study;
    }
}
