package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintIVOption extends MintHeaderOption<byte[]> {

    public MintIVOption() {
    }

    public MintIVOption(byte[] ivBytes) {

        setValue(ivBytes);
    }

    @Override
    public void setValue(byte[] value) {

        if (value.length != StudyCipher.IV_SIZE) {
            throw new IllegalArgumentException(String.format(
                    "IV must be %d bytes", StudyCipher.IV_SIZE));
        }
        super.setValue(value);
    }

    @Override
    public int getBitmask() {

        return MintHeaderOption.getIVBitMask();
    }

    @Override
    public int calculateLength() {

        return StudyCipher.IV_SIZE;
    }

    @Override
    protected void encodeValue(ByteBuffer buffer) {

        byte[] ivBytes = this.getValue();
        buffer.put(ivBytes);
    }

    @Override
    protected void decodeValue(ByteBuffer buffer, int length)
            throws MalformedMintHeaderException {

        if (length != StudyCipher.IV_SIZE) {
            throw new MalformedMintHeaderException(String.format(
                    "Invalid IV length %d", length));
        }
        byte[] ivBytes = new byte[StudyCipher.IV_SIZE];
        try {
            buffer.get(ivBytes);
            setValue(ivBytes);
        } catch (Exception e) {
            throw new MalformedMintHeaderException(e);
        }
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MintIVOption)) {
            return false;
        }
        byte[] value = this.getValue();
        byte[] otherValue = ((MintIVOption) obj).getValue();
        if (value == null) {
            return otherValue == null;
        }
        return Arrays.equals(value, otherValue);
    }
}
