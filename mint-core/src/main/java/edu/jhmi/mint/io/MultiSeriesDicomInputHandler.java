package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.io.DicomInputStream;
import org.dcm4che.util.TagUtils;

import edu.jhmi.mint.data.BulkdataReference;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MultiSeriesDicomInputHandler extends InstanceInputHandler {

    public MultiSeriesDicomInputHandler() {

        super(null);
    }

    @Override
    public void readValue(DicomInputStream dis, Attributes dataSet)
            throws IOException {

        int tag = dis.tag();
        VR vr = dis.vr();
        int length = dis.length();
        if (length == 0) {
            dataSet.setNull(tag, vr);
        } else if (vr == VR.SQ) {
            this.readSequence(dis, dataSet);
        } else if (length == -1) {
            this.readFragments(dis, dataSet);
        } else {
            byte[] valueBytes = dis.readValue();
            if (!TagUtils.isGroupLength(tag)) {
                if (vr == VR.BR) {
                    BulkdataReference bdr = BulkdataReference
                            .fromBytes(valueBytes);
                    dataSet.setValue(tag, vr, bdr);
                } else {
                    dataSet.setBytes(tag, vr, valueBytes);
                }
            } else if (tag == Tag.FileMetaInformationGroupLength) {
                dis.setFileMetaInformationGroupLength(valueBytes);
            }
        }
    }
}
