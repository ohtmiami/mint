package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public abstract class DicomOutputSink {

    public abstract OutputStream getOutputStream(String studyInstanceUID,
            String seriesInstanceUID, String sopInstanceUID) throws IOException;

    public static DicomOutputSink fromFolder(final Path outputDirPath,
            final boolean hierarchical) {

        return new DicomOutputSink() {

            @Override
            public OutputStream getOutputStream(String studyInstanceUID,
                    String seriesInstanceUID, String sopInstanceUID)
                    throws IOException {

                Path sopInstanceFilePath = hierarchical ? outputDirPath
                        .resolve(String.format("%s/%s/%s.dcm",
                                studyInstanceUID, seriesInstanceUID,
                                sopInstanceUID)) : outputDirPath.resolve(String
                        .format("%s.dcm", sopInstanceUID));
                if (!Files.exists(sopInstanceFilePath.getParent())) {
                    Files.createDirectories(sopInstanceFilePath.getParent());
                }
                return Files.newOutputStream(sopInstanceFilePath);
            }
        };
    }
}
