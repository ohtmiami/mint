package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.OutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.VR;
import org.dcm4che.data.Tag;
import org.dcm4che.data.Value;
import org.dcm4che.util.Base64;
import org.dcm4che.util.TagUtils;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.PatientTag;
import edu.jhmi.mint.data.PrivateGroup;
import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintJsonWriter {

	private static final String TAG = "tag";

	private static final String VALUEREPRESENTATION = "vr";

	private static final String VALUES = "values";

	private static final String ITEMS = "items";

	private static final String BDREF = "bdRef";

	private static final String PATIENT = "patient";

	private static final String STUDY = "study";

	private static final String SERIES = "series";

	private static final String INSTANCES = "instances";

	private static final String TRANSFERSYNTAX = "transferSyntax";

	private static final String DOCTYPE = "DocType";

	private static final String COUNT = "count";

	private static boolean skipInvalidValue = false;

	private static boolean prettyPrint = true;

	private static Attributes extractPatientAttributes(Attributes dataSet,
			int[] patientTags) throws IOException {

		Attributes patientDataSet = new Attributes(dataSet, patientTags);
		for (int i = 0; i < patientTags.length; i++) {
			dataSet.remove(patientTags[i]);
		}
		return patientDataSet;
	}

	private static void insertJson(JsonParser jp, JsonGenerator g)
			throws IOException {

		g.copyCurrentStructure(jp);
	}

	@SuppressWarnings("unchecked")
	private static void writeObject(Map<String, Object> map, JsonGenerator g)
			throws IOException {

		g.writeStartObject();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			Object value = entry.getValue();
			String key = entry.getKey();
			if (value instanceof LinkedHashMap) {
				g.writeFieldName(entry.getKey());
				writeObject((LinkedHashMap<String, Object>) value, g);
			} else if (value instanceof ArrayList) {
				g.writeFieldName(entry.getKey());
				writeArray((ArrayList<Object>) value, g);
			} else if (value == null) {
				g.writeNullField(key);
			} else if (value instanceof Double) {
				g.writeNumberField(key, (Double) value);
			} else if (value instanceof Integer) {
				g.writeNumberField(key, (Integer) value);
			} else if (value instanceof String) {
				g.writeStringField(key, (String) value);
			} else if (value instanceof Boolean) {
				g.writeBooleanField(key, (Boolean) value);
			} else {
				throw new RuntimeException("Invalid Value Type!");
			}
		}
		g.writeEndObject();
	}

	@SuppressWarnings("unchecked")
	private static void writeArray(ArrayList<Object> array, JsonGenerator g)
			throws IOException {

		g.writeStartArray();
		for (int i = 0; i < array.size(); i++) {
			Object value = array.get(i);
			if (value instanceof LinkedHashMap) {
				writeObject((LinkedHashMap<String, Object>) value, g);
			} else if (value instanceof ArrayList) {
				writeArray((ArrayList<Object>) value, g);
			} else if (value == null) {
				g.writeNull();
			} else if (value instanceof Double) {
				g.writeNumber((Double) value);
			} else if (value instanceof Integer) {
				g.writeNumber((Integer) value);
			} else if (value instanceof String) {
				g.writeString((String) value);
			} else if (value instanceof Boolean) {
				g.writeBoolean((Boolean) value);
			} else {
				throw new RuntimeException("Invalid Value Type!");
			}
		}
		g.writeEndArray();
	}

	private static void readObject(JsonParser jp, Map<String, Object> map)
			throws JsonParseException, IOException {

		if (jp.getCurrentToken() != JsonToken.START_OBJECT) { // {
			throw new RuntimeException("Invalid Object!");
		}
		while (jp.nextToken() != JsonToken.END_OBJECT) {
			String key = jp.getText();
			JsonToken token = jp.nextToken();
			if (token == JsonToken.START_ARRAY) {
				ArrayList<Object> newArray = new ArrayList<Object>();
				readArray(jp, newArray);
				map.put(key, newArray);
			} else if (token == JsonToken.START_OBJECT) {
				LinkedHashMap<String, Object> newMap = new LinkedHashMap<String, Object>();
				readObject(jp, newMap);
				map.put(key, newMap);
			} else if (token == JsonToken.VALUE_NULL) {
				map.put(key, null);
			} else if (token == JsonToken.VALUE_NUMBER_FLOAT) {
				map.put(key, jp.getDoubleValue());
			} else if (token == JsonToken.VALUE_NUMBER_INT) {
				map.put(key, jp.getIntValue());
			} else if (token == JsonToken.VALUE_STRING) {
				map.put(key, jp.getText());
			} else if (token == JsonToken.VALUE_TRUE) {
				map.put(key, true);
			} else if (token == JsonToken.VALUE_FALSE) {
				map.put(key, false);
			} else {
				throw new RuntimeException("Invalid Value Type!");
			}
		}
	}

	private static void readArray(JsonParser jp, ArrayList<Object> array)
			throws JsonParseException, IOException {

		if (jp.getCurrentToken() != JsonToken.START_ARRAY) { // [
			throw new RuntimeException("Invalid Array!");
		}
		while (jp.nextToken() != JsonToken.END_ARRAY) {
			JsonToken token = jp.getCurrentToken();
			if (token == JsonToken.START_ARRAY) {
				ArrayList<Object> newArray = new ArrayList<Object>();
				readArray(jp, newArray);
			} else if (token == JsonToken.START_OBJECT) {
				LinkedHashMap<String, Object> newMap = new LinkedHashMap<String, Object>();
				readObject(jp, newMap);
				array.add(newMap);
			} else if (token == JsonToken.VALUE_NULL) {
				array.add(null);
			} else if (token == JsonToken.VALUE_NUMBER_FLOAT) {
				array.add(jp.getDoubleValue());
			} else if (token == JsonToken.VALUE_NUMBER_INT) {
				array.add(jp.getIntValue());
			} else if (token == JsonToken.VALUE_STRING) {
				array.add(jp.getText());
			} else if (token == JsonToken.VALUE_TRUE) {
				array.add(true);
			} else if (token == JsonToken.VALUE_FALSE) {
				array.add(false);
			} else {
				throw new RuntimeException("Invalid Value Type!");
			}
		}

	}

	private static void writeDataSet(Attributes dataSet, JsonGenerator g)
			throws IOException, JsonGenerationException {

		if (dataSet.isEmpty()) {
			return;
		}

		int numAttributes = dataSet.size();
		if (dataSet.tagAt(0) < 0) {
			int firstTagIndex = 1;
			for (; firstTagIndex < numAttributes; firstTagIndex++) {
				if (dataSet.tagAt(firstTagIndex) >= 0) {
					break;
				}
			}
			writeDataSet(dataSet, firstTagIndex, numAttributes, g);
			writeDataSet(dataSet, 0, firstTagIndex, g);
		} else {
			writeDataSet(dataSet, 0, numAttributes, g);
		}
	}

	private static void writeDataSet(Attributes dataSet, int startIndex,
			int endIndex, JsonGenerator g) throws IOException,
			JsonGenerationException {

		for (int i = startIndex; i < endIndex; i++) {
			int tag = dataSet.tagAt(i);
			VR vr = dataSet.vrAt(i);
			Object value = dataSet.valueAt(i);
			if (value instanceof Sequence) {
				writeItems(tag, vr, (Sequence) value, g);
			} else if (value == Value.NULL) {
				if (vr != VR.SQ) {
					writeNullAttribute(tag, vr, g);
				} else {
					writeNullSequence(tag, vr, g);
				}

			} else {
				if (vr == VR.TM) {
					String[] times = dataSet.getStrings(tag);
					String[] strs = new String[times.length];
					boolean isInValid = false;
					for (int j = 0; j < times.length; j++) {
						if (times[j].length() == 8
								&& isDigit(times[j].substring(0, 4))
								&& isDigit(times[j].substring(4, 6))
								&& isDigit(times[j].substring(6, 8))) {
							strs[j] = new StringBuilder(times[j])
									.insert(2, ':').insert(5, ':').toString();
						} else {
							if (skipInvalidValue) {
								isInValid = true;
								continue;
							} else {
								strs[j] = times[j];
							}
						}
					}
					if (!isInValid)
						writeAttribute(tag, vr, strs, g);
				} else if (vr == VR.DA) {
					String[] dates = dataSet.getStrings(tag);
					String[] strs = new String[dates.length];
					boolean isInValid = false;
					for (int j = 0; j < dates.length; j++) {
						if (dates[j].length() == 8
								&& isDigit(dates[j].substring(0, 4))
								&& isDigit(dates[j].substring(4, 6))
								&& isDigit(dates[j].substring(6, 8))) {
							strs[j] = new StringBuilder(dates[j])
									.insert(4, '-').insert(7, '-').toString();
						} else {
							if (skipInvalidValue) {
								isInValid = true;
								continue;
							} else {
								strs[j] = dates[j];
							}
						}
					}
					if (!isInValid)
						writeAttribute(tag, vr, strs, g);
				} else if (vr.isInlineBinary()) {
					byte[] byteValue = dataSet.getBytes(tag);
					char[] charValue = new char[(byteValue.length * 4 / 3 + 3)
							& ~3];
					Base64.encode(byteValue, 0, byteValue.length, charValue, 0);
					writeAttribute(tag, vr, new String(charValue), g);
				} else if (vr.isStringType()) {
					String[] strs = dataSet.getStrings(tag);
					writeAttribute(tag, vr, strs, g);
				} else if (vr.isIntType()) {
					int[] nums = dataSet.getInts(tag);
					writeAttribute(tag, vr, nums, g);
				} else if (vr == VR.AT) {
					String[] strs = dataSet.getStrings(tag);
					writeAttribute(tag, vr, strs, g);
				} else if (vr == VR.FD) {
					double[] nums = dataSet.getDoubles(tag);
					writeAttribute(tag, vr, nums, g);
				} else if (vr == VR.FL) {
					float[] nums = dataSet.getFloats(tag);
					writeAttribute(tag, vr, nums, g);
				}
			}
		}
	}

	private static void writeNullAttribute(int tag, VR vr, JsonGenerator g)
			throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (vr == VR.BR) {
			g.writeFieldName(BDREF);
		} else {
			g.writeFieldName(VALUES);
		}
		g.writeNull();
		g.writeEndObject();
	}

	private static void writeNullSequence(int tag, VR vr, JsonGenerator g)
			throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeFieldName(ITEMS);
		g.writeNull();
		g.writeEndObject();
	}

	private static void writeAttribute(int tag, VR vr, String value,
			JsonGenerator g) throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (vr == VR.BR) {
			g.writeStringField(BDREF, value);
		} else {
			g.writeStringField(VALUES, value);
		}
		g.writeEndObject();
	}

	private static void writeAttribute(int tag, VR vr, String[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeStringField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeString(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject();
	}

	private static void writeAttribute(int tag, VR vr, double[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeNumberField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeNumber(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject();
	}

	private static void writeAttribute(int tag, VR vr, float[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeNumberField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeNumber(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject();
	}

	private static void writeAttribute(int tag, VR vr, int[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeNumberField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeNumber(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject();
	}

	private static void writeTranferSyntax(String transferSyntax,
			JsonGenerator g) throws IOException, JsonGenerationException {
		int tag = Tag.TransferSyntaxUID;
		VR vr = ElementDictionary.vrOf(tag, null);
		g.writeFieldName(TRANSFERSYNTAX);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		g.writeStringField(VALUES, transferSyntax);
		g.writeEndObject();
	}

	private static void writeItems(int tag, VR vr, Sequence sequence,
			JsonGenerator g) throws IOException, JsonGenerationException {

		String keyword = ElementDictionary.keywordOf(tag, null);
		g.writeFieldName(keyword);
		g.writeStartObject();
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeFieldName(ITEMS);
		g.writeStartArray();
		for (Attributes item : sequence) {
			g.writeStartObject();
			Attributes privateDataSet = extractPrivateTag(item);
			writeDataSet(item, g);
			if (privateDataSet.size() > 0) {
				writePrivateGroup(privateDataSet, g);
			}
			g.writeEndObject(); // iterate end
		}
		g.writeEndArray();

		g.writeEndObject();// seq end
	}

	public static HashMap<String, Attributes> extractPrivateTag(Study study) {
		HashMap<String, Attributes> pgTable = new HashMap<String, Attributes>();
		Attributes pgElementsInStudy = new Attributes();
		ArrayList<Integer> pgTagsInStudy = new ArrayList<Integer>();
		for (int i = 0; i < study.dataSet().size(); i++) {
			int tag = study.dataSet().tagAt(i);
			if (TagUtils.isPrivateGroup(tag)) {
				pgTagsInStudy.add(tag);
				pgElementsInStudy.addSelected(study.dataSet(), tag);
			}
		}

		for (int i = 0; i < pgTagsInStudy.size(); i++) {
			study.dataSet().remove(pgTagsInStudy.get(i));
		}

		for (Series series : study) {
			Attributes pgElementsInSeries = new Attributes(pgElementsInStudy);
			ArrayList<Integer> pgTagsInSeries = new ArrayList<Integer>();
			for (int i = 0; i < series.dataSet().size(); i++) {
				int tag = series.dataSet().tagAt(i);
				if (TagUtils.isPrivateGroup(tag)) {
					pgTagsInSeries.add(tag);
					pgElementsInSeries.addSelected(series.dataSet(), tag);
				}
			}

			for (int i = 0; i < pgTagsInSeries.size(); i++) {
				series.dataSet().remove(pgTagsInSeries.get(i));
			}

			for (Instance instance : series) {
				Attributes pgElementsInInstance = new Attributes(
						pgElementsInSeries);
				ArrayList<Integer> pgTagsInInstance = new ArrayList<Integer>();

				for (int i = 0; i < instance.dataSet().size(); i++) {
					int tag = instance.dataSet().tagAt(i);
					if (TagUtils.isPrivateGroup(tag)) {
						pgTagsInInstance.add(tag);
						pgElementsInInstance.addSelected(instance.dataSet(),
								tag);
					}
				}

				for (int i = 0; i < pgTagsInInstance.size(); i++) {
					instance.dataSet().remove(pgTagsInInstance.get(i));
				}

				String SOPInstanceUID = instance.getUID();
				if (pgElementsInInstance.size() > 0) {
					pgTable.put(SOPInstanceUID, pgElementsInInstance);
				}
			}
		}

		return pgTable;
	}

	public static Attributes extractPrivateTag(Attributes dataSet) {
		Attributes privateDataSet = new Attributes();
		ArrayList<Integer> pgTags = new ArrayList<Integer>();
		for (int i = 0; i < dataSet.size(); i++) {
			int tag = dataSet.tagAt(i);
			if (TagUtils.isPrivateGroup(tag)) {
				privateDataSet.addSelected(dataSet, tag);
				pgTags.add(tag);
			}
		}
		for (int i = 0; i < pgTags.size(); i++) {
			dataSet.remove(pgTags.get(i));
		}
		return privateDataSet;
	}

	private static HashMap<String, ArrayList<PrivateGroup>> createPrivateGroup(
			Attributes elements) {
		HashMap<Integer, PrivateGroup> privateGroups = new HashMap<Integer, PrivateGroup>();
		while (elements.size() > 0) {
			int tag = elements.tagAt(0);
			int privateCreatorTag = TagUtils.isPrivateCreator(tag) ? tag
					: TagUtils.creatorTagOf(tag);
			PrivateGroup privateGroup;
			if (privateGroups.containsKey(privateCreatorTag)) {
				privateGroup = privateGroups.get(privateCreatorTag);
			} else {
				privateGroup = new PrivateGroup();
			}
			if (TagUtils.isPrivateCreator(tag)) {
				String value = elements.getString(tag);
				VR vr = elements.getVR(tag);
				privateGroup.setPGCreator(tag, vr, value);
			} else {
				privateGroup.insertPGElement(elements, tag);
			}
			elements.remove(tag);
			privateGroups.put(privateCreatorTag, privateGroup);
		}

		HashMap<String, ArrayList<PrivateGroup>> privatePGNames = new HashMap<String, ArrayList<PrivateGroup>>();
		for (Map.Entry<Integer, PrivateGroup> entry : privateGroups.entrySet()) {
			PrivateGroup privateGroup = entry.getValue();
			String pgName;

			if (privateGroup.hasPGCreator()) {
				pgName = privateGroup.getPGName();
			} else {
				System.out
						.println("Warning: Doesn't have private creator tag!");
				continue;
			}

			ArrayList<PrivateGroup> privateGroupsList;
			if (privatePGNames.containsKey(pgName)) {
				privateGroupsList = privatePGNames.get(pgName);
			} else {
				privateGroupsList = new ArrayList<PrivateGroup>();
			}
			privateGroupsList.add(privateGroup);
			privatePGNames.put(pgName, privateGroupsList);

		}
		return privatePGNames;
	}

	private static void writePrivateGroup(Attributes privateDataSet,
			JsonGenerator g) throws IOException, JsonGenerationException {
		HashMap<String, ArrayList<PrivateGroup>> privatePGNames = createPrivateGroup(privateDataSet);
		for (Map.Entry<String, ArrayList<PrivateGroup>> entry : privatePGNames
				.entrySet()) {
			String pgName = entry.getKey();
			ArrayList<PrivateGroup> privateGroups = entry.getValue();
			g.writeFieldName(pgName);
			g.writeStartArray();
			for (int i = 0; i < privateGroups.size(); i++) {
				PrivateGroup privateGroup = privateGroups.get(i);
				g.writeStartObject(); // pg_group
				g.writeFieldName("group." + i);
				g.writeStartObject(); // pg_cratror
				g.writeStringField(TAG,
						TagUtils.toHexString(privateGroup.getPGCreatorTag()));
				g.writeStringField(VALUEREPRESENTATION, privateGroup
						.getPGCreatorVR().toString());
				g.writeStringField(VALUES, privateGroup.getPGName());
				g.writeFieldName(COUNT);
				g.writeStartObject(); // count
				g.writeNumberField("UN", privateGroup.getNumOfUNElemenst());
				g.writeNumberField("Non-UN",
						privateGroup.getNumOfNonUNElements());
				g.writeNumberField("total", privateGroup.getNumOfPGElements());
				g.writeEndObject(); // count
				g.writeEndObject();// pg_creator
				writePrivateDataSet(privateGroup.pgNonUNElements, g);
				g.writeEndObject();
			}
			g.writeEndArray(); // pg_group
		}
	}

	private static void writePrivateDataSet(Attributes privateElement,
			JsonGenerator g) throws IOException, JsonGenerationException {
		for (int i = 0; i < privateElement.size(); i++) {
			int tag = privateElement.tagAt(i);
			VR vr = privateElement.vrAt(i);
			Object value = privateElement.valueAt(i);
			if (value instanceof Sequence) {
				writePrivateItems(tag, vr, (Sequence) value, g);
			} else if (value == Value.NULL) {
				if (vr != VR.SQ) {
					writeNullPrivateAttribute(tag, vr, g);
				} else {
					writeNullPrivateSequence(tag, vr, g);
				}

			} else {
				if (vr == VR.TM) {
					String[] times = privateElement.getStrings(tag);
					String[] strs = new String[times.length];
					boolean isInValid = false;
					for (int j = 0; j < times.length; j++) {
						if (times[j].length() == 8
								&& isDigit(times[j].substring(0, 4))
								&& isDigit(times[j].substring(4, 6))
								&& isDigit(times[j].substring(6, 8))) {
							strs[j] = new StringBuilder(times[j])
									.insert(2, ':').insert(5, ':').toString();
						} else {
							if (skipInvalidValue) {
								isInValid = true;
								continue;
							} else {
								strs[j] = times[j];
							}
						}
					}
					if (!isInValid)
						writePrivateAttribute(tag, vr, strs, g);
				} else if (vr == VR.DA) {
					String[] dates = privateElement.getStrings(tag);
					String[] strs = new String[dates.length];
					boolean isInValid = false;
					for (int j = 0; j < dates.length; j++) {
						if (dates[j].length() == 8
								&& isDigit(dates[j].substring(0, 4))
								&& isDigit(dates[j].substring(4, 6))
								&& isDigit(dates[j].substring(6, 8))) {
							strs[j] = new StringBuilder(dates[j])
									.insert(4, '-').insert(7, '-').toString();
						} else {
							if (skipInvalidValue) {
								isInValid = true;
								continue;
							} else {
								strs[j] = dates[j];
							}
						}
					}
					if (!isInValid)
						writePrivateAttribute(tag, vr, strs, g);
				} else if (vr.isInlineBinary()) {
					byte[] byteValue = privateElement.getBytes(tag);
					char[] charValue = new char[(byteValue.length * 4 / 3 + 3)
							& ~3];
					Base64.encode(byteValue, 0, byteValue.length, charValue, 0);
					writePrivateAttribute(tag, vr, new String(charValue), g);
				} else if (vr.isStringType()) {
					String[] strs = privateElement.getStrings(tag);
					writePrivateAttribute(tag, vr, strs, g);
				} else if (vr.isIntType()) {
					int[] nums = privateElement.getInts(tag);
					writePrivateAttribute(tag, vr, nums, g);
				} else if (vr == VR.AT) {
					String[] strs = privateElement.getStrings(tag);
					writePrivateAttribute(tag, vr, strs, g);
				} else if (vr == VR.FD) {
					double[] nums = privateElement.getDoubles(tag);
					writePrivateAttribute(tag, vr, nums, g);
				} else if (vr == VR.FL) {
					float[] nums = privateElement.getFloats(tag);
					writePrivateAttribute(tag, vr, nums, g);
				}
			}
		}
	}

	private static void writeNullPrivateAttribute(int tag, VR vr,
			JsonGenerator g) throws IOException, JsonGenerationException {

		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // pg_element
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		g.writeFieldName(VALUES);
		g.writeNull();
		g.writeEndObject(); // pg_element
	}

	private static void writeNullPrivateSequence(int tag, VR vr, JsonGenerator g)
			throws IOException, JsonGenerationException {

		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // pg_element
		g.writeStringField(TAG, TagUtils.toHexString(tag));
		g.writeFieldName(ITEMS);
		g.writeNull();
		g.writeEndObject(); // pg_element
	}

	private static void writePrivateAttribute(int tag, VR vr, String[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {
		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // pg_element
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeStringField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeString(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject(); // pg_element
	}

	private static void writePrivateAttribute(int tag, VR vr, String value,
			JsonGenerator g) throws IOException, JsonGenerationException {
		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // pg_element
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		g.writeStringField(VALUES, value);
		g.writeEndObject(); // pg_element
	}

	private static void writePrivateAttribute(int tag, VR vr, double[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {
		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // pg_element
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeNumberField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeNumber(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject(); // pg_element
	}

	private static void writePrivateAttribute(int tag, VR vr, float[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {
		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // pg_element
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeNumberField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeNumber(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject(); // pg_element
	}

	private static void writePrivateAttribute(int tag, VR vr, int[] value,
			JsonGenerator g) throws IOException, JsonGenerationException {
		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // pg_element
		g.writeStringField(VALUEREPRESENTATION, vr.toString());
		if (value.length == 1) {
			g.writeNumberField(VALUES, value[0]);
		} else {
			g.writeFieldName(VALUES);
			g.writeStartArray();
			for (int i = 0; i < value.length; i++) {
				g.writeNumber(value[i]);
			}
			g.writeEndArray();
		}
		g.writeEndObject(); // pg_element
	}

	private static void writePrivateItems(int tag, VR vr, Sequence sequence,
			JsonGenerator g) throws IOException, JsonGenerationException {

		g.writeFieldName(TagUtils.toHexString(tag));
		g.writeStartObject(); // seq start
		g.writeFieldName(ITEMS);
		g.writeStartArray(); // array start
		for (Attributes item : sequence) {
			g.writeStartObject(); // iterate start
			Attributes privateDataSet = extractPrivateTag(item);
			writeDataSet(item, g);
			if (privateDataSet.size() > 0) {
				writePrivateGroup(privateDataSet, g);
			}
			g.writeEndObject(); // iterate end
		}
		g.writeEndArray(); // array end

		g.writeEndObject();// seq end
	}

	public static void writePatientStudiesIndex(OutputStream out, Study study,
			HashMap<String, Attributes> privateDataSets, String headerName,
			String headerContent) throws IOException, JsonGenerationException {

		JsonFactory f = new JsonFactory();
		JsonGenerator g = f.createGenerator(out, JsonEncoding.UTF8);
		if (prettyPrint) {
			g.useDefaultPrettyPrinter();
		}

		g.writeStartObject();

		if (headerName != null) {
			JsonParser jp = f.createParser(headerContent);
			jp.nextToken();
			g.writeFieldName(headerName);
			insertJson(jp, g);
		}
		g.writeFieldName(PATIENT);
		g.writeStartObject(); // patient
		Attributes patientDataSet = extractPatientAttributes(study.dataSet(),
				PatientTag.getPatientTags());
		writeDataSet(patientDataSet, g);
		g.writeEndObject(); // patient

		g.writeFieldName(STUDY);
		g.writeStartObject(); // studies

		String transferSyntax = study.fmi().getString(Tag.TransferSyntaxUID);
		if (transferSyntax != null) {
			writeTranferSyntax(transferSyntax, g);
		}

		writeDataSet(study.dataSet(), g);

		g.writeFieldName(SERIES);
		g.writeStartArray(); // series

		for (Series series : study) {
			g.writeStartObject(); // seriesObject

			transferSyntax = series.fmi().getString(Tag.TransferSyntaxUID);
			if (transferSyntax != null) {
				writeTranferSyntax(transferSyntax, g);
			}

			writeDataSet(series.dataSet(), g);

			g.writeFieldName(INSTANCES);
			g.writeStartArray(); // instances

			for (Instance instance : series) {
				String instanceUID = instance.getUID();
				Attributes privateDataSet = new Attributes();
				if (privateDataSets.containsKey(instanceUID)) {
					privateDataSet = privateDataSets.get(instanceUID);
				}

				g.writeStartObject();// instance Object
				// To Do
				transferSyntax = instance.fmi()
						.getString(Tag.TransferSyntaxUID);
				if (transferSyntax != null) {
					writeTranferSyntax(transferSyntax, g);
				}

				writeDataSet(instance.dataSet(), g);
				if (privateDataSet.size() > 0) {
					writePrivateGroup(privateDataSet, g);
				}
				g.writeEndObject(); // instance Object
			}

			g.writeEndArray(); // instances
			g.writeEndObject(); // seriesObject
		}

		g.writeEndArray(); // series
		g.writeEndObject(); // studies
		g.writeEndObject(); // begin
		g.close();
	}

	public static void writePatientStudiesIndexWithoutInstance(
			OutputStream out, Study study,
			HashMap<String, Attributes> privateDataSet, String headerName,
			String headerContent) throws IOException, JsonGenerationException {

		JsonFactory f = new JsonFactory();
		JsonGenerator g = f.createGenerator(out, JsonEncoding.UTF8);
		if (prettyPrint) {
			g.useDefaultPrettyPrinter();
		}

		g.writeStartObject();

		if (headerName != null) {
			JsonParser jp = f.createParser(headerContent);
			jp.nextToken();
			g.writeFieldName(headerName);
			insertJson(jp, g);
		}
		g.writeFieldName(PATIENT);
		g.writeStartObject(); // patient
		Attributes patientDataSet = extractPatientAttributes(study.dataSet(),
				PatientTag.getPatientTags());
		writeDataSet(patientDataSet, g);
		g.writeEndObject(); // patient

		g.writeFieldName(STUDY);
		g.writeStartObject(); // study object

		String transferSyntax = study.fmi().getString(Tag.TransferSyntaxUID);
		if (transferSyntax != null) {
			writeTranferSyntax(transferSyntax, g);
		}

		writeDataSet(study.dataSet(), g);

		g.writeFieldName(SERIES);
		g.writeStartArray();// series

		for (Series series : study) {
			g.writeStartObject();// series Object

			transferSyntax = series.fmi().getString(Tag.TransferSyntaxUID);
			if (transferSyntax != null) {
				writeTranferSyntax(transferSyntax, g);
			}

			writeDataSet(series.dataSet(), g);

			g.writeEndObject(); // Series Object
		}

		g.writeEndArray(); // series
		g.writeEndObject(); // study object
		g.writeEndObject(); // begin
		g.close();
	}

	public static void writePatientStudiesIndexWithOneInstance(
			OutputStream out, Study study,
			HashMap<String, Attributes> privateDataSets, String headerName,
			String headerContent) throws IOException, JsonGenerationException {

		JsonFactory f = new JsonFactory();
		JsonGenerator g = f.createGenerator(out, JsonEncoding.UTF8);
		if (prettyPrint) {
			g.useDefaultPrettyPrinter();
		}

		g.writeStartObject();

		if (headerName != null) {
			JsonParser jp = f.createParser(headerContent);
			jp.nextToken();
			g.writeFieldName(headerName);
			insertJson(jp, g);
		}
		g.writeFieldName(PATIENT);
		g.writeStartObject(); // patient
		Attributes patientDataSet = extractPatientAttributes(study.dataSet(),
				PatientTag.getPatientTags());
		writeDataSet(patientDataSet, g);
		g.writeEndObject(); // patient

		g.writeFieldName(STUDY);
		g.writeStartObject();

		String transferSyntax = study.fmi().getString(Tag.TransferSyntaxUID);
		if (transferSyntax != null) {
			writeTranferSyntax(transferSyntax, g);
		}

		writeDataSet(study.dataSet(), g);

		g.writeFieldName(SERIES);
		g.writeStartArray(); // series

		for (Series series : study) {
			g.writeStartObject(); // series object

			transferSyntax = series.fmi().getString(Tag.TransferSyntaxUID);
			if (transferSyntax != null) {
				writeTranferSyntax(transferSyntax, g);
			}

			writeDataSet(series.dataSet(), g);

			g.writeFieldName(INSTANCES);
			g.writeStartArray();// instances

			if (study.dataSet().getInts(Tag.InstanceNumber) != null
					|| series.dataSet().getInts(Tag.InstanceNumber) != null) {
				for (Instance instance : series) {
					g.writeStartObject();// instance Object
					transferSyntax = instance.fmi().getString(
							Tag.TransferSyntaxUID);
					if (transferSyntax != null) {
						writeTranferSyntax(transferSyntax, g);
					}

					writeDataSet(instance.dataSet(), g);

					g.writeEndObject(); // instance Object
					break;
				}
			} else {
				int[] instanceNums = new int[series.instanceCount()];
				int idx = 0;

				for (Instance instance : series) {
					instanceNums[idx] = instance.dataSet().getInt(
							Tag.InstanceNumber, 0);
					idx++;
				}

				Arrays.sort(instanceNums);

				int medianNum = instanceNums[instanceNums.length / 2];

				for (Instance instance : series) {
					int instanceNum = instance.dataSet().getInt(
							Tag.InstanceNumber, 0);
					if (instanceNum != medianNum) {
						continue;
					}
					String instanceUID = instance.getUID();
					Attributes privateDataSet = new Attributes();
					if (privateDataSets.containsKey(instanceUID)) {
						privateDataSet = privateDataSets.get(instanceUID);
					}

					g.writeStartObject();// instance Object
					transferSyntax = instance.fmi().getString(
							Tag.TransferSyntaxUID);
					if (transferSyntax != null) {
						writeTranferSyntax(transferSyntax, g);
					}

					writeDataSet(instance.dataSet(), g);
					if (privateDataSet.size() > 0) {
						writePrivateGroup(privateDataSet, g);
					}
					g.writeEndObject(); // instance Object
				}
			}
			g.writeEndArray(); // instances
			g.writeEndObject(); // series object
		}
		g.writeEndArray(); // series
		g.writeEndObject(); // studies
		g.writeEndObject(); // begin
		g.close();
	}

	public static void writePatientIndex(OutputStream out, Study study,
			String headerName, String headerContent) throws IOException,
			JsonGenerationException {

		JsonFactory f = new JsonFactory();
		JsonGenerator g = f.createGenerator(out, JsonEncoding.UTF8);
		if (prettyPrint) {
			g.useDefaultPrettyPrinter();
		}

		g.writeStartObject();// whole json

		if (headerName != null) {
			JsonParser jp = f.createParser(headerContent);
			jp.nextToken();
			g.writeFieldName(headerName);
			insertJson(jp, g);
		}

		Attributes patientDataSet = extractPatientAttributes(study.dataSet(),
				PatientTag.getPatientTags());
		writeDataSet(patientDataSet, g);

		g.writeFieldName(STUDY);
		g.writeStartArray();
		g.writeString(study.getUID());
		g.writeEndArray();

		g.writeEndObject();// whole json
		g.close();
	}

	public static void writeStudyIndex(OutputStream out, Study study,
			String headerName, String headerContent, boolean includePatient)
			throws IOException, JsonGenerationException {

		JsonFactory f = new JsonFactory();
		JsonGenerator g = f.createGenerator(out, JsonEncoding.UTF8);
		if (prettyPrint) {
			g.useDefaultPrettyPrinter();
		}

		g.writeStartObject();// whole json

		if (headerName != null) {
			JsonParser jp = f.createParser(headerContent);
			jp.nextToken();
			g.writeFieldName(headerName);
			insertJson(jp, g);
		}

		g.writeStringField(DOCTYPE, "study");

		Attributes patientDataSet = extractPatientAttributes(study.dataSet(),
				PatientTag.getPatientTags());

		writeTranferSyntax(study.fmi().getString(Tag.TransferSyntaxUID), g);

		if (includePatient) {
			writeDataSet(patientDataSet, g);
		}

		writeDataSet(study.dataSet(), g);

		g.writeFieldName(SERIES);
		g.writeStartArray();// series
		for (Series series : study) {
			g.writeString(series.getUID());
		}
		g.writeEndArray(); // series

		g.writeEndObject();// whole json
		g.close();
	}

	public static void writeSeriesIndex(OutputStream out, Series series,
			String headerName, String headerContent) throws IOException,
			JsonGenerationException {

		JsonFactory f = new JsonFactory();
		JsonGenerator g;
		g = f.createGenerator(out, JsonEncoding.UTF8);
		if (prettyPrint) {
			g.useDefaultPrettyPrinter();
		}

		g.writeStartObject();// whole json

		if (headerName != null) {
			JsonParser jp = f.createParser(headerContent);
			jp.nextToken();
			g.writeFieldName(headerName);
			insertJson(jp, g);
		}

		g.writeStringField(DOCTYPE, "series");
		Study study = series.getStudy();
		writeAttribute(Tag.StudyInstanceUID,
				ElementDictionary.vrOf(Tag.StudyInstanceUID, null),
				study.getUID(), g);

		writeDataSet(series.dataSet(), g);

		g.writeFieldName(INSTANCES);
		g.writeStartArray();// instances
		for (Instance instance : series) {
			g.writeString(instance.getUID());
		}
		g.writeEndArray();// instances

		g.writeEndObject();// whole json
		g.close();
	}

	public static void writeInstanceIndex(OutputStream out, Instance instance,
			Attributes privateDataSet, String headerName, String headerContent)
			throws IOException, JsonGenerationException {

		JsonFactory f = new JsonFactory();
		JsonGenerator g;
		g = f.createGenerator(out, JsonEncoding.UTF8);
		if (prettyPrint) {
			g.useDefaultPrettyPrinter();
		}

		g.writeStartObject();// whole json

		if (headerName != null) {
			JsonParser jp = f.createParser(headerContent);
			jp.nextToken();
			g.writeFieldName(headerName);
			insertJson(jp, g);
		}

		g.writeStringField(DOCTYPE, "instance");

		Series series = instance.getSeries();
		Study study = series.getStudy();
		writeAttribute(Tag.StudyInstanceUID,
				ElementDictionary.vrOf(Tag.StudyInstanceUID, null),
				study.getUID(), g);
		writeAttribute(Tag.SeriesInstanceUID,
				ElementDictionary.vrOf(Tag.SeriesInstanceUID, null),
				series.getUID(), g);

		writeDataSet(instance.dataSet(), g);
		if (privateDataSet.size() > 0) {
			writePrivateGroup(privateDataSet, g);
		}
		g.writeEndObject();// whole json
		g.close();
	}

	public static String encodeReservedCharacter(String unescape) {
		StringBuffer escape = new StringBuffer();
		for (int i = 0; i < unescape.length(); i++) {
			switch (unescape.charAt(i)) {
			case '\"':
				escape.append("%22");
			case '\\':
				escape.append("%92");
				break;
			case '/':
				escape.append("%2F");
				break;
			case ' ':
				escape.append("%20");
				break;
			default:
				escape.append(unescape.charAt(i));
				break;
			}
		}
		return escape.toString();
	}

	public static void setSkipInvalidValue(boolean value) {
		skipInvalidValue = value;
	}

	public static void setPrintStyle(boolean value) {
		prettyPrint = value;
	}

	public static boolean isDigit(String str) {
		try {
			Double.parseDouble(str);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
}
