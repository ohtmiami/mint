package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.PrintStream;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.SpecificCharacterSet;
import org.dcm4che.data.VR;
import org.dcm4che.util.TagUtils;

import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class StudyPrinter {

    private int lineWidth = 128;

    public final int getLineWidth() {

        return lineWidth;
    }

    public final void setLineWidth(int lineWidth) {

        this.lineWidth = lineWidth;
    }

    private void printAttribute(String privateCreator, int tag, VR vr,
            Object value, SpecificCharacterSet cs, PrintStream out,
            String prefix) {

        StringBuilder sb = new StringBuilder(lineWidth);
        sb.append(prefix).append(TagUtils.toString(tag)).append(' ').append(vr)
                .append(" [");
        if (vr.prompt(value, false, cs, lineWidth - sb.length() - 1, sb)) {
            sb.append("] ").append(
                    ElementDictionary.keywordOf(tag, privateCreator));
            if (sb.length() > lineWidth) {
                sb.setLength(lineWidth);
            }
        }
        out.println(sb);
    }

    public void printDataSet(Attributes dataSet, PrintStream out, String prefix) {

        if (dataSet.isEmpty()) {
            return;
        }

        int numAttributes = dataSet.size();
        if (dataSet.tagAt(0) < 0) {
            int firstTagIndex = 1;
            for (; firstTagIndex < numAttributes; firstTagIndex++) {
                if (dataSet.tagAt(firstTagIndex) >= 0) {
                    break;
                }
            }
            printDataSet(dataSet, firstTagIndex, numAttributes, out, prefix);
            printDataSet(dataSet, 0, firstTagIndex, out, prefix);
        } else {
            printDataSet(dataSet, 0, numAttributes, out, prefix);
        }
    }

    private void printDataSet(Attributes dataSet, int startIndex, int endIndex,
            PrintStream out, String prefix) {

        int creatorTag = 0;
        String privateCreator = null;
        for (int i = startIndex; i < endIndex; i++) {
            int tag = dataSet.tagAt(i);
            if (TagUtils.isPrivateTag(tag)) {
                int tmp = TagUtils.creatorTagOf(tag);
                if (creatorTag != tmp) {
                    creatorTag = tmp;
                    privateCreator = dataSet.getString(creatorTag, null);
                }
            } else {
                creatorTag = 0;
                privateCreator = null;
            }
            VR vr = dataSet.vrAt(i);
            Object value = dataSet.valueAt(i);
            printAttribute(privateCreator, tag, vr, value,
                    dataSet.getSpecificCharacterSet(vr), out, prefix);
            if (value instanceof Sequence) {
                printItems((Sequence) value, out, prefix + '>');
            }
        }
    }

    private void printItems(Sequence sequence, PrintStream out, String prefix) {

        int itemNumber = 0;
        for (Attributes item : sequence) {
            out.print(prefix);
            out.printf("Item #%d\n", ++itemNumber);
            printDataSet(item, out, prefix);
        }
    }

    public void printStudy(Study study, PrintStream out) {

        out.printf("======== BEGIN STUDY ========\n");
        printDataSet(study.fmi(), out, "");
        printDataSet(study.dataSet(), out, "");
        int seriesNumber = 0;
        for (Series series : study) {
            out.printf(" ======== BEGIN SERIES %02d ========\n", ++seriesNumber);
            printDataSet(series.fmi(), out, " ");
            printDataSet(series.dataSet(), out, " ");
            int instanceNumber = 0;
            for (Instance instance : series) {
                out.printf("  ======== BEGIN INSTANCE %02d-%02d ========\n",
                        seriesNumber, ++instanceNumber);
                printDataSet(instance.fmi(), out, "  ");
                printDataSet(instance.dataSet(), out, "  ");
                out.printf("  ======== END INSTANCE %02d-%02d ========\n",
                        seriesNumber, instanceNumber);
            }
            out.printf(" ======== END SERIES %02d ========\n", seriesNumber);
        }
        out.printf("======== END STUDY ========\n");
    }
}
