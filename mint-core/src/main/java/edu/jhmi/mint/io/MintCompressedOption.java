package edu.jhmi.mint.io;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */


import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class MintCompressedOption extends MintHeaderOption<String>{
    
    public MintCompressedOption() {
    }

    public MintCompressedOption(String algorithm) {

        setValue(algorithm);
    }
    @Override
    public void setValue(String value) {

        if (value == null) {
            throw new NullPointerException("value");
        }
        super.setValue(value);
    }

    @Override
    public int getBitmask() {
        
        return MintHeaderOption.getCompressedBitMask();
    }

    @Override
    public int calculateLength() {
        
        return this.getValue().length();
    }

    @Override
    protected void encodeValue(ByteBuffer buffer) {
        
        String algorithm = this.getValue();
        byte[] algorithmBytes = algorithm.getBytes(StandardCharsets.US_ASCII);
        buffer.put(algorithmBytes);        
    }

    @Override
    protected void decodeValue(ByteBuffer buffer, int length)
            throws MalformedMintHeaderException {
        
        byte[] algorithmBytes = new byte[length];
        try {
            buffer.get(algorithmBytes);
            String algorithm = new String(algorithmBytes, StandardCharsets.US_ASCII);
            setValue(algorithm);
        } catch (Exception e) {
            throw new MalformedMintHeaderException(e);
        }
    }
}
