package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.dcm4che.data.Tag;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */

public class InstanceTag {
	//private static final int[] instanceStaticTags = { Tag.SOPInstanceUID };
	private static final int[] instanceTags = { Tag.SOPInstanceUID,Tag.SOPClassUID,
			Tag.InstanceCreationDate, Tag.InstanceCreationTime,
			Tag.InstanceNumber };

	private static final Set<Integer> instanceTagsSet = new HashSet<Integer>();

	static {
		Arrays.sort(instanceTags);
		for (int i = 0; i < instanceTags.length; i++) {
			instanceTagsSet.add(instanceTags[i]);
		}
	}

	public static boolean isInstanceTag(int tag) {
		return instanceTagsSet.contains(tag);
	}

	public static int[] getInstanceTags() {
		return instanceTags;
	}
}
