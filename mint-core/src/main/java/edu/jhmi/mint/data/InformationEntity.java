package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public abstract class InformationEntity implements Serializable {

    private static final long serialVersionUID = 7002207926416070100L;

    protected final NormalizedDataSet fmi;

    protected final NormalizedDataSet dataSet;

    private InformationEntity parent;

    private final Map<String, InformationEntity> children;

    protected InformationEntity(String uid, InformationEntity parent,
            int initialCapacity) {

        fmi = createFMI(uid, parent);
        dataSet = createDataSet(uid, parent);
        dataSet.setPeer(fmi);
        if (parent != null) {
            parent.addChild(this);
        }
        children = new HashMap<String, InformationEntity>(initialCapacity);
    }

    protected InformationEntity(NormalizedDataSet fmi,
            NormalizedDataSet dataSet, InformationEntity parent, int childCount) {

        this.fmi = fmi;
        this.dataSet = dataSet;
        dataSet.setPeer(fmi);
        if (parent != null) {
            parent.addChild(this);
        }
        children = new HashMap<String, InformationEntity>(childCount);
    }

    protected abstract NormalizedDataSet createFMI(String uid,
            InformationEntity parent);

    protected abstract NormalizedDataSet createDataSet(String uid,
            InformationEntity parent);

    public String getUID() {

        return dataSet.getUID();
    }

    // TODO setUID()

    protected InformationEntity getParent() {

        return parent;
    }

    protected int childCount() {

        return children.size();
    }

    protected InformationEntity getChild(String uid) {

        return children.get(uid);
    }

    protected Iterator<InformationEntity> childIterator() {

        return children.values().iterator();
    }

    protected void addChild(InformationEntity child)
            throws DuplicateUIDException {

        String childUID = child.getUID();
        if (children.containsKey(childUID)) {
            throw new DuplicateUIDException(childUID);
        }

        children.put(childUID, child);
        child.parent = this;
    }

    protected InformationEntity removeChild(String uid) {

        InformationEntity child = children.remove(uid);
        if (child != null) {
            child.parent = null;
        }
        return child;
    }
}
