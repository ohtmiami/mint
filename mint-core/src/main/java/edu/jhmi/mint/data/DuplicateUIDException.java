package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class DuplicateUIDException extends RuntimeException {

    private static final long serialVersionUID = 7011705248837771618L;

    private final String uid;

    public DuplicateUIDException(String uid) {

        super(String.format("Found duplicate UID %s", uid));
        this.uid = uid;
    }

    public String getUID() {

        return uid;
    }
}
