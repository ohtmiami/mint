package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.dcm4che.data.Tag;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */

public class SeriesTag {
	private static final int[] seriesTags = { Tag.SeriesInstanceUID,
			Tag.SeriesNumber, Tag.SeriesDate, Tag.SeriesTime, Tag.ProtocolName,
			Tag.SeriesDescription, Tag.SeriesDescriptionCodeSequence };

	private static final Set<Integer> seriesTagsSet = new HashSet<Integer>();
	static {
		Arrays.sort(seriesTags);
		for (int i = 0; i < seriesTags.length; i++) {
			seriesTagsSet.add(seriesTags[i]);
		}
	}

	public static boolean isSeriesTag(int tag) {
		return seriesTagsSet.contains(tag);
	}

	public static int[] getSeriesTags() {
		return seriesTags;
	}
	
}
