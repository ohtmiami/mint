package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.SpecificCharacterSet;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.util.TagUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class InstanceDataSet extends NormalizedDataSet {

    private static final long serialVersionUID = 2702097597073494869L;

    private int studyLevelIndex = 0;

    private int seriesLevelIndex = 0;

    InstanceDataSet(String sopInstanceUID, SeriesDataSet seriesDataSet,
            int initialAttributesCapacity) {

        super(sopInstanceUID, initialAttributesCapacity, seriesDataSet, 0);
    }

    InstanceDataSet(String sopInstanceUID, SeriesDataSet seriesDataSet) {

        this(sopInstanceUID, seriesDataSet, 16);
    }

    InstanceDataSet(String sopInstanceUID, Attributes instanceLevelAttributes,
            long minTag, long maxTag, int initialAttributesCapacity,
            SeriesDataSet seriesDataSet) {

        super(sopInstanceUID, instanceLevelAttributes, minTag, maxTag,
                initialAttributesCapacity, seriesDataSet, 0);
    }

    public SeriesDataSet getSeries() {

        return (SeriesDataSet) this.parent;
    }

    public StudyDataSet getStudy() {

        return getSeries().getStudy();
    }

    public int frameCount() {

        Sequence perFrameSequence = this
                .getSequence(Tag.PerFrameFunctionalGroupsSequence);
        return perFrameSequence == null ? -1 : perFrameSequence.size();
    }

    @Override
    protected String updateUID(String uid) {

        SeriesDataSet seriesDataSet = getSeries();
        String oldUID = super.updateUID(uid);
        this.updateValueHierarchical(Tag.SOPInstanceUID, uid);
        seriesDataSet.removeChild(oldUID);
        seriesDataSet.addChild(this);
        return oldUID;
    }

    private void resetIndices() {

        studyLevelIndex = 0;
        seriesLevelIndex = 0;
    }

    private static boolean isNormalizable(int tag, VR vr) {

        if (vr == VR.BR) {
            return false;
        }

        switch (tag) {
        case Tag.SharedFunctionalGroupsSequence:
        case Tag.PerFrameFunctionalGroupsSequence:
        case Tag.PixelData:
            // TODO Bulkdata tags
            return false;

        default:
            return true;
        }
    }

    public boolean add(int tag, VR vr, Object value, boolean bigEndian,
            SpecificCharacterSet cs, boolean append) {

        if (!append) {
            resetIndices();
        }

        SeriesDataSet series = getSeries();
        StudyDataSet study = series.getStudy();
        // Treat all parameters as final
        Object valueCopy = value;
        if (bigEndian && valueCopy instanceof byte[]) {
            valueCopy = vr.toggleEndian((byte[]) valueCopy, true);
        }

        if (study.seriesCount() == 1 && series.instanceCount() == 1) {
            NormalizedDataSet destinationDataSet = isNormalizable(tag, vr) ? study
                    : this;
            if (append) {
                destinationDataSet.insertAtBack(tag, vr, valueCopy);
            } else {
                destinationDataSet.insert(tag, vr, valueCopy);
            }
            return true;
        }

        boolean foundDuplicate = false;
        int studyTag = demoteAllFromStudyLevelUpTo(tag);
        if (studyTag == tag) {
            if (study.equalValues(studyLevelIndex, vr, valueCopy, false, cs)) {
                ++studyLevelIndex;
                foundDuplicate = true;
            } else {
                study.demote(studyLevelIndex, this);
            }
        }
        if (series.instanceCount() > 1) {
            int seriesTag = demoteAllFromSeriesLevelUpTo(tag);
            if (seriesTag == tag) {
                // TODO Assert foundDuplicate == false
                if (series.equalValues(seriesLevelIndex, vr, valueCopy, false,
                        cs)) {
                    ++seriesLevelIndex;
                    foundDuplicate = true;
                } else {
                    series.demote(seriesLevelIndex, this);
                }
            }
        }
        if (!foundDuplicate) {
            NormalizedDataSet destinationDataSet = series.instanceCount() == 1
                    && isNormalizable(tag, vr) ? series : this;
            if (append) {
                destinationDataSet.insertAtBack(tag, vr, valueCopy);
            } else {
                destinationDataSet.insert(tag, vr, valueCopy);
            }
        }
        return !foundDuplicate;
    }

    public boolean add(int tag, VR vr, Object value, boolean append) {

        return add(tag, vr, value, false, this.getSpecificCharacterSet(),
                append);
    }

    public void addAll(Attributes dataSet, boolean append) {

        if (!append) {
            resetIndices();
        }

        for (int i = 0; i < dataSet.size(); i++) {
            add(dataSet.tagAt(i), dataSet.vrAt(i), dataSet.valueAt(i),
                    dataSet.bigEndian(), dataSet.getSpecificCharacterSet(),
                    true);
        }
    }

    public void finish() {

        SeriesDataSet series = getSeries();
        StudyDataSet study = series.getStudy();
        if (study.seriesCount() > 1 || series.instanceCount() > 1) {
            demoteAllFromStudyLevelUpTo(-1);
            if (series.instanceCount() > 1) {
                demoteAllFromSeriesLevelUpTo(-1);
            }
        }
    }

    private int demoteAllFromStudyLevelUpTo(int tag) {

        StudyDataSet study = getStudy();
        int studyTag = study.tagAt(studyLevelIndex);
        while (studyTag != -1 && (tag == -1 || studyTag < tag)) {
            study.demote(studyLevelIndex, this);
            studyTag = study.tagAt(studyLevelIndex);
        }
        return studyTag;
    }

    private int demoteAllFromSeriesLevelUpTo(int tag) {

        SeriesDataSet series = getSeries();
        int seriesTag = series.tagAt(seriesLevelIndex);
        while (seriesTag != -1 && (tag == -1 || seriesTag < tag)) {
            series.demote(seriesLevelIndex, this);
            seriesTag = series.tagAt(seriesLevelIndex);
        }
        return seriesTag;
    }

    @Override
    protected void onAttributeInsertion(int tag, VR vr, Object value) {

        if (tag == Tag.SOPInstanceUID) {
            String uid = value.toString();
            if (!this.getUID().equals(uid)) {
                this.updateUID(uid);
            }
        }
        super.onAttributeInsertion(tag, vr, value);
    }

    @Override
    protected void onAttributeRemoval(int tag, VR vr, Object value) {

        if (tag == Tag.SOPInstanceUID) {
            // Undo removal
            this.set(tag, vr, value);
            throw new UnsupportedOperationException(String.format(
                    "Removal of %s %s is not allowed", TagUtils.toString(tag),
                    ElementDictionary.keywordOf(tag, null)));
        }
        super.onAttributeRemoval(tag, vr, value);
    }

    @Override
    public Object getValueHierarchical(int tag, VR.Holder vr) {

        return super.getValueHierarchical(tag, vr);
    }

    @Override
    public String getStringHierarchical(int tag) {

        return super.getStringHierarchical(tag);
    }
}
