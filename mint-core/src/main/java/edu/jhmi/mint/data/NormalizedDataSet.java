package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Fragments;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.SpecificCharacterSet;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.data.Value;
import org.dcm4che.util.ByteUtils;
import org.dcm4che.util.StringUtils;
import org.dcm4che.util.TagUtils;
import org.dcm4che.util.UIDUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class NormalizedDataSet extends Attributes {

    private static final long serialVersionUID = 1092425396163307889L;

    private String uid;

    private final Map<String, NormalizedDataSet> children;

    private NormalizedDataSet peer;

    protected NormalizedDataSet(String uid, int initialAttributesCapacity,
            NormalizedDataSet parent, int initialChildrenCapacity) {

        super(initialAttributesCapacity);
        if (!UIDUtils.isValid(uid)) {
            throw new IllegalArgumentException(String.format("Invalid UID %s",
                    uid));
        }

        this.uid = uid;
        if (parent != null) {
            parent.addChild(this);
        }
        children = new HashMap<String, NormalizedDataSet>(
                initialChildrenCapacity);
        peer = null;
    }

    protected NormalizedDataSet(String uid, Attributes dataSet, long minTag,
            long maxTag, int initialAttributesCapacity,
            NormalizedDataSet parent, int initialChildrenCapacity) {

        super(initialAttributesCapacity);
        if (!UIDUtils.isValid(uid)) {
            throw new IllegalArgumentException(String.format("Invalid UID %s",
                    uid));
        }

        this.uid = uid;
        if (parent != null) {
            parent.addChild(this);
        }
        for (int i = 0; i < dataSet.size(); i++) {
            int tag = dataSet.tagAt(i);
            long unsignedTag = tag & 0xFFFFFFFFL;
            if (unsignedTag >= minTag) {
                if (unsignedTag <= maxTag) {
                    doInsertAt(this.size, tag, dataSet.vrAt(i),
                            dataSet.valueAt(i));
                } else if (tag >= 0) {
                    break;
                }
            }
        }
        children = new HashMap<String, NormalizedDataSet>(
                initialChildrenCapacity);
        peer = null;
    }

    private NormalizedDataSet(int initialCapacity) {

        super(initialCapacity);
        uid = null;
        children = Collections.emptyMap();
        peer = null;
    }

    public static NormalizedDataSet newItem(int initialCapacity) {

        return new NormalizedDataSet(initialCapacity);
    }

    public String getUID() {

        return uid;
    }

    void setUID(String uid) {

        if (this.uid == null) {
            throw new UnsupportedOperationException(
                    "Items are not allowed to have a UID");
        }

        if (!this.uid.equals(uid)) {
            updateUID(uid);
        }
    }

    protected String updateUID(String uid) {

        if (!UIDUtils.isValid(uid)) {
            throw new IllegalArgumentException(String.format("Invalid UID %s",
                    uid));
        }

        String oldUID = this.uid;
        this.uid = uid;
        return oldUID;
    }

    protected int childCount() {

        return children.size();
    }

    protected NormalizedDataSet getChild(String uid) {

        return children.get(uid);
    }

    protected Iterator<NormalizedDataSet> childIterator() {

        return children.values().iterator();
    }

    protected void addChild(NormalizedDataSet child)
            throws DuplicateUIDException {

        if (uid == null) {
            throw new UnsupportedOperationException(
                    "Items are not allowed to have children");
        }

        if (children.containsKey(child.uid)) {
            throw new DuplicateUIDException(child.uid);
        }

        children.put(child.uid, child);
        child.parent = this;
    }

    protected NormalizedDataSet removeChild(String uid) {

        if (uid == null) {
            throw new UnsupportedOperationException(
                    "Items are not allowed to have children");
        }

        NormalizedDataSet child = children.remove(uid);
        if (child != null) {
            child.parent = null;
        }
        return child;
    }

    public NormalizedDataSet getPeer() {

        return peer;
    }

    public void setPeer(NormalizedDataSet peer) {

        if (peer == null) {
            this.peer = null;
            return;
        }

        if (this == peer) {
            throw new IllegalArgumentException(
                    "Peers must be different objects");
        }
        if (!peer.getClass().equals(this.getClass())) {
            throw new IllegalArgumentException("Peers must be of the same type");
        }
        if (!peer.getUID().equals(this.getUID())) {
            throw new IllegalArgumentException("Peers must have the same UID");
        }

        this.peer = peer;
        peer.peer = this;
    }

    private void doInsertAt(int index, int tag, VR vr, Object value) {

        Object newValue = value;
        if (value instanceof Sequence) {
            if (vr != VR.SQ) {
                throw new IllegalArgumentException(String.format(
                        "Invalid VR %s for sequence %s %s", vr,
                        TagUtils.toString(tag),
                        ElementDictionary.keywordOf(tag, null)));
            }
            Sequence sequence = (Sequence) value;
            Sequence newSequence = this.newSequenceAt(index, tag,
                    sequence.size());
            for (Attributes item : sequence) {
                NormalizedDataSet newItem = new NormalizedDataSet(item.size());
                for (int i = 0; i < item.size(); i++) {
                    newItem.insertAtBack(item.tagAt(i), item.vrAt(i),
                            item.valueAt(i));
                }
                newSequence.add(newItem);
            }
            newValue = newSequence;
        } else if (value instanceof Fragments) {
            Fragments fragments = (Fragments) value;
            Fragments newFragments = this.newFragmentsAt(index, tag, vr,
                    fragments.size());
            for (Object fragment : fragments) {
                newFragments.add(fragment);
            }
            newValue = newFragments;
        } else {
            this.insert(index, tag, vr, value);
        }
        onAttributeInsertion(tag, vr, newValue);
    }

    public int insert(int tag, VR vr, Object value)
            throws AttributeAlreadyExistsException {

        int index = this.indexForInsertOf(tag);
        if (index >= 0) {
            throw new AttributeAlreadyExistsException(tag);
        }

        int insertionIndex = -index - 1;
        doInsertAt(insertionIndex, tag, vr, value);
        return insertionIndex;
    }

    public int insertAll(Attributes dataSet) {

        int numAttributes = dataSet.size();
        for (int i = 0; i < numAttributes; i++) {
            insert(dataSet.tagAt(i), dataSet.vrAt(i), dataSet.valueAt(i));
        }
        return numAttributes;
    }

    private void insertAt(int index, int tag, VR vr, Object value)
            throws AttributeAlreadyExistsException,
            AttributeOutOfOrderException {

        if (index > 0) {
            int prevTag = this.tags[index - 1];
            if (prevTag == tag) {
                throw new AttributeAlreadyExistsException(tag);
            }
            if (prevTag > tag) {
                throw new AttributeOutOfOrderException(tag, index, prevTag,
                        index - 1);
            }
        }
        if (index < this.size) {
            int nextTag = this.tags[index];
            if (nextTag == tag) {
                throw new AttributeAlreadyExistsException(tag);
            }
            if (nextTag < tag) {
                throw new AttributeOutOfOrderException(tag, index, nextTag,
                        index);
            }
        }
        doInsertAt(index, tag, vr, value);
    }

    protected void insertAtBack(int tag, VR vr, Object value)
            throws AttributeAlreadyExistsException,
            AttributeOutOfOrderException {

        insertAt(this.size, tag, vr, value);
    }

    protected Object removeAt(int index) {

        if (index < 0 || index >= this.size) {
            return null;
        }

        int tag = this.tags[index];
        VR vr = this.vrs[index];
        Object value = this.values[index];

        int numMoved = this.size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(this.tags, index + 1, this.tags, index, numMoved);
            System.arraycopy(this.vrs, index + 1, this.vrs, index, numMoved);
            System.arraycopy(this.values, index + 1, this.values, index,
                    numMoved);
        }
        this.values[--this.size] = null;

        onAttributeRemoval(tag, vr, value);
        return value;
    }

    @Override
    public Object remove(String privateCreator, int tag) {

        int index = indexOf(privateCreator, tag);
        if (index < 0) {
            return null;
        }

        return removeAt(index);
    }

    protected void onAttributeInsertion(int tag, VR vr, Object value) {

        if (tag == Tag.SpecificCharacterSet) {
            this.containsSpecificCharacterSet = true;
            this.cs = null;
        } else if (tag == Tag.TimezoneOffsetFromUTC) {
            this.containsTimezoneOffsetFromUTC = value != Value.NULL;
            this.tz = null;
        }
    }

    protected void onAttributeRemoval(int tag, VR vr, Object value) {

        if (tag == Tag.SpecificCharacterSet) {
            this.containsSpecificCharacterSet = false;
            this.cs = null;
        } else if (tag == Tag.TimezoneOffsetFromUTC) {
            this.containsTimezoneOffsetFromUTC = false;
            this.tz = null;
        }
    }

    @Override
    protected Object set(String privateCreator, int tag, VR vr, Object value) {

        if (vr == null) {
            throw new NullPointerException("vr");
        }

        if (privateCreator != null) {
            int creatorTag = this.creatorTagOf(privateCreator, tag, true);
            tag = TagUtils.toPrivateTag(creatorTag, tag);
        }

        if (TagUtils.isGroupLength(tag)) {
            return null;
        }

        Object oldValue = this.set(tag, vr, value);
        onAttributeInsertion(tag, vr, value);
        return oldValue;
    }

    protected boolean equalValues(int index, VR otherVR, Object otherValue,
            boolean otherBigEndian, SpecificCharacterSet otherCS) {

        VR vr = this.vrAt(index);
        if (vr != otherVR) {
            return false;
        }
        if (vr.isStringType()) {
            if (vr == VR.IS) {
                return equalISValues(index, otherVR, otherValue,
                        otherBigEndian, otherCS);
            } else if (vr == VR.DS) {
                return equalDSValues(index, otherVR, otherValue,
                        otherBigEndian, otherCS);
            } else {
                return equalStringValues(index, otherVR, otherValue,
                        otherBigEndian, otherCS);
            }
        }

        Object value1 = this.valueAt(index);
        Object value2 = otherValue;
        if (value1 instanceof byte[]) {
            if (value2 instanceof byte[]
                    && ((byte[]) value1).length == ((byte[]) value2).length) {
                if (otherBigEndian) {
                    value2 = vr.toggleEndian((byte[]) value2, true);
                }
                return Arrays.equals((byte[]) value1, (byte[]) value2);
            } else {
                return false;
            }
        } else if (value1 instanceof Sequence) {
            if (!(value2 instanceof Sequence)) {
                return false;
            }
            return areEqual((Sequence) value1, (Sequence) value2);
        } else if (value1 instanceof Fragments) {
            if (!(value2 instanceof Fragments)) {
                return false;
            }
            return areEqual((Fragments) value1, (Fragments) value2, vr,
                    otherBigEndian);
        } else {
            return value1.equals(value2);
        }
    }

    @Override
    protected boolean equalValues(Attributes other, int index, int otherIndex) {

        return equalValues(index, other.vrAt(otherIndex),
                other.valueAt(otherIndex), other.bigEndian(),
                other.getSpecificCharacterSet());
    }

    private boolean equalDSValues(int index, VR otherVR, Object otherValue,
            boolean otherBigEndian, SpecificCharacterSet otherCS) {

        try {
            return Arrays.equals(decodeDSValue(this, index),
                    decodeDSValue(otherVR, otherValue, otherBigEndian));
        } catch (NumberFormatException e) {
            return equalStringValues(index, otherVR, otherValue,
                    otherBigEndian, otherCS);
        }
    }

    @Override
    protected boolean equalDSValues(Attributes other, int index, int otherIndex) {

        return equalDSValues(index, other.vrAt(otherIndex),
                other.valueAt(otherIndex), other.bigEndian(),
                other.getSpecificCharacterSet());
    }

    private boolean equalISValues(int index, VR otherVR, Object otherValue,
            boolean otherBigEndian, SpecificCharacterSet otherCS) {

        try {
            return Arrays.equals(decodeISValue(this, index),
                    decodeISValue(otherVR, otherValue, otherBigEndian));
        } catch (NumberFormatException e) {
            return equalStringValues(index, otherVR, otherValue,
                    otherBigEndian, otherCS);
        }
    }

    @Override
    protected boolean equalISValues(Attributes other, int index, int otherIndex) {

        return equalISValues(index, other.vrAt(otherIndex),
                other.valueAt(otherIndex), other.bigEndian(),
                other.getSpecificCharacterSet());
    }

    private boolean equalStringValues(int index, VR otherVR, Object otherValue,
            boolean otherBigEndian, SpecificCharacterSet otherCS) {

        Object value1 = decodeStringValue(this, index);
        Object value2 = decodeStringValue(otherVR, otherValue, otherBigEndian,
                maskSpecificCharacterSet(otherCS, otherVR));
        if (value1 instanceof String[]) {
            if (value2 instanceof String[]) {
                return Arrays.equals((String[]) value1, (String[]) value2);
            }
        } else {
            return value1.equals(value2);
        }
        return false;
    }

    @Override
    protected boolean equalStringValues(Attributes other, int index,
            int otherIndex) {

        return equalStringValues(index, other.vrAt(otherIndex),
                other.valueAt(otherIndex), other.bigEndian(),
                other.getSpecificCharacterSet());
    }

    private static SpecificCharacterSet maskSpecificCharacterSet(
            SpecificCharacterSet cs, VR vr) {

        return vr.useSpecificCharacterSet() ? cs : SpecificCharacterSet.DEFAULT;
    }

    private static Object decodeStringValue(VR vr, Object value,
            boolean bigEndian, SpecificCharacterSet cs) {

        // Treat all parameters as final
        Object valueCopy = value;
        if (valueCopy instanceof byte[]) {
            valueCopy = vr.toStrings(valueCopy, bigEndian, cs);
            if (valueCopy instanceof String && ((String) valueCopy).isEmpty()) {
                valueCopy = Value.NULL;
            }
        }
        return valueCopy;
    }

    private static Object decodeStringValue(Attributes dataSet, int index) {

        VR vr = dataSet.vrAt(index);
        return decodeStringValue(vr, dataSet.valueAt(index),
                dataSet.bigEndian(), dataSet.getSpecificCharacterSet(vr));
    }

    private static double[] decodeDSValue(VR vr, Object value, boolean bigEndian) {

        // Treat all parameters as final
        Object valueCopy = value;
        if (valueCopy == Value.NULL) {
            return ByteUtils.EMPTY_DOUBLES;
        }

        if (valueCopy instanceof double[]) {
            return (double[]) valueCopy;
        }

        double[] ds;
        if (valueCopy instanceof byte[]) {
            valueCopy = vr.toStrings(valueCopy, bigEndian,
                    SpecificCharacterSet.DEFAULT);
        }
        if (valueCopy instanceof String) {
            String s = (String) valueCopy;
            if (s.isEmpty()) {
                return ByteUtils.EMPTY_DOUBLES;
            }
            ds = new double[] { StringUtils.parseDS(s) };
        } else { // value instanceof String[]
            String[] ss = (String[]) valueCopy;
            ds = new double[ss.length];
            for (int i = 0; i < ds.length; i++) {
                String s = ss[i];
                ds[i] = (s != null && !s.isEmpty()) ? StringUtils.parseDS(s)
                        : Double.NaN;
            }
        }
        return ds;
    }

    private static double[] decodeDSValue(Attributes dataSet, int index) {

        return decodeDSValue(dataSet.vrAt(index), dataSet.valueAt(index),
                dataSet.bigEndian());
    }

    private static int[] decodeISValue(VR vr, Object value, boolean bigEndian) {

        // Treat all parameters as final
        Object valueCopy = value;
        if (valueCopy == Value.NULL) {
            return ByteUtils.EMPTY_INTS;
        }

        if (valueCopy instanceof int[]) {
            return (int[]) valueCopy;
        }

        int[] is;
        if (valueCopy instanceof byte[]) {
            valueCopy = vr.toStrings(valueCopy, bigEndian,
                    SpecificCharacterSet.DEFAULT);
        }
        if (valueCopy instanceof String) {
            String s = (String) valueCopy;
            if (s.isEmpty()) {
                return ByteUtils.EMPTY_INTS;
            }
            is = new int[] { StringUtils.parseIS(s) };
        } else { // value instanceof String[]
            String[] ss = (String[]) valueCopy;
            is = new int[ss.length];
            for (int i = 0; i < is.length; i++) {
                String s = ss[i];
                is[i] = (s != null && !s.isEmpty()) ? StringUtils.parseIS(s)
                        : Integer.MIN_VALUE;
            }
        }
        return is;
    }

    private static int[] decodeISValue(Attributes dataSet, int index) {

        return decodeISValue(dataSet.vrAt(index), dataSet.valueAt(index),
                dataSet.bigEndian());
    }

    private boolean areEqual(Fragments fragments1, Fragments fragments2, VR vr,
            boolean toggleEndian) {

        if (fragments1.size() != fragments2.size()) {
            return false;
        }

        for (int i = 0; i < fragments1.size(); i++) {
            Object fragment1 = fragments1.get(i);
            Object fragment2 = fragments2.get(i);
            if (fragment1 instanceof byte[]) {
                if (fragment2 instanceof byte[]
                        && ((byte[]) fragment1).length == ((byte[]) fragment2).length) {
                    if (toggleEndian) {
                        fragment2 = vr.toggleEndian((byte[]) fragment2, true);
                    }
                    if (!Arrays.equals((byte[]) fragment1, (byte[]) fragment2)) {
                        return false;
                    }
                } else {
                    return false;
                }
            } else if (!fragment1.equals(fragment2)) {
                return false;
            }
        }
        return true;
    }

    private boolean areEqual(Sequence sequence1, Sequence sequence2) {

        if (sequence1.size() != sequence2.size()) {
            return false;
        }

        for (int i = 0; i < sequence1.size(); i++) {
            Attributes item1 = sequence1.get(i);
            Attributes item2 = sequence2.get(i);
            boolean equalItems = item1 instanceof NormalizedDataSet ? item1
                    .equals(item2) : item2.equals(item1);
            if (!equalItems) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof Attributes)) {
            return false;
        }

        Attributes other = (Attributes) o;
        if (this.size != other.size()) {
            return false;
        }

        for (int i = 0; i < this.size; i++) {
            int tag = this.tagAt(i);
            if (tag != other.tagAt(i) || !equalValues(other, i, i)) {
                return false;
            }
        }
        return true;
    }

    protected Object getValueHierarchical(int tag, VR.Holder vr) {

        Object value = null;
        Attributes dataSet = this;
        do {
            value = dataSet.getValue(tag, vr);
            dataSet = dataSet.getParent();
        } while (value == null && dataSet instanceof NormalizedDataSet);
        return value;
    }

    protected String getStringHierarchical(int tag) {

        String s = null;
        Attributes dataSet = this;
        do {
            s = dataSet.getString(tag);
            dataSet = dataSet.getParent();
        } while (s == null && dataSet instanceof NormalizedDataSet);
        return s;
    }

    protected Object updateValueHierarchical(int tag, Object newValue) {

        int index = -1;
        Attributes dataSet = this;
        do {
            index = ((NormalizedDataSet) dataSet).indexOf(null, tag);
            if (index >= 0) {
                return ((NormalizedDataSet) dataSet).set(tag,
                        dataSet.vrAt(index), newValue);
            }
            dataSet = dataSet.getParent();
        } while (dataSet instanceof NormalizedDataSet);
        return null;
    }
}
