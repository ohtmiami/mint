package edu.jhmi.mint.data;

import org.dcm4che.data.Attributes;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class Instance extends InformationEntity {

    private static final long serialVersionUID = 6638673870098589109L;

    Instance(String sopInstanceUID, Series series) {

        super(sopInstanceUID, series, 0);
    }

    Instance(String sopInstanceUID, Attributes instanceLevelAttributes,
            Series series) {

        super(new InstanceDataSet(sopInstanceUID, instanceLevelAttributes,
                0x00020000L, 0x0002FFFFL, 9, series.fmi()),
                new InstanceDataSet(sopInstanceUID, instanceLevelAttributes,
                        0x00030000L, 0xFFFFFFFFL,
                        instanceLevelAttributes.size(), series.dataSet()),
                series, 0);
    }

    @Override
    protected NormalizedDataSet createFMI(String uid, InformationEntity parent) {

        SeriesDataSet seriesFMI = ((Series) parent).fmi();
        return new InstanceDataSet(uid, seriesFMI, 9);
    }

    @Override
    protected NormalizedDataSet createDataSet(String uid,
            InformationEntity parent) {

        SeriesDataSet seriesDataSet = ((Series) parent).dataSet();
        return new InstanceDataSet(uid, seriesDataSet);
    }

    public InstanceDataSet fmi() {

        return (InstanceDataSet) this.fmi;
    }

    public InstanceDataSet dataSet() {

        return (InstanceDataSet) this.dataSet;
    }

    public Series getSeries() {

        return (Series) this.getParent();
    }

    public Study getStudy() {

        return getSeries().getStudy();
    }

    public int frameCount() {

        return dataSet().frameCount();
    }
}
