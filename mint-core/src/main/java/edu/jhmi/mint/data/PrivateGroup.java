package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */


import org.dcm4che.data.Attributes;
import org.dcm4che.data.VR;
import org.dcm4che.util.TagUtils;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */

public class PrivateGroup {

	private int numOfPGElements;
	
    private int numOfNonUNElements;
	
	public Attributes pgNonUNElements;

	private VR pgCreatorVR;

	private int pgCreatorTag;

	private String pgName = null;

	private boolean hasPGCreatorTag=false;
	
	private int pgGroupNumber;
	
	public PrivateGroup() {

		pgNonUNElements = new Attributes();
		numOfPGElements = 0;
		numOfNonUNElements=0;
	}

	public void setPGCreator(int tag, VR vr, String value) {

		if (vr != VR.LO) {
			System.out.println("Warning: VR of private creator tag "
					+ TagUtils.toHexString(tag) + " is not LO!");
		}
		pgCreatorVR = vr;
		pgCreatorTag = tag;
		pgGroupNumber = TagUtils.groupNumber(pgCreatorTag);
		pgName = value;
		hasPGCreatorTag=true;
	}

	public void insertPGElement(Attributes dataSet, int tag) {

		if (dataSet.getVR(tag) != VR.UN) {
			pgNonUNElements.addSelected(dataSet, tag);
			numOfNonUNElements++;
		}
		if(pgName==null){
			pgCreatorTag=TagUtils.creatorTagOf(tag);
			pgCreatorVR = VR.UN;
		}
		numOfPGElements++;		
	}

	public int getPGGroupNumber() {
		return pgGroupNumber;
	}

	public int getPGCreatorTag() {
		return pgCreatorTag;
	}

	public VR getPGCreatorVR() {
		return pgCreatorVR;
	}

	public int getNumOfPGElements() {
		return numOfPGElements;
	}

	public int getNumOfNonUNElements(){
		return numOfNonUNElements;
	}
	
	public int getNumOfUNElemenst(){
		return numOfPGElements-numOfNonUNElements;
	}
	
	public String getPGName() {
		return pgName;
	}

	public boolean hasPGCreator() {
		return hasPGCreatorTag;
	}

}
