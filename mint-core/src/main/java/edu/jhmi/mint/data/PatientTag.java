package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.dcm4che.data.Tag;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */

public class PatientTag {

	private static final int[] patientTags = { Tag.AdditionalPatientHistory,
			Tag.Allergies, Tag.AssigningAgencyOrDepartmentCodeSequence,
			Tag.AssigningFacilitySequence,
			Tag.AssigningJurisdictionCodeSequence, Tag.BranchOfService,
			Tag.BreedRegistrationNumber, Tag.BreedRegistrationSequence,
			Tag.BreedRegistryCodeSequence, Tag.CountryOfResidence,
			Tag.DeidentificationMethod, Tag.DeidentificationMethodCodeSequence,
			Tag.EthnicGroup, Tag.IdentifierTypeCode,
			Tag.InsurancePlanIdentification, Tag.IssuerOfPatientID,
			Tag.IssuerOfPatientIDQualifiersSequence, Tag.LastMenstrualDate,
			Tag.MedicalAlerts, Tag.MedicalRecordLocator, Tag.MilitaryRank,
			Tag.Occupation, Tag.OtherPatientIDs, Tag.OtherPatientIDsSequence,
			Tag.OtherPatientNames, Tag.PatientAddress, Tag.PatientAge,
			Tag.PatientBirthDate, Tag.PatientBirthName, Tag.PatientBirthTime,
			Tag.PatientBreedCodeSequence, Tag.PatientBreedDescription,
			Tag.PatientComments, Tag.PatientID, Tag.PatientIdentityRemoved,
			Tag.PatientInsurancePlanCodeSequence, Tag.PatientMotherBirthName,
			Tag.PatientName, Tag.PatientPrimaryLanguageCodeSequence,
			Tag.PatientPrimaryLanguageModifierCodeSequence,
			Tag.PatientReligiousPreference, Tag.PatientSex,
			Tag.PatientSexNeutered, Tag.PatientSize,
			Tag.PatientSpeciesCodeSequence, Tag.PatientSpeciesDescription,
			Tag.PatientTelephoneNumbers, Tag.PatientWeight,
			Tag.PregnancyStatus, Tag.QualityConrtolSubject,
			Tag.RegionOfResidence, Tag.ResponsibleOrganization,
			Tag.ResponsiblePerson, Tag.ResponsiblePersonRole,
			Tag.SmokingStatus, Tag.TypeOfPatientID, Tag.UniversalEntityID,
			Tag.UniversalEntityIDType };

	private static final Set<Integer> patientTagsSet = new HashSet<Integer>();

	static {
		Arrays.sort(patientTags);
		for (int i = 0; i < patientTags.length; i++) {
			patientTagsSet.add(patientTags[i]);
		}
	}

	public static boolean isPatientTag(int tag) {
		return patientTagsSet.contains(tag);
	}

	public static int[] getPatientTags() {
		return patientTags;
	}
}
