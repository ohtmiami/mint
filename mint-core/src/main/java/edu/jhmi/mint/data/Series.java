package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Iterator;

import org.dcm4che.data.Attributes;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class Series extends InformationEntity implements Iterable<Instance> {

    private static final long serialVersionUID = 2000269125201306307L;

    static final int ESTIMATED_NUM_INSTANCES_PER_SERIES = 16;

    Series(String seriesInstanceUID, Study study) {

        super(seriesInstanceUID, study, ESTIMATED_NUM_INSTANCES_PER_SERIES);
    }

    Series(String seriesInstanceUID, Attributes seriesLevelAttributes,
            Study study, int instanceCount) {

        super(new SeriesDataSet(seriesInstanceUID, seriesLevelAttributes,
                0x00020000L, 0x0002FFFFL, 9, study.fmi(), instanceCount),
                new SeriesDataSet(seriesInstanceUID, seriesLevelAttributes,
                        0x00030000L, 0xFFFFFFFFL, seriesLevelAttributes.size(),
                        study.dataSet(), instanceCount), study, instanceCount);
    }

    @Override
    protected NormalizedDataSet createFMI(String uid, InformationEntity parent) {

        StudyDataSet studyFMI = ((Study) parent).fmi();
        return new SeriesDataSet(uid, studyFMI, 9);
    }

    @Override
    protected NormalizedDataSet createDataSet(String uid,
            InformationEntity parent) {

        StudyDataSet studyDataSet = ((Study) parent).dataSet();
        return new SeriesDataSet(uid, studyDataSet);
    }

    public SeriesDataSet fmi() {

        return (SeriesDataSet) this.fmi;
    }

    public SeriesDataSet dataSet() {

        return (SeriesDataSet) this.dataSet;
    }

    public Study getStudy() {

        return (Study) this.getParent();
    }

    public Instance newInstance(String sopInstanceUID) {

        return new Instance(sopInstanceUID, this);
    }

    public Instance newInstance(String sopInstanceUID,
            Attributes instanceLevelAttributes) {

        return new Instance(sopInstanceUID, instanceLevelAttributes, this);
    }

    public int instanceCount() {

        return this.childCount();
    }

    public Instance getInstance(String sopInstanceUID) {

        return (Instance) this.getChild(sopInstanceUID);
    }

    @Override
    public Iterator<Instance> iterator() {

        return new InstanceIterator();
    }

    private class InstanceIterator implements Iterator<Instance> {

        private final Iterator<InformationEntity> childIterator;

        private InstanceIterator() {

            this.childIterator = Series.this.childIterator();
        }

        @Override
        public boolean hasNext() {

            return childIterator.hasNext();
        }

        @Override
        public Instance next() {

            return (Instance) childIterator.next();
        }

        @Override
        public void remove() {

            throw new UnsupportedOperationException();
        }
    }
}
