package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import org.dcm4che.data.ElementDictionary;
import org.dcm4che.util.TagUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class AttributeAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = 3394105475813556023L;

    public AttributeAlreadyExistsException(int tag) {

        super(String.format("Attribute %s %s already exists",
                TagUtils.toString(tag), ElementDictionary.keywordOf(tag, null)));
    }
}
