package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Iterator;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.util.TagUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class StudyDataSet extends NormalizedDataSet {

    private static final long serialVersionUID = 2800311986262169588L;

    StudyDataSet(String studyInstanceUID, int initialAttributesCapacity) {

        super(studyInstanceUID, initialAttributesCapacity, null,
                Study.ESTIMATED_NUM_SERIES_PER_STUDY);
    }

    StudyDataSet(String studyInstanceUID) {

        this(studyInstanceUID, 128);
    }

    StudyDataSet(String studyInstanceUID, Attributes studyLevelAttributes,
            long minTag, long maxTag, int initialAttributesCapacity,
            int seriesCount) {

        super(studyInstanceUID, studyLevelAttributes, minTag, maxTag,
                initialAttributesCapacity, null, seriesCount);
    }

    @Override
    protected String updateUID(String uid) {

        String oldUID = super.updateUID(uid);
        this.set(Tag.StudyInstanceUID, VR.UI, uid);
        return oldUID;
    }

    public int seriesCount() {

        return this.childCount();
    }

    public SeriesDataSet getSeries(String seriesInstanceUID) {

        return (SeriesDataSet) this.getChild(seriesInstanceUID);
    }

    public Iterator<SeriesDataSet> seriesIterator() {

        return new SeriesIterator();
    }

    protected void demote(int index, InstanceDataSet activeInstance) {

        int tag = this.tagAt(index);
        VR vr = this.vrAt(index);
        Object value = this.valueAt(index);
        this.removeAt(index);

        SeriesDataSet activeSeries = activeInstance == null ? null
                : activeInstance.getSeries();
        for (Iterator<NormalizedDataSet> seriesIterator = this.childIterator(); seriesIterator
                .hasNext();) {
            NormalizedDataSet series = seriesIterator.next();
            if (series != activeSeries) {
                series.insert(tag, vr, value);
            } else {
                for (Iterator<NormalizedDataSet> instanceIterator = series
                        .childIterator(); instanceIterator.hasNext();) {
                    NormalizedDataSet instance = instanceIterator.next();
                    if (instance != activeInstance) {
                        instance.insert(tag, vr, value);
                    }
                }
            }
        }
    }

    @Override
    protected void onAttributeInsertion(int tag, VR vr, Object value) {

        if (tag == Tag.StudyInstanceUID) {
            String uid = value.toString();
            if (!this.getUID().equals(uid)) {
                this.updateUID(uid);
            }
        }
        super.onAttributeInsertion(tag, vr, value);
    }

    @Override
    protected void onAttributeRemoval(int tag, VR vr, Object value) {

        if (tag == Tag.StudyInstanceUID) {
            // Undo removal
            this.set(tag, vr, value);
            throw new UnsupportedOperationException(String.format(
                    "Removal of %s is not allowed", TagUtils.toString(tag),
                    ElementDictionary.keywordOf(tag, null)));
        }
        super.onAttributeRemoval(tag, vr, value);
    }

    private class SeriesIterator implements Iterator<SeriesDataSet> {

        private final Iterator<NormalizedDataSet> childIterator;

        private SeriesIterator() {

            this.childIterator = StudyDataSet.this.childIterator();
        }

        @Override
        public boolean hasNext() {

            return childIterator.hasNext();
        }

        @Override
        public SeriesDataSet next() {

            return (SeriesDataSet) childIterator.next();
        }

        @Override
        public void remove() {

            throw new UnsupportedOperationException();
        }
    }
}
