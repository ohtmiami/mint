package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import org.dcm4che.data.ElementDictionary;
import org.dcm4che.util.TagUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class AttributeOutOfOrderException extends RuntimeException {

    private static final long serialVersionUID = 6737678734263069969L;

    public AttributeOutOfOrderException(int newTag, int insertionIndex,
            int tag, int index) {

        super(String.format("Insertion of attribute %s %s at index %d"
                + " would conflict with existing attribute %s %s at index %d",
                TagUtils.toString(newTag),
                ElementDictionary.keywordOf(newTag, null), insertionIndex,
                TagUtils.toString(tag), ElementDictionary.keywordOf(tag, null),
                index));
    }
}
