package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Iterator;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.util.TagUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class SeriesDataSet extends NormalizedDataSet {

    private static final long serialVersionUID = -8102746461507322309L;

    SeriesDataSet(String seriesInstanceUID, StudyDataSet studyDataSet,
            int initialAttributesCapacity) {

        super(seriesInstanceUID, initialAttributesCapacity, studyDataSet,
                Series.ESTIMATED_NUM_INSTANCES_PER_SERIES);
    }

    SeriesDataSet(String seriesInstanceUID, StudyDataSet studyDataSet) {

        this(seriesInstanceUID, studyDataSet, 16);
    }

    SeriesDataSet(String seriesInstanceUID, Attributes seriesLevelAttributes,
            long minTag, long maxTag, int initialAttributesCapacity,
            StudyDataSet studyDataSet, int instanceCount) {

        super(seriesInstanceUID, seriesLevelAttributes, minTag, maxTag,
                initialAttributesCapacity, studyDataSet, instanceCount);
    }

    @Override
    protected String updateUID(String uid) {

        StudyDataSet studyDataSet = getStudy();
        String oldUID = super.updateUID(uid);
        this.updateValueHierarchical(Tag.SeriesInstanceUID, uid);
        studyDataSet.removeChild(oldUID);
        studyDataSet.addChild(this);
        return oldUID;
    }

    public StudyDataSet getStudy() {

        return (StudyDataSet) this.parent;
    }

    public int instanceCount() {

        return this.childCount();
    }

    public InstanceDataSet getInstance(String sopInstanceUID) {

        return (InstanceDataSet) this.getChild(sopInstanceUID);
    }

    public Iterator<InstanceDataSet> instanceIterator() {

        return new InstanceIterator();
    }

    protected void demote(int index, InstanceDataSet activeInstance) {

        int tag = this.tagAt(index);
        VR vr = this.vrAt(index);
        Object value = this.valueAt(index);
        this.removeAt(index);

        for (Iterator<NormalizedDataSet> instanceIterator = this
                .childIterator(); instanceIterator.hasNext();) {
            NormalizedDataSet instance = instanceIterator.next();
            if (instance != activeInstance) {
                instance.insert(tag, vr, value);
            }
        }
    }

    @Override
    protected void onAttributeInsertion(int tag, VR vr, Object value) {

        if (tag == Tag.SeriesInstanceUID) {
            String uid = value.toString();
            if (!this.getUID().equals(uid)) {
                this.updateUID(uid);
            }
        }
        super.onAttributeInsertion(tag, vr, value);
    }

    @Override
    protected void onAttributeRemoval(int tag, VR vr, Object value) {

        if (tag == Tag.SeriesInstanceUID) {
            // Undo removal
            this.set(tag, vr, value);
            throw new UnsupportedOperationException(String.format(
                    "Removal of %s %s is not allowed", TagUtils.toString(tag),
                    ElementDictionary.keywordOf(tag, null)));
        }
        super.onAttributeRemoval(tag, vr, value);
    }

    @Override
    public Object getValueHierarchical(int tag, VR.Holder vr) {

        return super.getValueHierarchical(tag, vr);
    }

    @Override
    public String getStringHierarchical(int tag) {

        return super.getStringHierarchical(tag);
    }

    private class InstanceIterator implements Iterator<InstanceDataSet> {

        private final Iterator<NormalizedDataSet> childIterator;

        private InstanceIterator() {

            this.childIterator = SeriesDataSet.this.childIterator();
        }

        @Override
        public boolean hasNext() {

            return childIterator.hasNext();
        }

        @Override
        public InstanceDataSet next() {

            return (InstanceDataSet) childIterator.next();
        }

        @Override
        public void remove() {

            throw new UnsupportedOperationException();
        }
    }
}
