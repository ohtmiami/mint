package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

import org.dcm4che.data.VR;
import org.dcm4che.data.Value;
import org.dcm4che.io.DicomEncodingOptions;
import org.dcm4che.io.DicomOutputStream;
import org.dcm4che.util.ByteUtils;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class BulkdataReference implements Serializable, Value {

    private static final long serialVersionUID = 3223796179596455169L;

    private static final int ENCODED_LENGTH = 28;

    private static final byte TRUE_VALUE = 1;

    private static final byte FALSE_VALUE = 0;

    private final VR vr;

    private final boolean isFragments;

    private UUID fileUUID;

    private int valueOffset;

    private int valueLimit;

    public BulkdataReference(VR vr, boolean isFragments, UUID fileUUID,
            int valueOffset, int valueLimit) {

        if (vr == null) {
            throw new NullPointerException("vr");
        }
        if (fileUUID == null) {
            throw new NullPointerException("fileUUID");
        }

        this.vr = vr;
        this.isFragments = isFragments;
        this.fileUUID = fileUUID;
        this.valueOffset = valueOffset;
        this.valueLimit = valueLimit;
    }

    public static BulkdataReference fromBytes(byte[] bytes, int offset,
            int length, boolean bigEndian) {

        if (offset < 0 || length < 0 || offset + length > bytes.length) {
            throw new IllegalArgumentException("Invalid offset or length");
        }
        if (length < ENCODED_LENGTH) {
            throw new IllegalArgumentException(String.format(
                    "Length must be at least %d bytes", ENCODED_LENGTH));
        }

        int pos = offset;
        VR vr = VR.valueOf(ByteUtils.bytesToVR(bytes, pos));
        pos += 2;
        boolean isFragments = bytes[pos] != FALSE_VALUE;
        pos += 2;
        UUID fileUUID = new UUID(ByteUtils.bytesToLong(bytes, pos, bigEndian),
                ByteUtils.bytesToLong(bytes, pos + 8, bigEndian));
        pos += 16;
        int valueOffset = ByteUtils.bytesToInt(bytes, pos, bigEndian);
        pos += 4;
        int valueLimit = ByteUtils.bytesToInt(bytes, pos, bigEndian);
        return new BulkdataReference(vr, isFragments, fileUUID, valueOffset,
                valueLimit);
    }

    public static BulkdataReference fromBytes(byte[] bytes) {

        return fromBytes(bytes, 0, bytes.length, false);
    }

    public final VR getVR() {

        return vr;
    }

    public final boolean isFragments() {

        return isFragments;
    }

    public final UUID getFileUUID() {

        return fileUUID;
    }

    public final void setFileUUID(UUID fileUUID) {

        this.fileUUID = fileUUID;
    }

    public final int getValueOffset() {

        return valueOffset;
    }

    public final void setValueOffset(int valueOffset) {

        this.valueOffset = valueOffset;
    }

    public final int getValueLimit() {

        return valueLimit;
    }

    public final void setValueLimit(int valueLimit) {

        this.valueLimit = valueLimit;
    }

    public int getValueLength() {

        return valueLimit - valueOffset + 1;
    }

    public String getRelativeURI() {

        return String.format("%s?offset=%d&limit=%d", fileUUID, valueOffset,
                valueLimit);
    }

    @Override
    public String toString() {

        return String.format("vr=%s,frag=%s,file=%s,off=%d,lim=%d", vr,
                isFragments, fileUUID, valueOffset, valueLimit);
    }

    @Override
    public boolean isEmpty() {

        return false;
    }

    @Override
    public byte[] toBytes(VR vr, boolean bigEndian) throws IOException {

        byte[] bytes = new byte[ENCODED_LENGTH];
        int offset = 0;
        // VR (2B)
        ByteUtils.shortToBytesBE(this.vr.code(), bytes, offset);
        offset += 2;
        // Fragments Flag (1B)
        bytes[offset++] = isFragments ? TRUE_VALUE : FALSE_VALUE;
        // Reserved (1B)
        bytes[offset++] = 0;
        // File UUID (16B)
        ByteUtils.longToBytes(fileUUID.getMostSignificantBits(), bytes, offset,
                bigEndian);
        offset += 8;
        ByteUtils.longToBytes(fileUUID.getLeastSignificantBits(), bytes,
                offset, bigEndian);
        offset += 8;
        // Value Offset (4B)
        ByteUtils.intToBytes(valueOffset, bytes, offset, bigEndian);
        offset += 4;
        // Value Limit (4B)
        ByteUtils.intToBytes(valueLimit, bytes, offset, bigEndian);
        return bytes;
    }

    @Override
    public void writeTo(DicomOutputStream out, VR vr) throws IOException {

        out.write(toBytes(vr, out.isBigEndian()));
    }

    @Override
    public int calcLength(DicomEncodingOptions encOpts, boolean explicitVR,
            VR vr) {

        return ENCODED_LENGTH;
    }

    @Override
    public int getEncodedLength(DicomEncodingOptions encOpts,
            boolean explicitVR, VR vr) {

        return ENCODED_LENGTH;
    }
}
