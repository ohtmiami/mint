package edu.jhmi.mint.data;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.ArrayList;
import java.util.Iterator;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.util.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class Study extends InformationEntity implements Iterable<Series> {

	private static final Logger LOG = LoggerFactory.getLogger(Study.class);

	private static final long serialVersionUID = 4651012954632474251L;

	static final int ESTIMATED_NUM_SERIES_PER_STUDY = 16;

	public Study(String studyInstanceUID) {

		super(studyInstanceUID, null, ESTIMATED_NUM_SERIES_PER_STUDY);
	}

	public Study(String studyInstanceUID, Attributes studyLevelAttributes,
			int seriesCount) {

		super(new StudyDataSet(studyInstanceUID, studyLevelAttributes,
				0x00020000L, 0x0002FFFFL, 9, seriesCount), new StudyDataSet(
				studyInstanceUID, studyLevelAttributes, 0x00030000L,
				0xFFFFFFFFL, studyLevelAttributes.size(), seriesCount), null,
				seriesCount);
	}

	@Override
	protected NormalizedDataSet createFMI(String uid, InformationEntity parent) {

		return new StudyDataSet(uid, 9);
	}

	@Override
	protected NormalizedDataSet createDataSet(String uid,
			InformationEntity parent) {

		return new StudyDataSet(uid);
	}

	public StudyDataSet fmi() {

		return (StudyDataSet) this.fmi;
	}

	public StudyDataSet dataSet() {

		return (StudyDataSet) this.dataSet;
	}

	public Series newSeries(String seriesInstanceUID) {

		return new Series(seriesInstanceUID, this);
	}

	public Series newSeries(String seriesInstanceUID,
			Attributes seriesLevelAttributes, int instanceCount) {

		return new Series(seriesInstanceUID, seriesLevelAttributes, this,
				instanceCount);
	}

	public int seriesCount() {

		return this.childCount();
	}

	public Series getSeries(String seriesInstanceUID) {

		return (Series) this.getChild(seriesInstanceUID);
	}

	@Override
	public Iterator<Series> iterator() {

		return new SeriesIterator();
	}

	public Instance addSOPInstance(Attributes fmi, Attributes dataSet) {

		String studyInstanceUID = dataSet.getString(Tag.StudyInstanceUID);
		if (studyInstanceUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.StudyInstanceUID),
					ElementDictionary.keywordOf(Tag.StudyInstanceUID, null)));
			return null;
		}
		if (!studyInstanceUID.equals(this.getUID())) {
			LOG.error(String.format("Unexpected %s %s %s" + "\nExpected %s",
					TagUtils.toString(Tag.StudyInstanceUID),
					ElementDictionary.keywordOf(Tag.StudyInstanceUID, null),
					studyInstanceUID, this.getUID()));
			return null;
		}
		String seriesInstanceUID = dataSet.getString(Tag.SeriesInstanceUID);
		if (seriesInstanceUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.SeriesInstanceUID),
					ElementDictionary.keywordOf(Tag.SeriesInstanceUID, null)));
			return null;
		}
		String sopInstanceUID = dataSet.getString(Tag.SOPInstanceUID);
		if (sopInstanceUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.SOPInstanceUID),
					ElementDictionary.keywordOf(Tag.SOPInstanceUID, null)));
			return null;
		}
		String mediaStorageSOPInstanceUID = fmi
				.getString(Tag.MediaStorageSOPInstanceUID);
		if (mediaStorageSOPInstanceUID != null) {
			// Remove (0002,0003) MediaStorageSOPInstanceUID because it is a
			// duplicate of (0008,0018) SOPInstanceUID
			fmi.remove(Tag.MediaStorageSOPInstanceUID);
			if (!mediaStorageSOPInstanceUID.equals(sopInstanceUID)) {
				LOG.warn(String.format("Unexpected %s %s %s" + "\nExpected %s",
						TagUtils.toString(Tag.MediaStorageSOPInstanceUID),
						ElementDictionary.keywordOf(
								Tag.MediaStorageSOPInstanceUID, null),
						mediaStorageSOPInstanceUID, sopInstanceUID));
			}
		}
		String sopClassUID = dataSet.getString(Tag.SOPClassUID);
		if (sopClassUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.SOPClassUID),
					ElementDictionary.keywordOf(Tag.SOPClassUID, null)));
			return null;
		}
		String mediaStorageSOPClassUID = fmi
				.getString(Tag.MediaStorageSOPClassUID);
		if (mediaStorageSOPClassUID != null) {
			// Remove (0002,0002) MediaStorageSOPClassUID because it is a
			// duplicate of (0008,0016) SOPClassUID
			fmi.remove(Tag.MediaStorageSOPClassUID);
			if (!mediaStorageSOPClassUID.equals(sopClassUID)) {
				LOG.warn(String.format("Unexpected %s %s %s" + "\nExpected %s",
						TagUtils.toString(Tag.MediaStorageSOPClassUID),
						ElementDictionary.keywordOf(
								Tag.MediaStorageSOPClassUID, null),
						mediaStorageSOPClassUID, sopClassUID));
			}
		}

		Series series = getSeries(seriesInstanceUID);
		if (series == null) {
			series = newSeries(seriesInstanceUID);
		}
		Instance instance = series.newInstance(sopInstanceUID);
		instance.fmi().addAll(fmi, true);
		instance.fmi().finish();
		instance.dataSet().addAll(dataSet, true);
		instance.dataSet().finish();
		return instance;
	}

	public Instance addSOPInstance(Attributes dataSet) {

		String studyInstanceUID = dataSet.getString(Tag.StudyInstanceUID);
		if (studyInstanceUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.StudyInstanceUID),
					ElementDictionary.keywordOf(Tag.StudyInstanceUID, null)));
			return null;
		}
		if (!studyInstanceUID.equals(this.getUID())) {
			LOG.error(String.format("Unexpected %s %s %s" + "\nExpected %s",
					TagUtils.toString(Tag.StudyInstanceUID),
					ElementDictionary.keywordOf(Tag.StudyInstanceUID, null),
					studyInstanceUID, this.getUID()));
			return null;
		}
		String seriesInstanceUID = dataSet.getString(Tag.SeriesInstanceUID);
		if (seriesInstanceUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.SeriesInstanceUID),
					ElementDictionary.keywordOf(Tag.SeriesInstanceUID, null)));
			return null;
		}
		String sopInstanceUID = dataSet.getString(Tag.SOPInstanceUID);
		if (sopInstanceUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.SOPInstanceUID),
					ElementDictionary.keywordOf(Tag.SOPInstanceUID, null)));
			return null;
		}
	
		String sopClassUID = dataSet.getString(Tag.SOPClassUID);
		if (sopClassUID == null) {
			LOG.error(String.format("Missing %s %s",
					TagUtils.toString(Tag.SOPClassUID),
					ElementDictionary.keywordOf(Tag.SOPClassUID, null)));
			return null;
		}

		Series series = getSeries(seriesInstanceUID);
		if (series == null) {
			series = newSeries(seriesInstanceUID);
		}
		Instance instance = series.newInstance(sopInstanceUID);
		instance.dataSet().addAll(dataSet, true);
		instance.dataSet().finish();
		return instance;
	}	
	
	public Instance addSOPInstance(String transferSyntaxUID, Attributes dataSet) {

		Attributes fmi = new Attributes(1);
		fmi.setString(Tag.TransferSyntaxUID, VR.UI, transferSyntaxUID);
		return addSOPInstance(fmi, dataSet);
	}

	private class SeriesIterator implements Iterator<Series> {

		private final Iterator<InformationEntity> childIterator;

		private SeriesIterator() {

			this.childIterator = Study.this.childIterator();
		}

		@Override
		public boolean hasNext() {

			return childIterator.hasNext();
		}

		@Override
		public Series next() {

			return (Series) childIterator.next();
		}

		@Override
		public void remove() {

			throw new UnsupportedOperationException();
		}
	}

	public void reArrangeDataSet() {
		// push some series level attribute and instance level attribute back to
		// original level
		// check the series level
		if (this.seriesCount() == 1) {
			for (Series series : this) {
				for (int i = 0; i < SeriesTag.getSeriesTags().length; i++) {
					int tag = SeriesTag.getSeriesTags()[i];
					if (this.dataSet().contains(tag)) {
						VR vr = this.dataSet().getVR(tag);
						Object value = tag != 0x0020000E ? this.dataSet().getValue(
								tag) : this.dataSet().getString(tag);
						series.dataSet().insert(tag, vr, value);
						this.dataSet().remove(tag);
					}
				}
			}
		}

		// check the instance level
		ArrayList<Integer> tags = new ArrayList<Integer>();
		ArrayList<VR> vrs = new ArrayList<VR>();
		ArrayList<Object> values = new ArrayList<Object>();
		for (Series series : this) {
			if (series.instanceCount() == 1) {
				for (Instance instance : series) {
					for (int i = 0; i < InstanceTag.getInstanceTags().length; i++) {
						int tag = InstanceTag.getInstanceTags()[i];
						if (this.dataSet().contains(tag)) {
							tags.add(tag);
							VR vr = this.dataSet().getVR(tag);
							vrs.add(vr);
							if(tag == 0x0020000E || tag == 0x00080018){
								values.add(this.dataSet().getString(
										tag));	
							}else{
								values.add(this.dataSet().getValue(
										tag));
							}
							this.dataSet().remove(tag);
						} else if (series.dataSet().contains(tag)) {
							VR vr = series.dataSet().getVR(tag);
							Object value = tag != 0x00080018 ? series.dataSet()
									.getValue(tag) : series.dataSet
									.getString(tag);
							instance.dataSet().insert(tag, vr, value);
							series.dataSet().remove(tag);
						}
					}
				}
			}
		}

		for (Series series : this) {
			if (series.instanceCount() == 1) {
				for (Instance instance : series) {
					for (int i = 0; i < tags.size(); i++) {
						instance.dataSet().insert(tags.get(i), vrs.get(i),
								values.get(i));
					}
				}
			} else {
				for (int i = 0; i < tags.size(); i++) {
					series.dataSet().insert(tags.get(i), vrs.get(i),
							values.get(i));
				}
			}
		}
	}
}
