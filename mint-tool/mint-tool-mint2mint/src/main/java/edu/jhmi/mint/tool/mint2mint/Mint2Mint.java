package edu.jhmi.mint.tool.mint2mint;

/*
 * #%L
 * MINT to MINT Conversion Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.DefaultStudyStore;
import edu.jhmi.mint.io.MintEncodingOptions;
import edu.jhmi.mint.io.MintStudyReader;
import edu.jhmi.mint.io.MintStudyWriter;
import edu.jhmi.mint.io.StudyStore;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class Mint2Mint {

    public static void main(String[] args) throws IOException {

        if (args.length < 2 || args.length > 3) {
            System.err.println("Usage: Mint2Mint [-CE]"
                    + " <input_study_metadata_file>"
                    + "<output_study_metadata_file>");
            System.err.println(" -C: do not compress study metadata");
            System.err.println(" -E: do not encrypt study metadata/bulkdata");
            System.exit(1);
        }

        boolean encrypt = true;
        boolean compress = true;
        if (args.length == 3) {
            String options = args[0];
            encrypt = options.indexOf('E') == -1;
            compress = options.indexOf('C') == -1;
        }
        Path metadataFilePath = Paths.get(args[args.length - 2]);
        Path outputMetadataFilePath = Paths.get(args[args.length - 1]);
        StudyStore studyStore = new DefaultStudyStore(metadataFilePath
                .getParent().getParent(),true);
        Study study = null;
        try (InputStream in = Files.newInputStream(metadataFilePath)) {
            study = MintStudyReader.readMetadata(in);
        }
        if (study != null) {
            if(encrypt)
                studyStore.reorganizeBulkdata(study, "AES-GCM-256", false);
            else
                studyStore.reorganizeBulkdata(study, null, false);
            MintEncodingOptions encodingOptions = new MintEncodingOptions(
                    encrypt, compress);
            try (BufferedOutputStream bufferedOut = new BufferedOutputStream(
                    Files.newOutputStream(outputMetadataFilePath))) {
                MintStudyWriter.writeMetadata(bufferedOut, study,
                        encodingOptions);
            }
        }
    }
}
