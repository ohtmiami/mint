package edu.jhmi.mint.tool.mint2json;

/*
 * #%L
 * MINT to JSON Conversion Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Tag;

import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.MintStudyReader;
import edu.jhmi.mint.io.MintJsonWriter;

import com.fasterxml.jackson.core.JsonGenerationException;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class Mint2Json {

	public static void main(String[] args) throws IOException,
			JsonGenerationException {

		if (args.length < 1 || (args.length > 4 && args.length != 7)) {
			printUsageInfo();
		}

		String outputType = args[0];
		String indexType = args[1];
		Path metadataFilePath = Paths.get(args[2]);
		String outputDir = "";

		if (outputType.indexOf("D") >= 0) {
			if (args.length != 3) {
				printUsageInfo();
			}
		} else if (outputType.indexOf("F") >= 0) {
			if (args.length == 4 || args.length == 7) {
				outputDir = args[3];
			} else {
				printUsageInfo();
			}
		} else {
			printUsageInfo();
		}

		MintJsonWriter.setPrintStyle(outputType.indexOf("P") >= 0);
		MintJsonWriter.setSkipInvalidValue(outputType.indexOf("S") >= 0);

		String headerName = null;
		String headerContent = null;
		if (args.length >= 5) {
			if (args[4].indexOf("-A") >= 0) {
				headerName = args[5];
				try (Scanner scanner = new Scanner(new File(args[6]))) {
					headerContent = scanner.useDelimiter("\\Z").next();
				}
			}
		}
		Study study = null;
		HashMap<String, Attributes> privateDataSets = new HashMap<String, Attributes>();

		try (InputStream in = Files.newInputStream(metadataFilePath)) {
			study = MintStudyReader.readMetadata(in);
			study.reArrangeDataSet();
			privateDataSets = MintJsonWriter.extractPrivateTag(study);
		}
		if (study != null) {
			if (outputType.indexOf("D") >= 0) {
				if ("-patientstudies".equals(indexType)) {
					MintJsonWriter.writePatientStudiesIndex(System.out, study,
							privateDataSets, headerName, headerContent);
				} else if ("-patientstudiesnoinstance".equals(indexType)) {
					MintJsonWriter.writePatientStudiesIndexWithoutInstance(
							System.out, study, privateDataSets, headerName,
							headerContent);
				} else if ("-patientstudiesoneinstance".equals(indexType)) {
					MintJsonWriter.writePatientStudiesIndexWithOneInstance(
							System.out, study, privateDataSets, headerName,
							headerContent);
				} else if ("-patient".equals(indexType)) {
					MintJsonWriter.writePatientIndex(System.out, study,
							headerName, headerContent);
				} else if ("-study".equals(indexType)) {
					MintJsonWriter.writeStudyIndex(System.out, study,
							headerName, headerContent, false);
				} else if ("-studywithpatient".equals(indexType)) {
					MintJsonWriter.writeStudyIndex(System.out, study,
							headerName, headerContent, true);
				} else if ("-series".equals(indexType)) {
					for (Series series : study) {
						MintJsonWriter.writeSeriesIndex(System.out, series,
								headerName, headerContent);
					}
				} else if ("-instance".equals(indexType)) {
					for (Series series : study) {
						for (Instance instance : series) {
							String instanceUID = instance.getUID();
							Attributes privateDataSet = new Attributes();
							if (privateDataSets.containsKey(instanceUID)) {
								privateDataSet = privateDataSets
										.get(instanceUID);
							}
							MintJsonWriter.writeInstanceIndex(System.out,
									instance, privateDataSet, headerName,
									headerContent);
						}
					}
				} else {
					printUsageInfo();
				}
			} else if (outputType.indexOf("F") >= 0) {
				Path outputDirPath = Paths.get(outputDir);
				if (!Files.exists(outputDirPath)) {
					Files.createDirectories(outputDirPath);
				}
				String patientID = MintJsonWriter.encodeReservedCharacter(study
						.dataSet().getString(Tag.PatientID));
				if ("-patientstudies".equals(indexType)) {
					Path outputFilePath = outputDirPath.resolve(patientID
							+ ".json");
					try (OutputStream out = Files
							.newOutputStream(outputFilePath)) {
						MintJsonWriter.writePatientStudiesIndex(out, study,
								privateDataSets, headerName, headerContent);
					}
				} else if ("-patientstudiesnoinstance".equals(indexType)) {
					Path outputFilePath = outputDirPath.resolve(patientID
							+ ".json");
					try (OutputStream out = Files
							.newOutputStream(outputFilePath)) {
						MintJsonWriter.writePatientStudiesIndexWithoutInstance(
								out, study, privateDataSets, headerName,
								headerContent);
					}
				} else if ("-patientstudiesoneinstance".equals(indexType)) {
					Path outputFilePath = outputDirPath.resolve(patientID
							+ ".json");
					try (OutputStream out = Files
							.newOutputStream(outputFilePath)) {
						MintJsonWriter.writePatientStudiesIndexWithOneInstance(
								out, study, privateDataSets, headerName,
								headerContent);
					}
				} else if ("-patient".equals(indexType)) {
					Path outputFilePath = outputDirPath.resolve(patientID
							+ ".json");
					try (OutputStream out = Files
							.newOutputStream(outputFilePath)) {
						MintJsonWriter.writePatientIndex(out, study,
								headerName, headerContent);
					}
				} else if ("-study".equals(indexType)) {
					Path outputFilePath = outputDirPath.resolve(study.getUID()
							+ ".json");
					try (OutputStream out = Files
							.newOutputStream(outputFilePath)) {
						MintJsonWriter.writeStudyIndex(out, study, headerName,
								headerContent, false);
					}
				} else if ("-studywithpatient".equals(indexType)) {
					Path outputFilePath = outputDirPath.resolve(study.getUID()
							+ ".json");
					try (OutputStream out = Files
							.newOutputStream(outputFilePath)) {
						MintJsonWriter.writeStudyIndex(out, study, headerName,
								headerContent, true);
					}
				} else if ("-series".equals(indexType)) {
					for (Series series : study) {
						Path outputFilePath = outputDirPath.resolve(series
								.getUID() + ".json");
						try (OutputStream out = Files
								.newOutputStream(outputFilePath)) {
							MintJsonWriter.writeSeriesIndex(out, series,
									headerName, headerContent);
						}
					}
				} else if ("-instance".equals(indexType)) {
					for (Series series : study) {
						for (Instance instance : series) {
							Path outputFilePath = outputDirPath
									.resolve(instance.getUID() + ".json");
							try (OutputStream out = Files
									.newOutputStream(outputFilePath)) {
								String instanceUID = instance.getUID();
								Attributes privateDataSet = new Attributes();
								if (privateDataSets.containsKey(instanceUID)) {
									privateDataSet = privateDataSets
											.get(instanceUID);
								}
								MintJsonWriter.writeInstanceIndex(out,
										instance, privateDataSet, headerName,
										headerContent);
							}
						}
					}
				} else {
					printUsageInfo();
				}
			} else {
				printUsageInfo();
			}
		}
	}

	public static void printUsageInfo() {

		System.err.println("Usage: Mint2Json [-FPS|-DPS]"
				+ " [-patientstudies|-patient|-study|-series|-instance]"
				+ " <study_metadata_file> <output_dir>" + " [-A]"
				+ " <header_name>" + " <header_json_file>");
		System.err.println(" -D : Dump in JSON format");
		System.err.println(" -P : Print pretty");
		System.err.println(" -F : Save as JSON file");
		System.err.println(" -S : Skip the Invalid Value");
		System.err.println(" -A : Add header");
		System.err.println(" -patientstudies: To Patient Studies Index");
		System.err.println(" -patient     : To Patient Index");
		System.err.println(" -study       : To Study Indx");
		System.err.println(" -series      : To Series Index");
		System.err.println(" -instance    : To Instance Index");
		System.exit(1);
	}
}
