package edu.jhmi.mint.tool.mintdump;

/*
 * #%L
 * MINT Dump Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.dcm4che.io.DicomInputStream;
import org.dcm4che.tool.dcmdump.DcmDump;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.MintStudyReader;
import edu.jhmi.mint.io.MultiSeriesDicomWriter;
import edu.jhmi.mint.io.StudyPrinter;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MintDump {

    public static void main(String[] args) throws IOException {

        if (args.length < 1 || args.length > 2) {
            System.err.println("Usage: MintDump [-D] <study_metadata_file>");
            System.err.println(" -D: use the dcmdump output format");
            System.exit(1);
        }

        boolean useDcmDump = false;
        if (args.length == 2) {
            useDcmDump = "-D".equals(args[0]);
        }
        Path metadataFilePath = Paths.get(args[args.length - 1]);
        Study study = null;
        try (InputStream in = Files.newInputStream(metadataFilePath)) {
            study = MintStudyReader.readMetadata(in);
        }
        if (study != null) {
            if (useDcmDump) {
                dumpStudy(study);
            } else {
                StudyPrinter studyPrinter = new StudyPrinter();
                studyPrinter.printStudy(study, System.out);
            }
        }
    }

    private static void dumpStudy(final Study study) throws IOException {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        try (PipedOutputStream pipedOut = new PipedOutputStream()) {
            try (PipedInputStream pipedIn = new PipedInputStream(pipedOut)) {
                executor.submit(new Callable<Void>() {

                    @Override
                    public Void call() throws Exception {

                        MultiSeriesDicomWriter.writeStudy(pipedOut, study,
                                false, true);
                        pipedOut.flush();
                        pipedOut.close();
                        return null;
                    }
                });
                try (DicomInputStream dicomIn = new DicomInputStream(pipedIn)) {
                    DcmDump dcmDump = new DcmDump();
                    dcmDump.setWidth(128);
                    dcmDump.setIncludeTagPosition(false);
                    dcmDump.parse(dicomIn);
                }
            }
        } finally {
            executor.shutdown();
        }
    }
}
