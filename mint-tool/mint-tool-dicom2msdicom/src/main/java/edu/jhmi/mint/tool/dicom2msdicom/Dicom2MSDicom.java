package edu.jhmi.mint.tool.dicom2msdicom;

/*
 * #%L
 * DICOM to MSDICOM Conversion Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.DefaultStudyStore;
import edu.jhmi.mint.io.DicomInputSource;
import edu.jhmi.mint.io.DicomStudyReader;
import edu.jhmi.mint.io.MultiSeriesDicomWriter;
import edu.jhmi.mint.io.StudyStore;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class Dicom2MSDicom {

    public static void main(String[] args) throws IOException {

        if (args.length < 2 || args.length > 3) {
            System.err.println("Usage: Dicom2MSDicom [-X]"
                    + " <study_input_dir> <output_dir>");
            System.err.println(" -X: only read .dcm files");
            System.exit(1);
        }

        boolean dcmFilesOnly = args.length == 3 && "-X".equals(args[0]);
        Path studyDirPath = Paths.get(args[args.length - 2]);
        Path outputDirPath = Paths.get(args[args.length - 1]);
        StudyStore studyStore = new DefaultStudyStore(outputDirPath,true);
        DicomStudyReader studyReader = new DicomStudyReader(studyStore);
        Study study = null;
        try (DicomInputSource inputSource = DicomInputSource.fromFolder(
                studyDirPath,
                dcmFilesOnly ? DicomInputSource.DCM_EXTENSION_PATTERN
                        : DicomInputSource.ANY_EXTENSION_PATTERN)) {
            study = studyReader.readStudy(inputSource);
        }
        if (study != null) {
            String studyInstanceUID = study.getUID();
            Path studyFilePath = outputDirPath.resolve(String.format(
                    "%s.msdicom", studyInstanceUID));
            try (BufferedOutputStream bufferedOut = new BufferedOutputStream(
                    Files.newOutputStream(studyFilePath))) {
                MultiSeriesDicomWriter.writeStudy(bufferedOut, study);
            }
        }
    }
}
