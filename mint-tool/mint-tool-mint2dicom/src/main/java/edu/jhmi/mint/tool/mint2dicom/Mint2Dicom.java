package edu.jhmi.mint.tool.mint2dicom;

/*
 * #%L
 * MINT to DICOM Conversion Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.DefaultStudyStore;
import edu.jhmi.mint.io.DicomOutputSink;
import edu.jhmi.mint.io.DicomStudyWriter;
import edu.jhmi.mint.io.MintStudyReader;
import edu.jhmi.mint.io.StudyStore;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class Mint2Dicom {

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.err.println("Usage: Mint2Dicom"
                    + " <study_metadata_file> <output_dir>");
            System.exit(1);
        }

        Path metadataFilePath = Paths.get(args[0]);
        Path outputDirPath = Paths.get(args[1]);
        StudyStore studyStore = new DefaultStudyStore(metadataFilePath
                .getParent().getParent(),true);
        Study study = null;
        try (InputStream in = Files.newInputStream(metadataFilePath)) {
            study = MintStudyReader.readMetadata(in);
        }
        if (study != null) {
            DicomStudyWriter studyWriter = new DicomStudyWriter(studyStore);
            DicomOutputSink outputSink = DicomOutputSink.fromFolder(
                    outputDirPath, false);
            studyWriter.writeStudy(study, outputSink);
        }
    }
}
