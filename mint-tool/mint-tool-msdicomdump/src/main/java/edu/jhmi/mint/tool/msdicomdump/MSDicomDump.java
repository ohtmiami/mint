package edu.jhmi.mint.tool.msdicomdump;

/*
 * #%L
 * MSDICOM Dump Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.MultiSeriesDicomReader;
import edu.jhmi.mint.io.StudyPrinter;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * 
 */
public class MSDicomDump {

    public static void main(String[] args) throws IOException {

        if (args.length < 1) {
            System.err.println("Usage: MSDicomDump <study_metadata_file>");
            System.exit(1);
        }

        Path studyFilePath = Paths.get(args[0]);
        Study study = null;
        try (InputStream in = Files.newInputStream(studyFilePath)) {
            MultiSeriesDicomReader msdicomReader = new MultiSeriesDicomReader();
            study = msdicomReader.readStudy(in);
        }

        StudyPrinter studyPrinter = new StudyPrinter();
        studyPrinter.printStudy(study, System.out);
    }
}
