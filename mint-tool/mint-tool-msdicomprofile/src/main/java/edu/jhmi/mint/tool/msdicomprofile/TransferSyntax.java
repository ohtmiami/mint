package edu.jhmi.mint.tool.msdicomprofile;

/*
 * #%L
 * MSDICOM Profile Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import org.dcm4che.data.UID;

public class TransferSyntax {

    private static final String COMMON_UID_PREFIX = "1.2.840.10008.1.2.";

    private static final Map<String, String> uidNameMap = new HashMap<String, String>();

    static {

        uidNameMap.put(UID.ImplicitVRLittleEndian, "IVRLE");
        uidNameMap.put(UID.ExplicitVRLittleEndian, "EVRLE");
        uidNameMap.put(UID.ExplicitVRBigEndian, "EVRBE");
        uidNameMap.put(UID.RLELossless, "RLE_Lossless");
        uidNameMap.put(UID.JPEGBaseline1, "JPEG_Baseline1");
        uidNameMap.put(UID.JPEGExtended24, "JPEG_Extended24");
        uidNameMap.put(UID.JPEGLosslessNonHierarchical14, "JPEG_LosslessNH14");
        uidNameMap.put(UID.JPEGLossless, "JPEG_Lossless");
        uidNameMap.put(UID.JPEGLSLossless, "JPEG-LS_Lossless");
        uidNameMap.put(UID.JPEGLSLossyNearLossless, "JPEG-LS_Lossy");
        uidNameMap.put(UID.JPEG2000LosslessOnly, "JPEG2000_Lossless");
        uidNameMap.put(UID.JPEG2000, "JPEG2000");
    }

    public static String nameOf(String uid) {

        String name = uidNameMap.get(uid);
        if (name == null) {
            if (uid.startsWith(COMMON_UID_PREFIX)) {
                name = uid.substring(COMMON_UID_PREFIX.length());
            } else {
                name = uid;
            }
        }
        return name;
    }
}
