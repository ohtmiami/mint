package edu.jhmi.mint.tool.msdicomprofile;

/*
 * #%L
 * MSDICOM Profile Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.dcm4che.data.VR;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.MultiSeriesDicomReader;

public class MSDicomProfile {

    private static final String USAGE = "MSDicomProfile [OPTIONS]"
            + " <study_metadata_file>";

    private static final String DESCRIPTION = "Reads a MSDICOM study"
            + " and outputs useful statistics\nOPTIONS:";

    private static final String EXAMPLE = "\ne.g. MSDicomProfile"
            + " /path/to/metadata.msdicom";

    private static final String OPT_HEADER_ONLY = "H";

    private static final String OPT_VRS_ONLY = "V";

    private static final String OPT_HELP = "h";

    private static final String LONG_OPT_HELP = "help";

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static CommandLine parse(String[] args) {

        Options options = new Options();
        options.addOption(OPT_HEADER_ONLY, false,
                "only output the table header");
        options.addOption(OPT_VRS_ONLY, false,
                "only output the statistics of all VRs");
        options.addOption(OPT_HELP, LONG_OPT_HELP, false, "print this message");

        CommandLine commandLine = null;
        try {
            commandLine = new PosixParser().parse(options, args);
        } catch (ParseException e) {
            System.err.println("Invalid command-line arguments: "
                    + e.getMessage());
            System.err.println("Try 'MSDicomProfile -h' for usage");
            System.exit(1);
        }
        if (commandLine.hasOption(OPT_HELP)
                || !commandLine.hasOption(OPT_HEADER_ONLY)
                && commandLine.getArgs().length != 1) {
            new HelpFormatter().printHelp(USAGE, DESCRIPTION, options, EXAMPLE);
            System.exit(0);
        }
        return commandLine;
    }

    public static void main(String[] args) throws IOException {

        CommandLine commandLine = parse(args);
        if (commandLine.hasOption(OPT_HEADER_ONLY)) {
            System.out.print("Study IUID");
            if (commandLine.hasOption(OPT_VRS_ONLY)) {
                for (VR vr : VR.values()) {
                    System.out.printf("\t#%s", vr);
                }
                for (int i = 0; i <= StudyProfile.SEQUENCE_SIZE_THRESHOLD; i++) {
                    System.out.printf("\t#Size%dSQ", i);
                }
                System.out.printf("\t#Size>%dSQ",
                        StudyProfile.SEQUENCE_SIZE_THRESHOLD);
            } else {
                System.out.print("\tDate Time");
                System.out.print("\tAcq Mods");
                System.out.print("\tDoc Mods");
                System.out.print("\tOther Mods");
                System.out.print("\tPrimary Acq TS");
                System.out.print("\tDoc TSs");
                System.out.print("\tOther TSs");
                System.out.print("\tMods w/ Mixed TSs");
                System.out.print("\t#Series");
                System.out.print("\t#Instances");
                System.out.print("\t#MF Instances");
                System.out.print("\tBd Size");
                System.out.print("\tPriv Bd Size");
                System.out.print("\tNorm Md Size");
                System.out.print("\t#Norm Attrs");
                System.out.print("\t#Bd Attrs");
                System.out.print("\t#Std Bd Attrs");
                System.out.print("\t#Priv Bd Attrs");
            }
            System.out.println();
            return;
        }

        @SuppressWarnings("unchecked")
        List<String> argList = commandLine.getArgList();
        Path studyFilePath = Paths.get(argList.get(0));
        Study study = null;
        try (InputStream in = Files.newInputStream(studyFilePath)) {
            MultiSeriesDicomReader msdicomReader = new MultiSeriesDicomReader();
            study = msdicomReader.readStudy(in);
        }

        StudyProfile studyProfile = new StudyProfile(study);
        System.out.print(studyProfile.getStudyInstanceUID());
        if (commandLine.hasOption(OPT_VRS_ONLY)) {
            for (int vrFrequency : studyProfile.getVRFrequencies()) {
                System.out.printf("\t%d", vrFrequency);
            }
            for (int sequenceSizeFrequency : studyProfile
                    .getSequenceSizeFrequencies()) {
                System.out.printf("\t%d", sequenceSizeFrequency);
            }
        } else {
            System.out.printf("\t%s", new SimpleDateFormat(DATE_FORMAT)
                    .format(studyProfile.getStudyDateTime()));
            System.out.printf("\t%s", studyProfile.getAcquisitionModalities()
                    .getAllAsString());
            System.out.printf("\t%s", studyProfile.getDocumentModalities()
                    .getAllAsString());
            System.out.printf("\t%s", studyProfile.getOtherModalities()
                    .getAllAsString());
            System.out.printf("\t%s",
                    studyProfile.getPrimaryAcquisitionTransferSyntax());
            System.out.printf("\t%s", studyProfile
                    .getDocumentTransferSyntaxes().getAllAsString());
            System.out.printf("\t%s", studyProfile.getOtherTransferSyntaxes()
                    .getAllAsString());
            System.out.printf("\t%s", studyProfile
                    .getModalitiesWithMixedTransferSyntaxes().getAllAsString());
            System.out.printf("\t%d", studyProfile.getNumSeries());
            System.out.printf("\t%d", studyProfile.getNumInstances());
            System.out.printf("\t%d", studyProfile.getNumMultiFrameInstances());
            System.out.printf("\t%d", studyProfile.getBulkdataSize());
            System.out.printf("\t%d", studyProfile.getPrivateBulkdataSize());
            System.out.printf("\t%d", Files.size(studyFilePath));
            System.out
                    .printf("\t%d", studyProfile.getNumNormalizedAttributes());
            System.out.printf("\t%d", studyProfile.getNumBulkdataAttributes());
            System.out.printf("\t%s",
                    studyProfile.getStandardBulkdataAttributes());
            System.out.printf("\t%s",
                    studyProfile.getPrivateBulkdataAttributes());
        }
        System.out.println();
    }
}
