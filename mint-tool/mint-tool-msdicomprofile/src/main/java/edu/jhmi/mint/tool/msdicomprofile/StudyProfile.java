package edu.jhmi.mint.tool.msdicomprofile;

/*
 * #%L
 * MSDICOM Profile Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.ElementDictionary;
import org.dcm4che.data.Fragments;
import org.dcm4che.data.Sequence;
import org.dcm4che.data.Tag;
import org.dcm4che.data.VR;
import org.dcm4che.util.TagUtils;

import edu.jhmi.mint.data.BulkdataReference;
import edu.jhmi.mint.data.Instance;
import edu.jhmi.mint.data.Series;
import edu.jhmi.mint.data.Study;

public class StudyProfile {

    public static final int SEQUENCE_SIZE_THRESHOLD = 8;

    private final Study study;

    private final Map<String, EnumValueProfile> acquisitionModalityFrequencies;

    private final Map<String, EnumValueProfile> documentModalityFrequencies;

    private final Map<String, EnumValueProfile> otherModalityFrequencies;

    private final Map<String, EnumValueProfile> acquisitionTransferSyntaxFrequencies;

    private final Map<String, EnumValueProfile> documentTransferSyntaxFrequencies;

    private final Map<String, EnumValueProfile> otherTransferSyntaxFrequencies;

    private final Map<String, EnumValueProfile> modalitiesWithMixedTransferSyntaxes;

    private final Set<String> standardBulkdataAttributes;

    private final Set<String> privateBulkdataAttributes;

    private final int[] vrFrequencies;

    private final int[] sequenceSizeFrequencies;

    private int numInstances;

    private int numMultiFrameInstances;

    private int numNormalizedAttributes;

    private int numBulkdataAttributes;

    private long bulkdataSize;

    private long privateBulkdataSize;

    public StudyProfile(Study study) {

        this.study = study;
        acquisitionModalityFrequencies = new HashMap<String, EnumValueProfile>();
        documentModalityFrequencies = new HashMap<String, EnumValueProfile>();
        otherModalityFrequencies = new HashMap<String, EnumValueProfile>();
        acquisitionTransferSyntaxFrequencies = new HashMap<String, EnumValueProfile>();
        documentTransferSyntaxFrequencies = new HashMap<String, EnumValueProfile>();
        otherTransferSyntaxFrequencies = new HashMap<String, EnumValueProfile>();
        modalitiesWithMixedTransferSyntaxes = new HashMap<String, EnumValueProfile>();
        standardBulkdataAttributes = new HashSet<String>();
        privateBulkdataAttributes = new HashSet<String>();
        vrFrequencies = new int[VR.values().length];
        sequenceSizeFrequencies = new int[SEQUENCE_SIZE_THRESHOLD + 2];
        profile();
    }

    private void clear() {

        acquisitionModalityFrequencies.clear();
        documentModalityFrequencies.clear();
        otherModalityFrequencies.clear();
        acquisitionTransferSyntaxFrequencies.clear();
        documentTransferSyntaxFrequencies.clear();
        otherTransferSyntaxFrequencies.clear();
        modalitiesWithMixedTransferSyntaxes.clear();
        standardBulkdataAttributes.clear();
        privateBulkdataAttributes.clear();
        for (int i = 0; i < vrFrequencies.length; i++) {
            vrFrequencies[i] = 0;
        }
        for (int i = 0; i < sequenceSizeFrequencies.length; i++) {
            sequenceSizeFrequencies[i] = 0;
        }
        numInstances = 0;
        numMultiFrameInstances = 0;
        numBulkdataAttributes = 0;
        bulkdataSize = 0;
        privateBulkdataSize = 0;
    }

    public final String getStudyInstanceUID() {

        return study.getUID();
    }

    public final Date getStudyDateTime() {

        return study.dataSet().getDate(Tag.StudyDateAndTime);
    }

    public final int getNumSeries() {

        return study.seriesCount();
    }

    public final int getNumInstances() {

        return numInstances;
    }

    public final int getNumMultiFrameInstances() {

        return numMultiFrameInstances;
    }

    public SortedEnumValues getAcquisitionModalities() {

        SortedSet<EnumValueProfile> sortedAcquisitionModalities = new TreeSet<EnumValueProfile>(
                new EnumValueProfile.NameComparator());
        sortedAcquisitionModalities.addAll(acquisitionModalityFrequencies
                .values());
        return new SortedEnumValues(sortedAcquisitionModalities, '/');
    }

    public SortedEnumValues getDocumentModalities() {

        return new SortedEnumValues(new TreeSet<EnumValueProfile>(
                documentModalityFrequencies.values()), '\\');
    }

    public SortedEnumValues getOtherModalities() {

        return new SortedEnumValues(new TreeSet<EnumValueProfile>(
                otherModalityFrequencies.values()), '\\');
    }

    public String getPrimaryAcquisitionTransferSyntax() {

        SortedEnumValues sortedAcquisitionTransferSyntaxes = new SortedEnumValues(
                new TreeSet<EnumValueProfile>(
                        acquisitionTransferSyntaxFrequencies.values()), '\\');
        return sortedAcquisitionTransferSyntaxes.getPrimary();
    }

    public SortedEnumValues getDocumentTransferSyntaxes() {

        return new SortedEnumValues(new TreeSet<EnumValueProfile>(
                documentTransferSyntaxFrequencies.values()), '\\');
    }

    public SortedEnumValues getOtherTransferSyntaxes() {

        SortedSet<EnumValueProfile> sortedAcquisitionTransferSyntaxes = new TreeSet<EnumValueProfile>(
                acquisitionTransferSyntaxFrequencies.values());
        SortedSet<EnumValueProfile> sortedOtherTransferSyntaxes = new TreeSet<EnumValueProfile>(
                otherTransferSyntaxFrequencies.values());
        if (!sortedAcquisitionTransferSyntaxes.isEmpty()) {
            sortedAcquisitionTransferSyntaxes
                    .remove(sortedAcquisitionTransferSyntaxes.first());
            sortedOtherTransferSyntaxes
                    .addAll(sortedAcquisitionTransferSyntaxes);
        }
        return new SortedEnumValues(sortedOtherTransferSyntaxes, '\\');
    }

    public SortedEnumValues getModalitiesWithMixedTransferSyntaxes() {

        return new SortedEnumValues(new TreeSet<EnumValueProfile>(
                modalitiesWithMixedTransferSyntaxes.values()), '\\');
    }

    public String getStandardBulkdataAttributes() {

        return join(standardBulkdataAttributes.iterator(), '\\');
    }

    public String getPrivateBulkdataAttributes() {

        return join(privateBulkdataAttributes.iterator(), '\\');
    }

    public int[] getVRFrequencies() {

        return Arrays.copyOf(vrFrequencies, vrFrequencies.length);
    }

    public int[] getSequenceSizeFrequencies() {

        return Arrays.copyOf(sequenceSizeFrequencies,
                sequenceSizeFrequencies.length);
    }

    public final int getNumNormalizedAttributes() {

        return numNormalizedAttributes;
    }

    public final int getNumBulkdataAttributes() {

        return numBulkdataAttributes;
    }

    public final long getBulkdataSize() {

        return bulkdataSize;
    }

    public final long getPrivateBulkdataSize() {

        return privateBulkdataSize;
    }

    private void processDataSet(Attributes dataSet, int level) {

        for (int i = 0; i < dataSet.size(); i++) {
            ++numNormalizedAttributes;
            int tag = dataSet.tagAt(i);
            VR vr = dataSet.vrAt(i);
            Object value = dataSet.valueAt(i);
            if (!TagUtils.isFileMetaInformation(tag)) {
                ++vrFrequencies[vr.ordinal()];
            }
            if (value instanceof Sequence) {
                Sequence sequence = (Sequence) value;
                int sequenceSize = sequence.size();
                if (sequenceSize > SEQUENCE_SIZE_THRESHOLD) {
                    sequenceSize = SEQUENCE_SIZE_THRESHOLD + 1;
                }
                ++sequenceSizeFrequencies[sequenceSize];
                for (Attributes item : sequence) {
                    // For Item
                    ++numNormalizedAttributes;
                    processDataSet(item, level + 1);
                }
            } else if (value instanceof Fragments) {
                Fragments fragments = (Fragments) value;
                for (@SuppressWarnings("unused")
                Object fragment : fragments) {
                    // For Item
                    ++numNormalizedAttributes;
                }
            } else if (value instanceof BulkdataReference) {
                BulkdataReference bdr = (BulkdataReference) value;
                ++numBulkdataAttributes;
                bulkdataSize += bdr.getValueLength();
                if (TagUtils.isPrivateTag(tag)) {
                    privateBulkdataSize += bdr.getValueLength();
                    privateBulkdataAttributes.add(getAttributeName(
                            dataSet.getPrivateCreator(tag), tag, level));
                } else {
                    standardBulkdataAttributes.add(getAttributeName(null, tag,
                            level));
                }
            } else {
                // Do nothing
            }
        }
    }

    public void profile() {

        clear();

        // For FileMetaInformationGroupLength through TransferSyntaxUID
        numNormalizedAttributes += 5;
        // For PatientStudiesSequence, Item and Item
        numNormalizedAttributes += 3;
        processDataSet(study.fmi(), 0);
        processDataSet(study.dataSet(), 0);
        // For PerSeriesFunctionalGroupsSequence
        ++numNormalizedAttributes;

        for (Series series : study) {
            // For Item
            ++numNormalizedAttributes;
            processDataSet(series.fmi(), 0);
            processDataSet(series.dataSet(), 0);
            // For PerInstanceFunctionalGroupsSequence
            ++numNormalizedAttributes;

            String modality = series.dataSet().getStringHierarchical(
                    Tag.Modality);
            if (modality == null) {
                throw new RuntimeException(String.format(
                        "Missing %s %s from %s/%s",
                        TagUtils.toString(Tag.Modality),
                        ElementDictionary.keywordOf(Tag.Modality, null),
                        study.getUID(), series.getUID()));
            }

            Modality.Type modalityType = Modality.typeOf(modality);
            switch (modalityType) {
            case ACQUISITION:
                updateFrequency(modality, acquisitionModalityFrequencies);
                break;

            case DOCUMENT:
                updateFrequency(modality, documentModalityFrequencies);
                break;

            default:
                updateFrequency(modality, otherModalityFrequencies);
                break;
            }

            if (series.fmi().getStringHierarchical(Tag.TransferSyntaxUID) == null) {
                updateFrequency(modality, modalitiesWithMixedTransferSyntaxes);
            }

            for (Instance instance : series) {
                // For Item
                ++numNormalizedAttributes;
                processDataSet(instance.fmi(), 0);
                processDataSet(instance.dataSet(), 0);

                ++numInstances;
                if (instance.frameCount() >= 0) {
                    ++numMultiFrameInstances;
                }

                String transferSyntaxUID = instance.fmi()
                        .getStringHierarchical(Tag.TransferSyntaxUID);
                if (transferSyntaxUID == null) {
                    throw new RuntimeException(String.format(
                            "Missing %s %s from %s/%s/%s", TagUtils
                                    .toString(Tag.TransferSyntaxUID),
                            ElementDictionary.keywordOf(Tag.TransferSyntaxUID,
                                    null), study.getUID(), series.getUID(),
                            instance.getUID()));
                }
                String transferSyntax = TransferSyntax
                        .nameOf(transferSyntaxUID);

                switch (modalityType) {
                case ACQUISITION:
                    updateFrequency(transferSyntax,
                            acquisitionTransferSyntaxFrequencies);
                    break;

                case DOCUMENT:
                    updateFrequency(transferSyntax,
                            documentTransferSyntaxFrequencies);
                    break;

                default:
                    updateFrequency(transferSyntax,
                            otherTransferSyntaxFrequencies);
                    break;
                }
            }
        }
    }

    private static boolean updateFrequency(String enumValue,
            Map<String, EnumValueProfile> frequencies) {

        EnumValueProfile frequency = frequencies.get(enumValue);
        boolean create = frequency == null;
        if (create) {
            frequency = new EnumValueProfile(enumValue);
            frequencies.put(enumValue, frequency);
        }
        frequency.increaseCount();
        return create;
    }

    static String join(Iterator<String> iter, char separator) {

        if (!iter.hasNext()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(iter.next());
        while (iter.hasNext()) {
            sb.append(separator).append(iter.next());
        }
        return sb.toString();
    }

    private static String getAttributeName(String privateCreator, int tag,
            int level) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            sb.append('>');
        }
        String keyword = ElementDictionary.keywordOf(tag, privateCreator);
        if (keyword.isEmpty()) {
            keyword = "?";
        }
        sb.append(TagUtils.toString(tag)).append(' ').append(keyword);
        return sb.toString();
    }
}
