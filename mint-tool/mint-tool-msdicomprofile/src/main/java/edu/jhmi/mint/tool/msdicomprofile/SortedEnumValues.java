package edu.jhmi.mint.tool.msdicomprofile;

/*
 * #%L
 * MSDICOM Profile Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

public class SortedEnumValues {

    private final String primary;

    private final List<String> others;

    private final char delimiter;

    public SortedEnumValues(SortedSet<EnumValueProfile> sortedSet,
            char delimiter) {

        this.delimiter = delimiter;
        if (sortedSet.isEmpty()) {
            primary = "";
            others = Collections.emptyList();
        } else {
            Iterator<EnumValueProfile> iter = sortedSet.iterator();
            primary = iter.next().getName();
            others = new ArrayList<String>(sortedSet.size() - 1);
            while (iter.hasNext()) {
                others.add(iter.next().getName());
            }
        }
    }

    public final String getPrimary() {

        return primary;
    }

    public final List<String> getOthers() {

        return others.isEmpty() ? others : Collections.unmodifiableList(others);
    }

    public final String getOthersAsString() {

        return StudyProfile.join(others.iterator(), delimiter);
    }

    public final String getAllAsString() {

        if (primary.isEmpty()) {
            return "";
        }
        if (others.isEmpty()) {
            return primary;
        }
        return primary + delimiter + getOthersAsString();
    }
}
