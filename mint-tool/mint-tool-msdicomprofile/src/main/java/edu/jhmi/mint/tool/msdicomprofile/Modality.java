package edu.jhmi.mint.tool.msdicomprofile;

/*
 * #%L
 * MSDICOM Profile Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public class Modality {

    public static enum Type {

        ACQUISITION, DOCUMENT, OTHER
    }

    private static final Map<String, Type> codeTypeMap = new HashMap<String, Type>();

    static {

        codeTypeMap.put("AR", Type.ACQUISITION);
        codeTypeMap.put("BMD", Type.ACQUISITION);
        codeTypeMap.put("BDUS", Type.ACQUISITION);
        codeTypeMap.put("EPS", Type.ACQUISITION);
        codeTypeMap.put("CR", Type.ACQUISITION);
        codeTypeMap.put("CT", Type.ACQUISITION);
        codeTypeMap.put("DX", Type.ACQUISITION);
        codeTypeMap.put("ECG", Type.ACQUISITION);
        codeTypeMap.put("ES", Type.ACQUISITION);
        codeTypeMap.put("XC", Type.ACQUISITION);
        codeTypeMap.put("GM", Type.ACQUISITION);
        codeTypeMap.put("HD", Type.ACQUISITION);
        codeTypeMap.put("IO", Type.ACQUISITION);
        codeTypeMap.put("IVOCT", Type.ACQUISITION);
        codeTypeMap.put("IVUS", Type.ACQUISITION);
        codeTypeMap.put("KER", Type.ACQUISITION);
        codeTypeMap.put("LEN", Type.ACQUISITION);
        codeTypeMap.put("MR", Type.ACQUISITION);
        codeTypeMap.put("MG", Type.ACQUISITION);
        codeTypeMap.put("NM", Type.ACQUISITION);
        codeTypeMap.put("OAM", Type.ACQUISITION);
        codeTypeMap.put("OCT", Type.ACQUISITION);
        codeTypeMap.put("OPM", Type.ACQUISITION);
        codeTypeMap.put("OP", Type.ACQUISITION);
        codeTypeMap.put("OPR", Type.ACQUISITION);
        codeTypeMap.put("OPT", Type.ACQUISITION);
        codeTypeMap.put("OPV", Type.ACQUISITION);
        codeTypeMap.put("PX", Type.ACQUISITION);
        codeTypeMap.put("PT", Type.ACQUISITION);
        codeTypeMap.put("RF", Type.ACQUISITION);
        codeTypeMap.put("RG", Type.ACQUISITION);
        codeTypeMap.put("SM", Type.ACQUISITION);
        codeTypeMap.put("SRF", Type.ACQUISITION);
        codeTypeMap.put("US", Type.ACQUISITION);
        codeTypeMap.put("VA", Type.ACQUISITION);
        codeTypeMap.put("XA", Type.ACQUISITION);

        codeTypeMap.put("AU", Type.DOCUMENT);
        codeTypeMap.put("DOC", Type.DOCUMENT);
        codeTypeMap.put("KO", Type.DOCUMENT);
        codeTypeMap.put("PR", Type.DOCUMENT);
        codeTypeMap.put("RTDOSE", Type.DOCUMENT);
        codeTypeMap.put("SR", Type.DOCUMENT);
    }

    public static Type typeOf(String modalityCode) {

        Type type = codeTypeMap.get(modalityCode);
        return type == null ? Type.OTHER : type;
    }

    public static boolean isAcquisition(String modalityCode) {

        return codeTypeMap.get(modalityCode) == Type.ACQUISITION;
    }

    public static boolean isDocument(String modalityCode) {

        return codeTypeMap.get(modalityCode) == Type.DOCUMENT;
    }
}
