package edu.jhmi.mint.tool.msdicomprofile;

/*
 * #%L
 * MSDICOM Profile Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.util.Comparator;
import java.util.Iterator;

class EnumValueProfile implements Comparable<EnumValueProfile> {

    private final String name;

    private int count;

    public EnumValueProfile(String name, int count) {

        this.name = name;
        this.count = count;
    }

    public EnumValueProfile(String name) {

        this(name, 0);
    }

    public final String getName() {

        return name;
    }

    public final int getCount() {

        return count;
    }

    public void increaseCount() {

        ++count;
    }

    @Override
    public int compareTo(EnumValueProfile o) {

        int countDiff = o.count - count;
        if (countDiff != 0) {
            return countDiff;
        }
        return name.compareTo(o.name);
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + count;
        result = prime * result + (name == null ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof EnumValueProfile)) {
            return false;
        }
        EnumValueProfile other = (EnumValueProfile) obj;
        return name.equals(other.name) && count == other.count;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("name=").append(name);
        sb.append(", count=").append(count);
        return sb.toString();
    }

    public static Iterator<String> toNameIterator(
            final Iterator<EnumValueProfile> iter) {

        return new Iterator<String>() {

            @Override
            public boolean hasNext() {

                return iter.hasNext();
            }

            @Override
            public String next() {

                return iter.next().name;
            }

            @Override
            public void remove() {

                iter.remove();
            }
        };
    }

    static class NameComparator implements Comparator<EnumValueProfile> {

        @Override
        public int compare(EnumValueProfile o1, EnumValueProfile o2) {

            return o1.name.compareTo(o2.name);
        }
    }
}
