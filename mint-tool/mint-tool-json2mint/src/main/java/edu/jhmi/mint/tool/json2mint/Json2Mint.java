package edu.jhmi.mint.tool.json2mint;

/*
 * #%L
 * MSDICOM to DICOM Conversion Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.MintEncodingOptions;
import edu.jhmi.mint.io.MintHeader;
import edu.jhmi.mint.io.MintJsonReader;
import edu.jhmi.mint.io.MintStudyWriter;

import com.fasterxml.jackson.core.JsonGenerationException;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class Json2Mint {

    public static void main(String[] args) throws IOException,
            JsonGenerationException {
        if (args.length < 2 || args.length > 4) {
            System.err.println("Usage: Json2Mint [-CEV] <version_of_header>"
                    + " <json_input_dir> <mint_store_dir>");
            System.err.println(" -C: do not compress study metadata");
            System.err.println(" -E: do not encrypt study metadata");
            System.err.println(" -V: the version of mint");
            System.exit(1);
        }

        boolean encrypt = true;
        boolean compress = true;
        String version = "2.1";
        if (args.length >= 3) {
            String options = args[0];
            encrypt = options.indexOf('E') == -1;
            compress = options.indexOf('C') == -1;
            if (options.indexOf('V') != -1) {
                version = args[1];
                MintHeader.setMajorVersion((byte) Integer.parseInt(version
                        .substring(0, 1)));
                MintHeader.setMinorVersion((byte) Integer.parseInt(version
                        .substring(2, 3)));
            }
        }
        Path jsonDirPath = Paths.get(args[args.length - 2]);
        Path studyStoreDirPath = Paths.get(args[args.length - 1]);

        Study study = null;
        try (InputStream in = Files.newInputStream(jsonDirPath)) {
            study = MintJsonReader.readJSON(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (study != null) {
            String studyInstanceUID = study.getUID();
            Path outputDirPath = studyStoreDirPath.resolve(studyInstanceUID);
            if (!Files.exists(outputDirPath)) {
                Files.createDirectories(outputDirPath);
            }
            Path metadataFilePath = outputDirPath.resolve(String.format(
                    "%s.mmd", studyInstanceUID));
            MintEncodingOptions encodingOptions = new MintEncodingOptions(
                    encrypt, compress);
            try (BufferedOutputStream bufferedOut = new BufferedOutputStream(
                    Files.newOutputStream(metadataFilePath))) {
                MintStudyWriter.writeMetadata(bufferedOut, study,
                        encodingOptions);
            }
        }
    }
}
