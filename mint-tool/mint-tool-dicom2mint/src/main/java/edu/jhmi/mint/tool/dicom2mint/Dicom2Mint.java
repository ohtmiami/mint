package edu.jhmi.mint.tool.dicom2mint;

/*
 * #%L
 * DICOM to MINT Conversion Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import edu.jhmi.mint.data.Study;
import edu.jhmi.mint.io.DefaultStudyStore;
import edu.jhmi.mint.io.DicomInputSource;
import edu.jhmi.mint.io.DicomStudyReader;
import edu.jhmi.mint.io.MintEncodingOptions;
import edu.jhmi.mint.io.MintHeader;
import edu.jhmi.mint.io.MintStudyWriter;
import edu.jhmi.mint.io.StudyStore;

/**
 * 
 * @author Raphael Yu Ning <ning@jhmi.edu>
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class Dicom2Mint {

	public static void main(String[] args) throws IOException {

		if (args.length < 2 || args.length > 4) {
			System.err
					.println("Usage: Dicom2Mint [-XCERLBV] <version_of_header>"
							+ " <study_input_dir|study_file_list> <study_store_dir>");
			System.err.println(" -X: only read .dcm files");
			System.err.println(" -C: do not compress study metadata");
			System.err.println(" -E: do not encrypt study metadata/bulkdata");
			System.err.println(" -R: do not reorganize study bulkdata");
			System.err.println(" -L: input is a text file containing a list of"
					+ " DICOM file paths, one per line");
			System.err.println(" -B: do not write bulkdata");
			System.err.println(" -V: the version of mint");
			System.exit(1);
		}
		boolean dcmFilesOnly = false;
		boolean encrypt = true;
		boolean compress = true;
		boolean reorganize = true;
		boolean writeBulkData = true;
		String version = "2.1";
		List<Path> filePaths = null;
		if (args.length >= 3) {
			String options = args[0];
			dcmFilesOnly = options.indexOf('X') >= 0;
			encrypt = options.indexOf('E') == -1;
			compress = options.indexOf('C') == -1;
			reorganize = options.indexOf('R') == -1;
			writeBulkData = options.indexOf('B') == -1;
			if (options.indexOf('V') != -1) {
				version = args[1];
				MintHeader.setMajorVersion((byte) Integer.parseInt(version
						.substring(0, 1)));
				MintHeader.setMinorVersion((byte) Integer.parseInt(version
						.substring(2, 3)));
			}
			if (options.indexOf('L') >= 0) {
				Path listingFilePath = Paths.get(args[args.length - 2]);
				filePaths = new ArrayList<Path>();
				try (BufferedReader fileIn = Files
						.newBufferedReader(listingFilePath)) {
					while (true) {
						String line = fileIn.readLine();
						if (line == null) {
							break;
						}
						String filePathName = line.trim();
						if (!filePathName.isEmpty()) {
							filePaths.add(Paths.get(filePathName));
						}
					}
				}
			}
		}
		Path studyDirPath = Paths.get(args[args.length - 2]);
		Path studyStoreDirPath = Paths.get(args[args.length - 1]);
		StudyStore studyStore = new DefaultStudyStore(studyStoreDirPath,
				writeBulkData);
		DicomStudyReader studyReader = new DicomStudyReader(studyStore);
		String extensionPattern = dcmFilesOnly ? DicomInputSource.DCM_EXTENSION_PATTERN
				: DicomInputSource.ANY_EXTENSION_PATTERN;
		Study study = null;
		try (DicomInputSource inputSource = filePaths == null ? DicomInputSource
				.fromFolder(studyDirPath, extensionPattern) : DicomInputSource
				.fromFiles(filePaths)) {
			study = studyReader.readStudy(inputSource);
		}
		if (study != null) {
			if (reorganize) {
				if (encrypt)
					studyStore.reorganizeBulkdata(study, "AES-GCM-256", true);
				else
					studyStore.reorganizeBulkdata(study, null, true);
			}
			String studyInstanceUID = study.getUID();
			Path outputDirPath = studyStoreDirPath.resolve(studyInstanceUID);
			if (!Files.exists(outputDirPath)) {
				Files.createDirectories(outputDirPath);
			}
			Path metadataFilePath = outputDirPath.resolve(String.format(
					"%s.mmd", studyInstanceUID));
			MintEncodingOptions encodingOptions = new MintEncodingOptions(
					encrypt, compress);
			try (BufferedOutputStream bufferedOut = new BufferedOutputStream(
					Files.newOutputStream(metadataFilePath))) {
				MintStudyWriter.writeMetadata(bufferedOut, study,
						encodingOptions);
			}
		}
	}
}
