package edu.jhmi.mint.tool.header2header;

/*
 * #%L
 * MINT Header Conversion Tool
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 * 
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 * 
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 * 
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.jhmi.mint.io.MintHeader;
import edu.jhmi.mint.io.MintHeaderConversion;

/**
 * 
 * @author Hsin-Hong Chiang <chsinhong@gmail.com>
 * 
 */
public class Header2Header {
    public static void main(String[] args) throws IOException {

        if (args.length < 2 || args.length > 4) {
            System.err.println("Usage: Header2Header [-V] <version_of_header>"
                    + " <input_study_file>" + " <output_study_file>");
            System.exit(1);
        }
        
        String version="2.1";
        if(args.length>2){
            if(args[0].indexOf('V') != -1 && version.length()==3){
                version=args[1];
                MintHeader.setMajorVersion((byte) Integer.parseInt(version
                        .substring(0, 1)));
                MintHeader.setMinorVersion((byte) Integer.parseInt(version
                        .substring(2, 3)));
            }else{
                System.err.println("Usage: Header2Header [-V] <version_of_header>"
                        + " <input_study_file>" + " <output_study_file>");
                System.exit(1);
            }
        }
        
        Path inputFilePath = Paths.get(args[args.length - 2]);
        Path outputFilePath = Paths.get(args[args.length - 1]);
        try (InputStream in = Files.newInputStream(inputFilePath);
                OutputStream out = Files.newOutputStream(outputFilePath)) {            
            MintHeaderConversion.mintMetaDataHeaderConversion(in, out,version.substring(0,1),version.substring(2,3));
        }
    }
}
